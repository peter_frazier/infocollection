InfoCollection.jar is a java framework for doing experiments in simulation
optimization.  The following is an overview of some of the important classes.

Example code may be found in the examples/ directory.  When you compile and run
each example, make sure InfoCollection.jar and all third-party libraries are in
your classpath.


**WARNING** This description is for a previous version of the library, and is
incorrect in some places for the new version of the library.  A better place to
start, at least for now, would be with the example code in InfoCollection/examples/.

InfoCollection.Problem
InfoCollection.Policy
	These contain base classes that will be inherited from in
	IndependentGaussian/.  The Problem class contains general information
	about the problem to be solved which is assumed known to the
	experimenter, such as the number of alternatives, the prior
	distribution of the unknown values to be measured, and the variance of
	the noise.  The Policy class contains the skeleton of a measurement
	policy, which will include the Bayesian updating as well as deciding
	which alternative to measure at each point in time. 

InfoCollection.IndependentGaussian.Problem
InfoCollection.IndependentGaussian.Policy
	These implement InfoCollection.Problem and InfoCollection.Policy to be
	specific to the independent normal problem.
	InfoCollection.IndependentGaussian.Policy is still an abstract class.
	It implements the independent normal Bayesian updating, but leaves the
	task of deciding which alternative to measure to one of its concrete
	subclasses below.

InfoCollection.IndependentGaussian.Boltzmann
InfoCollection.IndependentGaussian.Exploit
InfoCollection.IndependentGaussian.Gittins
InfoCollection.IndependentGaussian.IntervalEstimation
InfoCollection.IndependentGaussian.KnowledgeGradient1
InfoCollection.IndependentGaussian.KnowledgeGradient2
InfoCollection.IndependentGaussian.KnowledgeGradient3
InfoCollection.IndependentGaussian.MaxVarianceExplore
InfoCollection.IndependentGaussian.UniformExplore
	These all implement the interface in
	InfoCollection.IndependentGaussian.Policy.  In particular,
	KnowledgeGradient1 is a slow but simple implementation of the
	independent normal knowledge-gradient policy.  KnowledgeGradient2 and
	KnowledgeGradient3 are more computationally complex but should run
	faster.  All three should give the same decision. 

InfoCollection.Util
InfoCollection.Gaussian
	These classes contain various generically useful functions.

InfoCollection.Simulation/BasicSimulator
	BasicSimulator contains two methods for doing monte carlo simulations
	given an InfoCollection.Problem and an InfoCollection.Policy.  These
	methods generate true values from the Problem's prior distribution, and
	then simulate measurements by adding noise.  One method runs a single
	Monte Carlo simulation, and returns the value achieved.  Another method
	runs many simulations and calculates the mean and standard error of the
	result, which it stores using the MeanVariance class below.

InfoCollection.Simulation/MeanVariance
	A generic class for taking a stream of samples and pulling out the
	sample mean and sample standard deviation.
