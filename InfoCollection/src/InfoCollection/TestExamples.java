package InfoCollection;

import java.util.Random;

/*
 * This class is intended to contain various static methods for running a
 * policy on various test examples.
 */
public class TestExamples {

	/*
	 * Compute Example 1 from HeChCh07 and print out everything for debugging.
	 * Pass the policy you want to run. The policy should be set up to run 10
	 * alternatives.
	 */
	public static void Example1(SamplingRule policy, boolean verbose)
			throws Exception {
		Random rnd = new Random(42); // fix a seed so a sequence is repeatable
										// for debugging.
		NormalTruth truth = NormalTruth.Example1();

		if (verbose)
			System.out.println("Example 1: " + policy);
		policy.Start(truth.M());
		for (int n = 0; n < 100; n++) {
			int x = policy.GetMeasurementDecision();
			double y = truth.Sample(x, rnd);
			policy.RecordMeasurement(x, y);
			// calculate opportunity cost and correct selection. I
			// could average these over multiple runs to estimate
			// the expected value.
			int impl = policy.GetImplementationDecision();
			double OC = truth.Loss(impl);
			if (verbose)
				System.out.println("n=" + n + " x=" + x + " OC=" + OC);
		}
	}

	/*
	 * Runs Example1, but the focus is on evaluating computational speed of the
	 * algorithm, rather than on correctness. The idea is that you run this code
	 * while the profiler is running.
	 */
	public static void SpeedTest(SamplingRule policy) throws Exception {
		NormalTruth truth = NormalTruth.Example1();
		Simulator.LossAtEnd(policy, truth, 100, 100);
	}

	/*
	 * Randomly generate a problem from a belief and run the policy on that
	 * problem. The problem should be set up to handle M=10. It is nice to run
	 * this on a policy several times with assertions enabled to test for bugs.
	 */
	public static void RandomProblem(Belief generator, SamplingRule policy)
			throws Exception {
		RandomProblem(generator, policy, true);
	}

	public static void RandomProblem(Belief generator, SamplingRule policy,
			boolean verbose) throws Exception {
		Random rnd = new Random();
		Truth truth = generator.GenerateTruth(rnd);
		int nsamples = 100;

		if (verbose) {
			System.out.println("TestExamples.RandomProblem(" + policy + ")");
			System.out.println(truth);
		}
		Simulator.LossAtEnd(policy, truth, nsamples);
	}

	/*
	 * The next few functions test the policy on the slippage configuration with
	 * a collection of stopping rules of the same type. The function name tells
	 * which type of stopping rule to use: EOC Bonf, EOC Bayes, Fixed.
	 */
	/*
	 * public static void TestStopping(SamplingRule policy, StoppingRuleSeq
	 * stopSeq, boolean verbose) throws Exception {
	 * TestStopping(policy,stopSeq,verbose, ); }
	 */
	public static void TestStopping(SamplingRule policy,
			StoppingRuleSeq stopSeq, NormalTruth truth, boolean verbose)
			throws Exception {
		int nruns = 10000;
		int maxN = 2000;
		double result[][];

		if (verbose) {
			System.out.printf("# TestStopping nruns=%d maxN=%d\n", nruns, maxN);
			System.out.println("# Policy=" + policy);
			System.out.println("# Stopping Rule=" + stopSeq);
			System.out.println("# Stopping Rule=" + stopSeq);
			System.out.println("# E[OC]\tE[N]\ttimeouts=");
		}
		result = Simulator.LossWhenStopped(policy, truth, stopSeq, nruns, maxN);
		if (verbose) {
			for (int s = 0; s < stopSeq.Length(); s++)
				System.out.printf("%.3f\t%.3f\t%d\n", result[s][0],
						result[s][1], (int) result[s][2]);
		}
	}

	public static void TestStopping(SamplingRule policy, StoppingRuleSeq stopSeq)
			throws Exception {
		TestStopping(policy, stopSeq, NormalTruth.SlippageExample(), true);
	}

	/*
	 * Takes two policies and checks to see if they give the same measurement
	 * decisions on Example1() (by default, or whatever truth is passed). If
	 * they give the same decisions, then they will receive the same measurement
	 * noise, so if two policies really are equivalent then they should behave
	 * identically. If they don't give the same decision, then we just use the
	 * sampling decision from the first rule and continue on using that one.
	 * This test is useful for comparing two SamplingRules that are supposed to
	 * be equivalent. Returns the number of sampling decisions that were
	 * different.
	 */
	public static int CheckEquivalence(SamplingRule p1, SamplingRule p2)
			throws Exception {
		NormalTruth truth = NormalTruth.Example1();
		return CheckEquivalence(p1, p2, truth, false);
	}

	public static int CheckEquivalence(SamplingRule p1, SamplingRule p2,
			boolean verbose) throws Exception {
		NormalTruth truth = NormalTruth.Example1();
		return CheckEquivalence(p1, p2, truth, verbose);
	}

	public static int CheckEquivalence(SamplingRule p1, SamplingRule p2,
			Truth truth, boolean verbose) throws Exception {
		int numDisagreements;
		int n, x1, x2, impl1, impl2;
		double y1, y2;
		Random rnd = new Random(); // do not fix a seed.

		p1.Start(truth.M());
		p2.Start(truth.M());
		numDisagreements = 0;
		for (n = 0; n < 100; n++) {
			x1 = p1.GetMeasurementDecision();
			x2 = p2.GetMeasurementDecision();
			y1 = truth.Sample(x1, rnd);
			y2 = y1; // note we are sampling from x1, not x2.
			p1.RecordMeasurement(x1, y1);
			p2.RecordMeasurement(x1, y2); // again, we use x1, not x2.
			impl1 = p1.GetImplementationDecision();
			impl2 = p2.GetImplementationDecision();

			if (x1 == x2 && impl1 == impl2) {
				if (verbose)
					System.out.printf("x1=x2=%d impl1=impl2=%d\n", x1, impl1);
			} else {
				numDisagreements++;
				if (verbose)
					System.out.printf("x1=%d x2=%d impl1=%d impl2=%d\n", x1,
							x2, impl1, impl2);
			}
		}

		if (verbose) {
			if (numDisagreements == 0)
				System.out.println("Equivalent");
			else
				System.out.printf("NOT Equivalent (%d disagreements)\n",
						numDisagreements);
		}
		return numDisagreements;
	}
}
