package InfoCollection;

/* Creates a StoppingRuleSeq with only a single StoppingRule in it. */
public class OneStoppingRule implements StoppingRuleSeq {
	StoppingRule rule;

	public OneStoppingRule(StoppingRule rule) {
		this.rule = rule;
	}

	public void Start(int M) throws Exception {
		rule.Start(M);
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		rule.RecordMeasurement(x, y);
	}

	public boolean ShouldStop() {
		return rule.ShouldStop();
	}

	public boolean Next() {
		return false;
	}

	@Override
	public String toString() {
		return rule.toString();
	}

	public int Length() {
		return 1;
	}
}
