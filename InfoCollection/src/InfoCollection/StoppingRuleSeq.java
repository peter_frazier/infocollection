package InfoCollection;

/*
 * Contains a sequence of stopping rules, which are ordered in a particular
 * way.  In particular, they are be such that if rule i would stop at a belief
 * b, then rule j would also stop at b, for all j<i.
 */
public interface StoppingRuleSeq {

	public void Start(int M) throws Exception; // also goes to the beginning.

	public void RecordMeasurement(int x, double y) throws Exception;

	public boolean ShouldStop();

	public boolean Next(); // move to the next rule.

	public String toString();

	public int Length();
}
