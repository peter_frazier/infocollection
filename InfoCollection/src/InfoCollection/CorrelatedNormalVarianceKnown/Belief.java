/* 
 * Written by Gerald van den Berg and Will Manning
 * 
 * Creates a set of correlated beliefs
 */

package InfoCollection.CorrelatedNormalVarianceKnown;

import java.util.Random;

import InfoCollection.NormalTruth;
import InfoCollection.NotInformativeException;
import InfoCollection.Truth;

public class Belief implements InfoCollection.Belief {

	private double[] mu; // currently assumed means (M X 1 or M-vector)
	private double[] mvar; // measurement variance of each variable (M X 1 or
							// M-vector)
	private CovMatrix Sigma; // covariance matrix (M X M)
	private int M; // number of alternatives

	/**************************************************************************/
	/* Constructors */
	/**************************************************************************/

	// degenerate independent constructor
	public Belief(double[] mu, double[] mvar) {
		// check sizes
		this.M = mu.length;
		if (mvar.length != M)
			throw new IllegalArgumentException();

		// initalize matrices
		this.mu = mu.clone();
		this.mvar = mvar.clone();
		this.Sigma = CovMatrix.identity(M);
	}

	// array constructor
	public Belief(double[] mu, double[] mvar, double[][] Sigma) {
		// check sizes
		this.M = mu.length;
		if ((mvar.length != M) || (Sigma.length != M) || (Sigma[0].length != M))
			throw new IllegalArgumentException();

		// initalize matrices
		this.mu = mu.clone();
		this.mvar = mvar.clone();
		this.Sigma = new CovMatrix(Sigma);
	}

	// array constructor with constant measurement variance
	public Belief(double[] mu, double mvar, double[][] Sigma) {
		// check sizes
		this.M = mu.length;
		if ((Sigma.length != M) || (Sigma[0].length != M))
			throw new IllegalArgumentException();

		// initialize matrices
		this.mu = mu.clone();
		this.mvar = new double[M];
		for (int i = 0; i < M; i++)
			this.mvar[i] = mvar;
		this.Sigma = new CovMatrix(Sigma);
	}

	// if no covariance matrix is entered, but instead we have a constant alpha,
	// then create a power exponential covariance matrix
	// Note: Here we have msigma vector. Next constructor uses constant mvar
	public Belief(double[] mu, double[] mvar, double alpha, double beta) {

		// check sizes
		this.M = mu.length;
		if (mvar.length != M)
			throw new IllegalArgumentException();

		// initialize matrices and arrays
		this.mu = mu.clone();
		this.mvar = mvar.clone();
		this.Sigma = PowerExpCov(alpha, beta, M);
	}

	// constructor with a power exponential covariance matrix and constant mvar
	public Belief(double[] mu, double mvar, double alpha, double beta) {
		this.M = mu.length;
		this.mu = mu.clone();
		this.mvar = new double[M];
		for (int i = 0; i < M; i++)
			this.mvar[i] = mvar;
		this.Sigma = PowerExpCov(alpha, beta, M);
	}

	// Copy constructor
	public Belief(Belief b) {
		this.M = b.M();
		this.mu = b.mu.clone();
		this.Sigma = b.Sigma.clone();
		this.mvar = b.mvar.clone();
	}

	/**************************************************************************/
	/* Accessor Methods */
	/**************************************************************************/

	public double[] mu() {
		return this.mu;
	}

	public double[] mvar() {
		return this.mvar;
	}

	public CovMatrix Sigma() {
		return this.Sigma;
	}

	public int M() {
		return this.M;
	}

	/**************************************************************************/
	/* Core Functions */
	/**************************************************************************/

	// update our beliefs with a new observation
	public void Update(int x, double obs) {
		// calculate front constant multiplier
		double lambda = mvar[x];
		double var = Sigma.getVar(x);
		double cnst;
		if (lambda == 0.0 && var == 0.0) {
			if (StrictMath.abs(this.mu[x] - obs) > 1e-10)
				throw new IllegalArgumentException(
						"Measurement contradicts known value");
			else
				cnst = Double.POSITIVE_INFINITY;
		} else
			cnst = 1.0 / (lambda + var);

		// store current mean value of choice for updating
		double curmux = mu[x];

		// update mean vector
		for (int i = 0; i < M; i++)
			mu[i] += cnst * Sigma.getCov(i, x) * (obs - curmux);

		// update covariance matrix
		for (int i = 0; i < M; i++) {
			for (int j = 0; (j <= i) && (i != x); j++) {
				if (j != x) {
					double val = Sigma.getCov(i, j) - Sigma.getCov(x, i)
							* Sigma.getCov(x, j) * cnst;
					Sigma.set(i, j, val);
				}
			}
		}
		for (int j = 0; j < M; j++) {
			double val = Sigma.getCov(x, j) - Sigma.getCov(x, j) * var * cnst;
			Sigma.set(x, j, val);
		}
	}

	// String representation of our belief
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("mean\t\t\tmeasurement precision\n");

		for (int i = 0; i < M; i++) {
			sb.append(mu[i]);
			sb.append("\t\t\t");
			sb.append(mvar[i]);
			sb.append('\n');
		}
		sb.append('\n');
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				sb.append(Sigma.getCov(i, j));
				sb.append(" ");
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	/**************************************************************************/
	/* Additional Functions */
	/**************************************************************************/

	// creates a power exponential covariance matrix with parameters alpha, beta
	// Under construction - GV
	private CovMatrix PowerExpCov(double alpha, double beta, int M) {
		CovMatrix cov = new CovMatrix(M);

		// O(M^2) time, fill in all the appropriate entries assuming one
		// dimension
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				double val = beta * StrictMath.exp(alpha * (i - j) * (i - j));
				cov.set(i, j, val);
			}
		}
		return cov;
	}

	// calculate sigma tilde vector
	public double[] sigmatilde(int x) {
		double[] sigmatilde = new double[M];

		// calculate front constant multiplier
		double var = Sigma.getVar(x);
		double fconst = (1.0 / Math.sqrt(mvar[x] + var));

		for (int i = 0; i < M; i++)
			sigmatilde[i] = fconst * Sigma.getCov(i, x);
		return sigmatilde;
	}

	/*
	 * Returns whether or not the value of every alternative is integrable under
	 * the belief. This will be the case if the belief is informative.
	 */
	public Boolean IsIntegrable() {
		int x;
		for (x = 0; x < this.M() && !Double.isInfinite(this.Sigma.getVar(x)); x++)
			;
		return (x == this.M()); // true if all have finite variance
	}

	public Boolean IsInformative() {
		return IsIntegrable();
	}

	/*
	 * Generates the true values from the random number generator according to
	 * the prior.
	 */
	public Truth GenerateTruth(Random rnd) throws NotInformativeException {
		if (!IsInformative())
			throw (new NotInformativeException());

		double[] value = new double[this.M]; // true mean vector
		double[] noiseBeta = new double[this.M]; // truth variance
		double[][] A = Sigma.toArray(); // Covariance Matrix
		double[][] L = new double[this.M][this.M]; // Cholesky Decomposition of
													// A

		// Main loop to calculate elements of L
		// Citation: JAMA v1.0.2, CholeskyDecomposition.java, Constructor method
		// JAMA can be found at http://math.nist.gov/javanumerics/jama/
		for (int j = 0; j < this.M; j++) {
			double[] Lrowj = L[j];
			double d = 0.0;
			for (int k = 0; k < j; k++) {
				double[] Lrowk = L[k];
				double s = 0.0;
				for (int i = 0; i < k; i++) {
					s += Lrowk[i] * Lrowj[i];
				}
				Lrowj[k] = s = (A[j][k] - s) / L[k][k];
				d = d + s * s;
			}
			d = A[j][j] - d;
			L[j][j] = StrictMath.sqrt(StrictMath.max(d, 0.0));
			for (int k = j + 1; k < this.M; k++) {
				L[j][k] = 0.0;
			}
		}

		// Generates Z vector, where each Z[i] ~ Gsn(0,1) i.i.d.
		double[] Z = new double[this.M];
		for (int i = 0; i < this.M; i++) {
			Z[i] = rnd.nextGaussian();
		}

		// Generates truth vectors from L and mu
		for (int i = 0; i < this.M; i++) {
			double s = 0.0;
			for (int j = 0; j < this.M; j++) {
				s += L[i][j] * Z[j];
			}
			value[i] = this.mu[i] + s;
			noiseBeta[i] = 1 / this.mvar[i];
		}

		// returns NormalTruth object
		return new NormalTruth(value, noiseBeta);
	}

	// Effective number of measurements of alternative i
	public double EffectiveNum(int x) {
		return mvar[x] / this.Sigma.getVar(x);
	}

	// Computes the standard deviations of our belief about the true means.
	public double[] StdDev() {
		double[] sig = new double[this.M];
		for (int x = 0; x < this.M; x++) {
			sig[x] = StrictMath.sqrt(this.Sigma.getVar(x));
		}
		return sig;
	}

	/***************************************************************************/
	/* Test client */
	/***************************************************************************/

	public static void main(String[] args) {
		System.out.println("Testing...");
		System.out.println("***********************************************");

		boolean pass = true;
		pass = test1() && pass;
		pass = test2() && pass;
		pass = test3() && pass;

		if (pass)
			System.out.println("Belief: PASS");
		else
			System.out.println("Belief: FAIL");
	}

	public static boolean test1() {
		System.out.println("test1: ...");
		double[] mu = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		double[] mvar = { .5, .5, .5, .5, .5, .5, .5, .5, .5 };
		double[][] sigma = { { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
				{ 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 1, 1, 1, 1, 1, 1, 1, 1, 1 } };
		try {
			Belief b = new Belief(mu, mvar, sigma);
			System.out.println(b);
			b.Update(1, 2.1);
			System.out.println(b);
			System.out.println("PASS");
			return true;
		} catch (Exception e) {
			System.out.println("FAIL");
			return false;
		}
	}

	@SuppressWarnings("unused")
	public static boolean test2() {
		double[] mu = { 1, 1 };
		double[] mvar = { .1, .15 };
		double[][] sigma = { { .5, .4 }, { .3, .2 } };
		try {
			Belief b = new Belief(mu, mvar, sigma);
			System.out.println("test2: FAIL");
			return false;
		} catch (Exception e) {
			System.out.println("test2: PASS");
			return true;
		}
	}

	@SuppressWarnings("unused")
	public static boolean test3() {
		double[] mu = { 1, 2, 3, 4 };
		double[] mvar = { 1, 2 };
		double[][] sigma = { { 1, 2 }, { 2, 4 } };
		try {
			Belief b = new Belief(mu, mvar, sigma);
			System.out.println("test3: FAIL");
			return false;
		} catch (Exception e) {
			System.out.println("test3: PASS");
			return true;
		}
	}
}