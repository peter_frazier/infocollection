/*
 * This class describes the covariance matrix for a
 * multivariate, correlated normal random variable.
 * It is primarily used by the Belief data type.
 * 
 * Written by Will Manning
 */

package InfoCollection.CorrelatedNormalVarianceKnown;

public class CovMatrix extends Symmatrix {
	public CovMatrix(int M) {
		super(M);
	}

	// general constructor
	public CovMatrix(double[][] array) {
		super(array);
		if (!this.isValid())
			throw new IllegalArgumentException();
	}

	// general constructor with ability to disable validity checking
	public CovMatrix(double[][] array, boolean check) {
		super(array);
		if (check) {
			if (!this.isValid())
				throw new IllegalArgumentException();
		}
	}

	// random constructor; values of matrix on interval [M*a^2,M*b^2]
	public CovMatrix(int M, double a, double b) {
		super(M);
		double[][] L = new double[M][M];

		// assure that a <= b
		if (a > b) {
			double temp = a;
			a = b;
			b = temp;
		}

		// generates random matrix
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				L[i][j] = a + Math.random() * (b - a);
			}
		}

		// sets matrix equal to (L * L-transpose)
		double[] LtCol = new double[M];
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				LtCol[j] = L[i][j]; // fills LtCol with the ith column of L'
			}
			for (int j = 0; j < M; j++) {
				double[] LRow = L[j]; // jth row of LRow
				double val = 0;
				for (int k = 0; k < M; k++) {
					val += LRow[k] * LtCol[k];
				}
				try {
					super.set(j, i, val);
				} catch (Exception e) {
					// Impossible
					assert (false);
				}
			}
		}
	}

	// returns a deep copy of the object
	@Override
	public CovMatrix clone() {
		return new CovMatrix(super.toArray());
	}

	public static CovMatrix identity(int M) {
		CovMatrix cov = new CovMatrix(M);
		for (int i = 0; i < M; i++)
			cov.set(i, i, 1);
		return cov;
	}

	// returns the covariance of alternatives i & j
	// rows and columns are indexed from 0 to M-1
	public double getCov(int i, int j) {
		return super.get(i, j);
	}

	// returns the variance of the belief for alternative i
	// simply for convenience; equal to getCov(i, i)
	public double getVar(int i) {
		return super.get(i, i);
	}

	// returns the precision of the belief for alternative i
	public double getBeta(int x) {
		double var = super.get(x, x);
		if (var == Double.POSITIVE_INFINITY)
			return 0;
		else if (var == 0)
			return Double.POSITIVE_INFINITY;
		else
			return (1 / var);
	}

	// checks (within numerical precision) that Covariance matrix is valid
	// uses identity cov(i,j) = corr(i,j) * sigma_i * sigma_j, corr on [-1,1]
	public boolean isValid() {
		for (int i = 0; i < this.M(); i++) {
			for (int j = 0; j <= i; j++) {
				double sig1 = StrictMath.sqrt(this.getVar(i));
				double sig2 = StrictMath.sqrt(this.getVar(j));
				double cov = this.getCov(i, j);
				double corr = cov / (sig1 * sig2);
				if (StrictMath.abs(corr) - 1 > 1e-15) {
					// String err =
					// String.format("(%d,%d) -> R^2 = %f",i,j,corr);
					// System.out.println(err);
					return false;
				}
			}
		}
		return true;
	}

	// test client
	public static void main(String[] args) {
		System.out.println("Testing...");
		System.out.println("***********************************************");

		boolean pass = true;
		pass = test1() && pass;
		pass = test2() && pass;
		pass = test3() && pass;
		pass = test4() && pass;

		if (pass)
			System.out.println("CovMatrix: PASS");
		else
			System.out.println("CovMatrix: FAIL");
	}

	// Creates random CovMatrix and checks to make sure that it is
	// completely valid
	public static boolean test1() {
		int M = 10;
		double a = 0.0, b = 10.0;
		System.out.print("test1: ");
		CovMatrix cov = new CovMatrix(M, a, b);
		System.out.println("\n" + cov.toString());

		for (int i = 0; i < M; i++) {
			if (cov.getVar(i) != cov.getCov(i, i)) {
				System.out.println("FAIL");
				return false;
			}
			for (int j = 0; j < M; j++) {
				double val = cov.getCov(i, j);
				if (val != cov.getCov(j, i)) {
					System.out.println("FAIL");
					return false;
				} else if (val > M * b * b || val < M * a * a) {
					System.out.println("FAIL");
					return false;
				}
			}
		}

		if (!cov.isValid()) {
			System.out.println("FAIL");
			return false;
		}

		System.out.println("PASS");
		return true;
	}

	// Creates a random CovMatrix and clones it. Makes sure
	// that the clone is identical to the original.
	public static boolean test2() {
		int M = 100;
		double a = -1000.0, b = 1000.0;
		CovMatrix cov = new CovMatrix(M, a, b);
		CovMatrix cov2 = cov.clone();

		if (M != cov.M() || M != cov2.M()) {
			System.out.println("test2: FAIL");
			return false;
		}

		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				if (cov.getCov(i, j) != cov2.getCov(i, j)) {
					System.out.println("test2: FAIL");
					return false;
				}
			}
		}
		System.out.println("test2: PASS");
		return true;
	}

	// Tests validity checking methods by inputting invalid array
	@SuppressWarnings("unused")
	public static boolean test3() {
		double[][] vals = { { 1, 2, 3 }, { 2, 1, 5 }, { 3, 4, 1 } };
		try {
			CovMatrix cov = new CovMatrix(vals);
			System.out.println("test3: FAIL");
			return false;
		} catch (Exception e) {
			System.out.println("test3: PASS");
			return true;
		}
	}

	// Checks the validity of N random MxM CovMatrices
	public static boolean test4() {
		int M = 10, N = 1000;
		double a = 0.0, b = 100.0;

		for (int i = 0; i < N; i++) {
			CovMatrix cov = new CovMatrix(M, a, b);
			if (!cov.isValid()) {
				System.out.println("test4: FAIL");
				return false;
			}
		}

		System.out.println("test4: PASS");
		return true;
	}
}
