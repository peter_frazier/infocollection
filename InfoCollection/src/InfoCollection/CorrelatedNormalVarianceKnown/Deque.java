/*
 * Deque.java
 * Double-ended Queue data type
 * 
 * Written by Will Manning & Gerald van den Berg
 * 
 * Citation: Iterator & test code based on counterparts in 
 * Queue.java by Robert Sedgewick & Kevin Wayne, found at:
 * http://www.cs.princeton.edu/algs4/26stacks/Queue.java.html
 *
 */

package InfoCollection.CorrelatedNormalVarianceKnown;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
	private Node first, last;
	private int N;

	private class Node {
		private Item item;
		private Node next;
		private Node prev;
	}

	public Deque()
	// construct an empty deque
	{
		first = null;
		last = null;
		N = 0;
	}

	public boolean isEmpty()
	// return true if the queue is empty, false otherwise
	{
		return (N == 0);
	}

	public int size()
	// return the number of elements in the deque
	{
		return N;
	}

	public void addFirst(Item item)
	// insert the item at the front of the queue
	{
		Node temp = new Node();
		temp.item = item;
		temp.next = first;
		temp.prev = null;
		if (first != null)
			first.prev = temp;
		first = temp;
		if (N == 0)
			last = first;
		N++;
	}

	public void addLast(Item item)
	// insert the item at the end of the queue
	{
		Node temp = new Node();
		temp.item = item;
		temp.next = null;
		temp.prev = last;
		if (last != null)
			last.next = temp;
		last = temp;
		if (N == 0)
			first = last;
		N++;
	}

	public Item removeFirst()
	// delete and return the first item in queue
	{
		if (isEmpty())
			throw new RuntimeException("Deque underflow error");
		Item item = first.item;
		first = first.next;
		if (first != null)
			first.prev = null;
		N--;
		return item;
	}

	public Item removeLast()
	// delete and return the last item in queue
	{
		if (isEmpty())
			throw new RuntimeException("Deque underflow error");
		Item item = last.item;
		last = last.prev;
		if (last != null)
			last.next = null;
		N--;
		return item;
	}

	// return the last item in queue
	public Item last() {
		return last.item;
	}

	// return the first item in queue
	public Item first() {
		return first.item;
	}

	public Iterator<Item> iterator()
	// return an iterator that examines the items in order from front to back
	{
		return new DequeIterator();
	}

	// an iterator
	private class DequeIterator implements Iterator<Item> {
		private Node current = first;

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext())
				throw new NoSuchElementException();
			Item item = current.item;
			current = current.next;
			return item;
		}
	}

	// Test method.
	public static void main(String[] args) {
		Deque<String> deque = new Deque<String>();
		deque.addFirst("successful!");
		deque.addFirst("test ");
		deque.addFirst("First ");

		deque.removeFirst();
		deque.addFirst("Second ");

		deque.addLast("successful!");
		deque.addLast("test ");
		deque.addLast("Third ");

		try {
			deque.removeFirst();
		} catch (Exception e) {
			System.out.println("It appropriately threw an exception when "
					+ "removing from the start of an empty deque.");
		}

		try {
			deque.removeLast();
		} catch (Exception e) {
			System.out.println("It appropriately threw an exception when "
					+ "removing from the end of an empty deque.");
		}

	}
}
