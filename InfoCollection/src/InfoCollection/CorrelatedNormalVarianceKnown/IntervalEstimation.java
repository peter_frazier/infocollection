package InfoCollection.CorrelatedNormalVarianceKnown;

import InfoCollection.util.MathPF;

public class IntervalEstimation extends BayesSamplingRule {
	double zalpha; // equal to NormalDist.inverseF01(1-alpha/2);

	/*
	 * Interval estimation is parameterized by a confidence level alpha, or
	 * equivalently by z_{\alpha/2} which is the value z such that the
	 * probability a standard normal exceeds z is \alpha/2. Typically, this
	 * number, which we call zalpha in the code, is between 2 and 4.
	 */
	public IntervalEstimation(double zalpha) {
		super();
		this.zalpha = zalpha;
	}

	public IntervalEstimation() {
		super();
		zalpha = 3;
	} // Default confidence level

	public IntervalEstimation(Belief b, double zalpha) {
		super(b);
		this.zalpha = zalpha;
	}

	public int GetMeasurementDecision() {
		/*
		 * upperBound[x] is the upperBound of a 100(1-\alpha)% confidence level
		 * for Y_x under the assumption that the observations of alternative x
		 * have unknown mean Y_x and known variance.
		 * 
		 * Confidence intervals come from classical statistics, and priors come
		 * from Bayesian statistics. How do we reconcile these? On each
		 * measurement of alternative x, the inverse variance of the prior on
		 * Y_x increases by problem.noiseBeta. Therefore we can think of the
		 * number of observations of alternative x, including observations that
		 * are "implicit" from the prior at time 0, as the inverse variance of
		 * the prior on alternative x divided by the incremental inverse
		 * variance that each measurement adds. Similarly, the mean of the prior
		 * is equal to the sample mean of the measurements actually taken
		 * together with "implicit" measurements encoded by the time 0 prior. If
		 * we take the noninformative prior, which is beta^0=0, then this gives
		 * exactly what classical statistics gives.
		 * 
		 * In an old version of the code we calculated this upper bound by
		 * double numMeasurementsOfX = beta[x]/problem.noiseBeta; upperBound[x]
		 * = NormalConfidenceIntervalUpperBound(numMeasurementsOfX, mu[x],
		 * 1/problem.noiseBeta, zalpha); where
		 * NormalConfidenceIntervalUpperBound was computed by double
		 * NormalConfidenceIntervalUpperBound(double numObserved, double
		 * sampleMean, double variance, double zalpha) { return sampleMean +
		 * Math.sqrt(variance/numObserved) * zalpha; This is equivalent to the
		 * way we do it now.
		 */
		double upperBound[] = new double[M()];

		for (int x = 0; x < M(); x++) {
			upperBound[x] = belief.mu()[x]
					+ Math.sqrt(belief.Sigma().getVar(x)) * zalpha;
			// System.out.print(upperBound[x] + " ");
		}
		// System.out.println("");
		return (MathPF.argmax(upperBound));
	}

	@Override
	public String toString() {
		return "IntervalEstimation z_{\\alpha/2}=" + zalpha;
	}
};
