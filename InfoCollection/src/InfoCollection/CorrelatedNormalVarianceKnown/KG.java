/* 
 * Author: Gerald van den Berg
 * Adapted by Will Manning
 * Under direction of P. Frazier, W. Powell, and I. Rhyzov at CASTLE lab
 * 
 * Implementation of the Correlated Knowledge Gradient
 * Algorithms herein taken from "The Knowledge Gradient Policy for 
 * Correlated Normal Beliefs" (2008) by P. Frazier, W. Powell, and S. Dayanik.
 * Implementation, however, is the authors'. 
 * Takes a Belief and returns an array containing each knowledge gradient index. 
 */

package InfoCollection.CorrelatedNormalVarianceKnown;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;
import java.util.TreeMap;

import InfoCollection.util.MathPF;
import InfoCollection.util.Normal;

public class KG extends BayesSamplingRule {
	private double logKG[]; // natural logarithms of KG factors

	/**************************************************************************/
	/* Constructor */
	/**************************************************************************/

	public KG(Belief b) throws Exception {
		super(b);
	}

	/**************************************************************************/
	/* Administrative Methods */
	/**************************************************************************/

	/*
	 * The following methods are part of the superclass: public void
	 * RecordMeasurement(int x, double y) public int GetImplementationDecision()
	 * public int GetNumMeasurements(int x) public int M() public Belief
	 * GetBelief()
	 */

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		logKG = new double[M()];
		this.RecomputeKG();
	}

	public Belief GetPrior() {
		return this.belief0;
	}

	public double[] GetLogKG() {
		return this.logKG;
	}

	public int GetMeasurementDecision() {
		return MathPF.argmax(this.logKG);
	}

	@Override
	public void RecordMeasurement(int x, double y) {
		super.RecordMeasurement(x, y);
		RecomputeKG();
	}

	/*
	 * This is the version of RecordMeasurement that outputs debugging info. We
	 * implement this method rather than inheriting the default so that we can
	 * print out the extra state this policy keeps.
	 */
	public void RecordMeasurement(int x, double y, PrintStream s)
			throws Exception {
		int[] ret = MathPF.FirstSecond(belief.mu());
		s.format("mu=%s\n", Arrays.toString(belief.mu()));
		s.format("mvar=%s\n", Arrays.toString(belief.mvar()));
		s.format("Sigma=%s\n", belief.Sigma().toString());
		s.format("mu[first]=%g mu[second]=%g\n", belief.mu()[ret[0]], belief
				.mu()[ret[1]]);
		s.format("logKG=%s\n", Arrays.toString(logKG));
		s.format("RecordMeasurement: x=%d y=%g\n", x, y);
		RecordMeasurement(x, y);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		return sb.toString();
	}

	/**************************************************************************/
	/* Updating Methods */
	/**************************************************************************/

	// return an array containing the KG index corresponding to each
	// alternative (actually its log for numerical precision - Vkg can
	// become quite small.)
	private void RecomputeKG() {
		double[] a0 = this.belief.mu(); // current belief regarding means
		TreeMap<Double, Double> slopes = new TreeMap<Double, Double>();

		// loop through each alternative and calculate its KG index
		for (int l = 0; l < M(); l++) {

			// read in current slopes and put them in a SET
			// all entries with duplicate b-values will be removed
			// with the entry with the highest a-value remaining
			double[] b0 = this.belief.sigmatilde(l);
			for (int i = 0; i < M(); i++) {
				Put(slopes, a0[i], b0[i]); // see function below for details
			}

			// store our pairs in arrays. They will now be in sorted
			// ordered without any duplicate entries
			int Mi = slopes.size(); // new size without duplicate entries
			double[] a = new double[Mi];
			double[] b = new double[Mi];

			for (int i = 0; i < Mi; i++) {
				b[i] = slopes.firstKey();
				a[i] = slopes.remove(b[i]);
			}

			// Debugging - GV
			// for (int i = 0; i < Mprime; i++) {
			// System.out.println("a[]: " + a[i]);
			// System.out.println("b[]: " + b[i]);
			// }
			// assert(slopes.isEmpty());

			double[] c = new double[Mi];
			Deque<Integer> A = new Deque<Integer>();
			BreakpointAlgorithm1(A, c, a, b);
			int Mii = A.size();

			// Debugging - GV
			// System.out.println("size: " + Mii);

			// the final sum for the knowledge gradient
			// First the logs of the items to be summed are
			// calculated, and then their log_sum is taken.

			int i, iplusone;
			double[] templogv = new double[Mii - 1];
			int index = 0;

			while (A.size() > 1) {
				i = A.removeLast();
				iplusone = A.last();
				// System.out.println("i: " + i);
				// System.out.println("iplusone: " + iplusone);
				templogv[index] = Math.log(b[iplusone] - b[i])
						+ Normal.LogPsi(Math.abs(c[i]));
				index++;

				// Debugging - GV
				// System.out.println("Cur i: " + i);
				// System.out.println("Cur b: " + b[i]);
				// System.out.println("Cur c: " + c[i]);
			}

			// log_sum computes log(\sum_i a[i]) when given log(a[i])
			logKG[l] = MathPF.log_sum(templogv);
		}
	}

	// Algorithm 1 on pg. 13 of P. Frazier et al., 2008
	// The indexing is a little different from the original
	// algorithm as we index arrays from 0 in JAVA. Thus
	// everything is 1 less in this algorithm. - GV
	// The algorithm calculates c and A s.t. i in A and
	// z in [ci-1, ci) are such that g(z) = i
	// In non-mathemtical terms, it breaks down the real
	// line into a series of breakpoints corresponding
	// to intersections of lines with intercept a and
	// coefficient b
	private void BreakpointAlgorithm1(Deque<Integer> A, double[] c, double[] a,
			double[] b) {
		c[0] = Double.POSITIVE_INFINITY;
		int j, k = 0;
		int Mi = a.length;
		A.addFirst(0);

		for (int i = 0; i < Mi - 1; i++) {
			do {
				c[i + 1] = Double.POSITIVE_INFINITY;
				j = A.removeFirst();
				c[j] = (a[j] - a[i + 1]) / (b[i + 1] - b[j]);
				if (!A.isEmpty())
					k = A.first();
			} while ((!A.isEmpty()) && (c[j] <= c[k]));
			A.addFirst(j);
			A.addFirst(i + 1);
		}
	}

	// This function ensures that we only maintain the "Pair" with
	// the highest a values in cases of equivalent b values.
	private void Put(TreeMap<Double, Double> slopes, double a, double b) {
		if (slopes.containsKey(b)) {
			if (slopes.get(b) >= a) {
				// keep original entry
			} else {
				// insert new entry, which has a greater a value
				// and remove original entry
				slopes.remove(b);
				slopes.put(b, a);
			}
		} else {
			slopes.put(b, a);
		}
	}

	/***************************************************************************/
	/* Test client */
	/***************************************************************************/

	public static void main(String[] args) throws Exception {
		/*
		 * String stars = "*********************************";
		 * System.out.println("Testing"); System.out.println(stars);
		 */

		boolean pass = true;
		// pass = test1() && pass;
		// pass = test2() && pass;
		// pass = test3() && pass;
		pass = test4() && pass;
		/*
		 * if (pass) System.out.println("KG: PASS"); else
		 * System.out.println("KG: FAIL");
		 */
	}

	// compares results with those output by analogous Matlab code
	public static boolean test1() {
		int M = 5;
		double[] mu = new double[M];
		for (int i = 0; i < M; i++) {
			mu[i] = 1;
		}
		double mvar = .5;
		double[][] Sigma = { { 2.1706, 0.8137, 1.9535, 1.4609, 1.5476 },
				{ 0.8137, 0.6076, 0.7518, 0.5765, 0.6037 },
				{ 1.9535, 0.7518, 2.1738, 1.6281, 1.6101 },
				{ 1.4609, 0.5765, 1.6281, 1.5576, 1.4502 },
				{ 1.5476, 0.6037, 1.6101, 1.4502, 1.3943 } };
		double[] ans = { -1.104887430168766, -2.4088879012205746,
				-1.058624542607066, -1.229395846861665, -1.2319836148166816 };

		Belief b1 = new Belief(mu, mvar, Sigma);
		try {
			KG kg = new KG(b1);
			kg.Start(b1.M());
			double[] logKG = kg.GetLogKG();
			for (int i = 0; i < b1.M(); i++) {
				if (StrictMath.abs(logKG[i] - ans[i]) > 1e-15) {
					System.out.println("test1: FAIL");
					return false;
				}
			}
			System.out.println("test1: PASS");
			return true;
		} catch (Exception e) {
			System.out.println("test1: Exception thrown: FAIL");
			return false;
		}
	}

	// runs simulated policies on random mean vectors with both independent
	// and correlated code (cov matrix = identity) through a certain number
	// of measurements
	public static boolean test2() {
		int M = 10; // number of alternatives
		int N = 1000; // number of iterations
		int P = 10; // number of measurements per iteration
		Random rand = new Random();
		double[] mu = new double[M]; // randomly generated means
		double[] ones = new double[M]; // array of ones
		double[] logKGi, logKGc; // logKG for independent and correlated cases

		for (int i = 0; i < M; i++) {
			ones[i] = 1.0;
		}
		try {
			for (int i = 0; i < N; i++) {
				// initialize random mu
				for (int j = 0; j < M; j++) {
					mu[j] = rand.nextGaussian();
				}

				// initialize Beliefs and KGs
				Belief bc = new Belief(mu, ones);
				KG kgc = new KG(bc);
				kgc.Start(M);
				InfoCollection.IndependentNormalVarianceKnown.Belief bi = new InfoCollection.IndependentNormalVarianceKnown.Belief(
						mu, ones);
				InfoCollection.IndependentNormalVarianceKnown.KG kgi = new InfoCollection.IndependentNormalVarianceKnown.KG(
						bi);
				kgi.Start(M);

				// check initial logKGs
				logKGi = kgi.GetLogKG();
				logKGc = kgc.GetLogKG();
				for (int j = 0; j < M; j++) {
					if (StrictMath.abs(logKGi[j] - logKGc[j]) > 1e-10) {
						System.out.println("test2: FAIL");
						String err = String.format("logKGi = %g; logKGc = %g",
								logKGi[j], logKGc[j]);
						System.out.println(err);
						return false;
					}
				}

				// take measurements, checking logKGs after each
				for (int j = 0; j < P; j++) {
					int x = (int) (rand.nextDouble() * M);
					double obs = mu[x] + ones[x] * rand.nextGaussian();
					kgc.RecordMeasurement(x, obs);
					kgi.RecordMeasurement(x, obs);
					for (int k = 0; k < M; k++) {
						if (StrictMath.abs(logKGi[k] - logKGc[k]) > 1e-10) {
							System.out.println("test2: FAIL");
							String err = String.format(
									"logKGi = %g; logKGc = %g", logKGi[k],
									logKGc[k]);
							System.out.println(err);
							return false;
						}
					}
				}
			}
			System.out.println("test2: PASS");
			return true;
		} catch (Exception e) {
			System.out.println("test2: Exception thrown: FAIL");
			return false;
		}
	}

	public static boolean test3() {
		double[] mu = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		double[] mvar = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		double[][] Sigma = {
				{ 370.1209448353449, 318.3213072154765, 186.95676180772304,
						242.66867651667408, 201.9984143956534,
						264.79728580816914, 244.70758709751254,
						355.510035881079, 212.10849080160645,
						252.02375064426334 },
				{ 318.3213072154765, 363.84877909782244, 238.79828693841722,
						243.8233967093607, 179.7335037535733,
						301.24657950113163, 292.04015276608663,
						355.10878527145246, 163.82866495088206,
						291.0402125920667 },
				{ 186.95676180772304, 238.79828693841722, 252.10638917941014,
						172.17599792336023, 164.53021172396635,
						214.04553826084884, 229.25063088218587,
						238.8631837801762, 143.14839813875633,
						225.7291815787268 },
				{ 242.66867651667408, 243.8233967093607, 172.17599792336023,
						276.12800848533595, 183.42890297302816,
						199.7460718614817, 195.58830693339644,
						247.16716986662317, 147.80326146992053,
						259.65206359770144 },
				{ 201.9984143956534, 179.7335037535733, 164.53021172396635,
						183.42890297302816, 189.05107074159667,
						137.04102246055717, 168.90017069161368,
						201.00871290778102, 162.25509007547865,
						191.80611907087564 },
				{ 264.79728580816914, 301.24657950113163, 214.04553826084884,
						199.7460718614817, 137.04102246055717,
						285.2014499050174, 253.43053326542298,
						295.08473098718827, 141.383796516241,
						236.08783087776368 },
				{ 244.70758709751254, 292.04015276608663, 229.25063088218587,
						195.58830693339644, 168.90017069161368,
						253.43053326542298, 285.4412204318835,
						306.98782584865205, 149.781297889787, 256.319877980762 },
				{ 355.510035881079, 355.10878527145246, 238.8631837801762,
						247.16716986662317, 201.00871290778102,
						295.08473098718827, 306.98782584865205,
						409.2372884729042, 184.83271150180374, 292.46155634966 },
				{ 212.10849080160645, 163.82866495088206, 143.14839813875633,
						147.80326146992053, 162.25509007547865,
						141.383796516241, 149.781297889787, 184.83271150180374,
						188.55249085941537, 172.96090676392234 },
				{ 252.02375064426334, 291.0402125920667, 225.7291815787268,
						259.65206359770144, 191.80611907087564,
						236.08783087776368, 256.319877980762, 292.46155634966,
						172.96090676392234, 373.6153788120116 } };
		Belief b = new Belief(mu, mvar, Sigma);
		try {
			KG kg = new KG(b);
			kg.Start(mu.length);
			/*
			 * double[] logKG1 = kg.GetLogKG(); System.out.println("logKG1: " +
			 * Arrays.toString(logKG1) + "\n");
			 * System.out.println("sigmatilde: "); for (int i = 0; i <
			 * mu.length; i++)
			 * System.out.println(Arrays.toString(b.sigmatilde(i)));
			 */

			kg.RecordMeasurement(1, 0.1);
			/*
			 * double[] logKG2 = kg.GetLogKG(); System.out.println("logKG2: " +
			 * Arrays.toString(logKG2) + "\n"); System.out.println("mu: " +
			 * Arrays.toString(kg.belief.mu()) + "\n");
			 * System.out.println("Sigma: " + kg.belief.Sigma() + "\n");
			 */

			System.out.println("test3: PASS");
			return true;
		} catch (Exception e) {
			System.out.println("test3: Exception thrown: FAIL");
			return false;
		}
	}

	public static boolean test4() {
		// defining initial variables
		int M = 5;
		double b = 10.0;
		CovMatrix Sigma = new CovMatrix(M, 0.0, b);
		double[][] Sigma0 = Sigma.toArray();
		Random rand = new Random();
		double[] mu = new double[M];
		double[] mvar = new double[M];
		for (int i = 0; i < M; i++) {
			mu[i] = Math.round(100 * rand.nextGaussian()) / 100.0;
			mvar[i] = Math.round(100 * rand.nextDouble() * b) / 100.0;
			for (int j = 0; j < M; j++) {
				Sigma0[i][j] = Math.round(100 * Sigma0[i][j]) / 100.0;
			}
		}

		// run trial
		try {
			Belief b0 = new Belief(mu, mvar, Sigma0);
			KG kg = new KG(b0);
			kg.Start(M);

			System.out.println("mu_0: \n" + Arrays.toString(mu));
			System.out.println("\nmvar_0: \n" + Arrays.toString(mvar) + "\n");
			System.out.println("Sigma_0: \n");
			for (int i = 0; i < M; i++)
				System.out.println(Arrays.toString(Sigma0[i]));
			// System.out.println("Sigma_0: \n" + kg.belief0.Sigma());
			System.out.println("logKG_0: \n" + Arrays.toString(kg.GetLogKG())
					+ "\n");

			int x = 2;
			double obs = 0.1;
			System.out.printf("x: %d, obs: %g\n\n", x, obs);

			kg.RecordMeasurement(x, obs);
			System.out.println("mu_1: \n" + Arrays.toString(kg.belief.mu())
					+ "\n");
			double[][] Sigma1 = kg.belief.Sigma().toArray();
			System.out.println("Sigma_1: \n");
			for (int i = 0; i < M; i++)
				System.out.println(Arrays.toString(Sigma1[i]));
			// System.out.println("Sigma_1: \n" + kg.belief.Sigma());
			System.out.println("logKG_1: \n" + Arrays.toString(kg.GetLogKG())
					+ "\n");

			// System.out.println("test4: PASS");
			return true;
		} catch (Exception e) {
			System.out.println("test4: FAIL");
			return false;
		}
	}
}
