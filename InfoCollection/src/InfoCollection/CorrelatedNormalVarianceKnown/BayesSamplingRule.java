package InfoCollection.CorrelatedNormalVarianceKnown;

import InfoCollection.util.MathPF;

/*
 * Most sampling rules are bayesian sampling rules, in that they maintain a
 * belief which they update through time.  This class care of the Bayesian
 * updating and choosing the implementation decision.  Concrete policy classes
 * will subclass from this class, and will add the GetMeasurementDecision
 * function.
 *
 * PF: there is some question as to whether this would be better if it were
 * split into an interface, and a concrete class implementing the interface,
 * and the concrete class would have a constructor that you could just call
 * whose argument was an interface with a GetMeasurementDecision(Belief b)
 * method in it.  Then the constructor would make a BayesSamplingRule from that
 * method just by calling it with the current belief.
 */
public abstract class BayesSamplingRule implements InfoCollection.SamplingRule {
	/*
	 * belief0 is the belief we started with. If it is non-null, that is our
	 * belief. If it is null, then we think of it as meaning we know nothing,
	 * including the value of M. with no idea about M. When M is passed in
	 * Start() we will fix M and choose a noninformative prior.
	 * 
	 * belief is the current belief in the problem we are measuring. It is reset
	 * by Start().
	 */
	Belief belief0;
	Belief belief;

	/*
	 * Constructors. One constructor starts the policy with a null belief0,
	 * which indicates we don't know M. When Start(desiredM) is called, a
	 * noninformative prior with the correct number of alternatives will be
	 * chosen.
	 * 
	 * The other constructor starts with a fixed, usually informative, belief
	 * for a prior. This belief has a fixed number of alternatives, and if
	 * Start(desiredM) is called and desiredM does not match this number of
	 * alternatives, then an exception will be thrown.
	 */
	public BayesSamplingRule() {
		belief0 = null;
		belief = null;
	}

	public BayesSamplingRule(Belief b) {
		belief0 = new Belief(b);
		belief = null;
	}

	/*
	 * Informs the policy that we are starting the game with the time-0 prior
	 * belief.
	 */
	public void Start(int desiredM) throws Exception {
		if (desiredM == belief0.M())
			belief = new Belief(belief0);
		else
			throw new Exception("belief0 and desiredM don't match.");
	}

	/*
	 * Determines x^n, i.e. the measurement decision at time n.
	 */
	// public abstract int GetMeasurementDecision();
	public void RecordMeasurement(int x, double y) {
		belief.Update(x, y);
	}

	/*
	 * Returns the optimal implementation decision, which is an integer between
	 * 1 and M. The optimal implementation decision is the same across all
	 * measurement policies. This function should only be called at the terminal
	 * time N.
	 */
	public int GetImplementationDecision() {
		return MathPF.argmax(this.belief.mu());
		// XXX What should I do on a tie? I should randomly choose
		// among the argmax set.
	}

	/* Get the number of times that alternative x has been measured. */
	public int GetNumMeasurements(int x) {
		double num = belief.EffectiveNum(x) - belief0.EffectiveNum(x);
		int num_int = (int) num;
		assert (num == num_int);
		return num_int;
	}

	/*
	 * These are just convenience functions, since the same information can be
	 * derived from GetBelief(). They can only be called after Start is called.
	 * Otherwise they will generate a null exception.
	 */
	// public double mu(int x) { return belief.mu[x]; }
	// public double beta(int x) { return belief.beta[x]; }
	public int M() {
		return belief.M();
	}

	public Belief GetBelief() {
		return belief;
	}

	/* Returns true if the decision x is an exploitive one. */
	public boolean IsExploitive(int x) {
		return belief.mu()[x] == MathPF.max(belief.mu());
	}

	// Please also implement toString.
	// public abstract String toString();
}