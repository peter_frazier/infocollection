/*
 * Analogous to ArrayOutOfBoundsException
 * Written by Will Manning on 6.16.09
 */

package InfoCollection.CorrelatedNormalVarianceKnown;

@SuppressWarnings("serial")
public class MatrixIndexOutOfBoundsException extends
		ArrayIndexOutOfBoundsException {
}