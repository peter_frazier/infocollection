/*
 * Symmetric Matrix data type
 * Memory-efficient representation of a Symmetric Matrix
 * Implements get() and set() methods
 * 
 * Written by Will Manning and Gerald van den Berg
 */

package InfoCollection.CorrelatedNormalVarianceKnown;

public class Symmatrix {
	private int M; // dimension
	private double[][] tri; // triangular array to represent matrix

	// constructor for empty matrix
	public Symmatrix(int M) {
		this.M = M;
		this.tri = new double[this.M][];
		for (int i = 0; i < this.M; i++) {
			this.tri[i] = new double[i + 1];
		}
	}

	// random constructor -- values are uniformly distributed on [a,b], where a
	// < b
	public Symmatrix(int M, double a, double b) {
		this.M = M;

		// initialize the triangular array that represents the matrix
		this.tri = new double[this.M][];
		for (int i = 0; i < this.M; i++) {
			this.tri[i] = new double[i + 1];
		}

		// assure that a <= b
		if (a > b) {
			double temp = a;
			a = b;
			b = temp;
		}

		for (int i = 0; i < M; i++) {
			for (int j = 0; j <= i; j++) {
				this.tri[i][j] = a + Math.random() * (b - a);
			}
		}
	}

	// constructor in full generality
	public Symmatrix(double[][] vals) {
		// SymMatrix represents MxM square matrix
		this.M = vals.length;

		// check that the given array is square
		for (int i = 0; i < this.M; i++) {
			if (vals[i].length != this.M)
				throw new IllegalArgumentException("Symmatrix must be square.");
		}

		// initialize the triangular array that represents the matrix
		this.tri = new double[this.M][];
		for (int i = 0; i < this.M; i++) {
			this.tri[i] = new double[i + 1];
		}

		// fill triangular array
		for (int i = 0; i < this.M; i++) {
			for (int j = 0; j <= i; j++) {
				if (vals[i][j] == vals[j][i])
					this.tri[i][j] = vals[i][j];
				else
					throw new IllegalArgumentException(
							"Symmatrix must be symmetric.");
			}
		}
	}

	public static Symmatrix identity(int M) {
		Symmatrix sym = new Symmatrix(M);
		for (int i = 0; i < M; i++)
			sym.set(i, i, 1);
		return sym;
	}

	// deep copy of this Symmatrix
	@Override
	public Symmatrix clone() {
		Symmatrix sym = new Symmatrix(this.M);
		sym.tri = this.tri.clone();
		return sym;
	}

	public int M() {
		return this.M;
	}

	// returns item at row i, column j in matrix
	public double get(int i, int j) {
		if (i >= this.M || j >= this.M)
			throw new MatrixIndexOutOfBoundsException();
		else if (j < i)
			return this.tri[i][j];
		return this.tri[j][i];
	}

	// sets row i, column j to item
	public void set(int i, int j, double val) {
		if (i >= this.M || j >= this.M)
			throw new MatrixIndexOutOfBoundsException();
		else if (j < i)
			this.tri[i][j] = val;
		else
			this.tri[j][i] = val;
	}

	// returns a full (square) array representation of the symmatrix
	public double[][] toArray() {
		double[][] array = new double[this.M][this.M];
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				try {
					array[i][j] = this.get(i, j);
				} catch (Exception e) {
					// Impossible
					System.out
							.println("Symmatrix.getArray() threw an Exception");
					assert (false);
					return null;
				}
			}
		}
		return array;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.M; i++) {
			for (int j = 0; j < this.M; j++) {
				try {
					sb.append(this.get(i, j));
					sb.append("  ");
				} catch (Exception e) {
					return "toString() failure";
				}
			}
			sb.append('\n');
		}
		return sb.toString();
	}

	private static boolean isValid(double[][] vals) {
		int M = vals.length;

		// check that the given array is square
		for (int i = 0; i < M; i++) {
			if (vals[i].length != M)
				return false;
		}

		// fill triangular array
		for (int i = 0; i < M; i++) {
			for (int j = 0; j <= i; j++) {
				if (vals[i][j] != vals[j][i])
					return false;
			}
		}

		return true;
	}

	// test client
	public static void main(String[] args) throws Exception {
		System.out.println("Testing...");
		System.out.println("***********************************************");

		boolean pass = true;
		pass = test1() && pass;
		pass = test2() && pass;
		pass = test3() && pass;
		pass = test4() && pass;
		pass = test5() && pass;

		if (pass)
			System.out.println("Symmatrix: PASS");
		else
			System.out.println("Symmatrix: FAIL");
	}

	// tests toString() method
	public static boolean test1() {
		double[][] id = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		try {
			Symmatrix sym = new Symmatrix(id);
			String s = sym.toString();
			if (s.equals("toString() failure")) {
				System.out.println("test1: toString(): FAIL");
				return false;
			}
			System.out.println("test1: PASS");
			return true;
		} catch (Exception e) {
			System.out.println("test1: Initialization: FAIL");
			return false;
		}
	}

	// Tests get() & set() methods
	public static boolean test2() {
		double[][] vals = { { 1.0, 2.0, 3.0 }, { 2.0, 1.0, 4.0 },
				{ 3.0, 4.0, 1.0 } };
		try {
			Symmatrix sym = new Symmatrix(vals);
			try {
				if (sym.get(1, 2) != 4.0) {
					System.out.println("test2: Get: FAIL");
					return false;
				}
				sym.set(1, 2, 5.0);
				if (sym.get(1, 2) != 5.0) {
					System.out.println("test2: Set: FAIL");
					return false;
				}
			} catch (Exception e) {
				System.out.println("test2: Set/Get FAIL");
				return false;
			}
		} catch (Exception e) {
			System.out.println("test2: Initialization FAIL");
			return false;
		}
		System.out.println("test2: PASS");
		return true;
	}

	// Tests bounds checking measures
	public static boolean test3() {
		double[][] vals = { { 1, 2, 3 }, { 2, 1, 4 }, { 3, 4, 1 } };
		try {
			Symmatrix sym = new Symmatrix(vals);
			try {
				sym.get(3, 2);
			} catch (MatrixIndexOutOfBoundsException m) {
				System.out.println("test3: PASS");
				return true;
			}
			System.out.println("test3: MatrixOutOfBoundsException FAIL");
			return false;
		} catch (Exception e) {
			System.out.println("test3: Initialization: FAIL");
			return false;
		}
	}

	// Creates a random Symmatrix, prints it out, converts it to array form,
	// and checks that the array is valid
	public static boolean test4() {
		int M = 5;

		Symmatrix sym = new Symmatrix(M, 0, 10);
		double[][] array = sym.toArray();
		System.out.println("test4: ... ");
		System.out.println(sym.toString());
		System.out.println();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				sb.append(array[i][j]);
				sb.append("  ");
			}
			sb.append('\n');
		}

		System.out.println(sb.toString());

		if (Symmatrix.isValid(array)) {
			System.out.println("test4: PASS");
			return true;
		} else {
			System.out.println("test4: FAIL");
			return false;
		}
	}

	// tests the identity() method
	public static boolean test5() {
		int M = 100;
		Symmatrix id = Symmatrix.identity(M);
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < M; j++) {
				if ((i == j) && (id.get(i, j) != 1)) {
					System.out.println("test5: FAIL");
					return false;
				} else if ((i != j) && (id.get(i, j) != 0)) {
					System.out.println("test5: FAIL");
					return false;
				}
			}
		}
		System.out.println("test5: PASS");
		return true;
	}
}
