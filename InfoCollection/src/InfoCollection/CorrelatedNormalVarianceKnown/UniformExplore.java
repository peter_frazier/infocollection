package InfoCollection.CorrelatedNormalVarianceKnown;

import java.util.Random;

/*
 * This measurement policy is classic pure exploration, which chooses according
 * to a uniform random variable.  It does not look at the belief state to make
 * sampling decisions, but it does need the belief state to make the
 * implementation decision.
 */
public class UniformExplore extends BayesSamplingRule {
	Random rnd;

	public UniformExplore(Random rnd) {
		this.rnd = rnd;
	}

	public UniformExplore() {
		rnd = new Random();
	}

	/* The measurement decision is a uniform random variable. */
	public int GetMeasurementDecision() {
		return rnd.nextInt(M());
	}

	@Override
	public String toString() {
		return "UniformExplore";
	}
}
