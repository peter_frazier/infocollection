package InfoCollection;

import java.util.Random;

/* 
 * This is an abstraction of a Bayesian prior or posterior distribution
 * on the values of a collection of alternatives.
 */
public interface Belief {
	/* Update the belief based on observing value y for alternative x. */
	public void Update(int x, double y) throws Exception;

	/*
	 * Generates true values from the random number generator according to the
	 * prior. Throws an exception if the prior is not informative.
	 */
	public Truth GenerateTruth(Random rnd) throws NotInformativeException;

	/* Returns true if we can call GenerateTruth. */
	public Boolean IsInformative();
}
