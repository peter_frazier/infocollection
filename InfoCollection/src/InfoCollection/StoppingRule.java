package InfoCollection;

/*
 * Tells us when to stop sampling.
 */
public interface StoppingRule {

	/*
	 * To use these three routines, one would create an instance of a
	 * StoppingRule, then call Start() with M set to the number of alternatives
	 * being sampled from, and call RecordMeasurement with each measurement
	 * seen. Before taking each measurement, call ShouldStop() to see whether
	 * this should be the last measurement. This operation is very similar to
	 * that of the Policy class.
	 */
	public void Start(int M) throws Exception;

	public void RecordMeasurement(int x, double y) throws Exception;

	public boolean ShouldStop();

	public String toString();
}
