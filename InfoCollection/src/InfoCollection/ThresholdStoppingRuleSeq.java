package InfoCollection;

import java.util.Arrays;

/*
 * Most of the time, a stopping rule just has a threshold, and the way that we
 * want to implement a StoppingRuleSeq is by having an array of thresholds, and
 * then having Next() just move to the next threshold in the array.  This class
 * allows doing this in a generic array.
 */
public class ThresholdStoppingRuleSeq<T> implements StoppingRuleSeq {
	int i; // current parameter being used.
	T thresholds[];
	ThresholdStoppingRule<T> rule;

	public ThresholdStoppingRuleSeq(T thresholds[],
			ThresholdStoppingRule<T> rule) {
		// PF: clone is dangerous. Make sure T is shallow enough to
		// clone arrays easily.
		this.thresholds = thresholds.clone();
		this.rule = rule;
		// I should also check that the thresholds are in the right order.
		i = 0;
	}

	public void Start(int M) throws Exception {
		i = 0;
		rule.SetThreshold(thresholds[i]);
		rule.Start(M);
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		rule.RecordMeasurement(x, y);
	}

	public boolean ShouldStop() {
		return rule.ShouldStop();
	}

	public boolean Next() {
		if (i == Length() - 1)
			return false;
		// else
		rule.SetThreshold(thresholds[++i]);
		// System.out.println("Moved to threshold " + i);
		return true;
	}

	public int Length() {
		return thresholds.length;
	}

	@Override
	public String toString() {
		return rule.Name() + Arrays.toString(thresholds);
	}
}
