package InfoCollection.IndependentNormalVarianceKnown;

import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.NormalDistQuick;
import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;

/*
 * This is the OCBA-EOC procedure from He, Chick & Chen 2007.  It is optimized
 * for speed and only supports the special case: m=1, tau=1, n0=0.  It can
 * support any initial belief.  
 *
 * Note about n0.  n0=0 makes sense for an informative prior, but the way it
 * handles non-informative priors is a little special.  Even though the HeChCh07
 * procedure isn't well defined for n0=0 and the non-informative prior, this
 * implementation takes the first measurement in round-robin fashion from the
 * first non-informative alternative.  This is just like having n0=1 in the
 * special case of the non-informative prior.
 *
 * Historical note:
 * This was HeChCh07c in the old version of the code, which distinguished it
 * from HeChCh07a, now HeChCh07Simple, and from HeChCh07b.  It is the same
 * functionality than HeChCh07b (now gone), but faster because it only
 * recomputes f(.) values when the arguments change, It also uses a more
 * accurate implementation of the f() function.
 */

public class HeChCh07Fast extends BayesSamplingRule {
	int b;
	double[] D, s2bi, s2bii, s2bib, fbi, fbii, fbib;
	int fversion;
	boolean noninformative; // true if the current belief is noninformative.

	/*
	 * Constructor: The argument fversion selects which version of the normal
	 * cdf to use. fversion=0 means use the accurate but slow version from
	 * umontreal. fversion=1 means use the quick version from umontreal.
	 */
	public HeChCh07Fast(Belief b, int fversion) throws Exception {
		super(b);
		this.fversion = fversion;
	}

	/*
	 * Evaluates the function (a,b) -> Exp[(a+bZ)^+], where Z is a standard
	 * normal random variable, b is strictly positive, and a is any real number.
	 * It is related to Normal.Psi(), since bf(a,b) is b*Psi(-a/b).
	 */
	public double bf(double a, double b) {
		/*
		 * We check the case b == 0 explicitly so that we don't have to worry
		 * about dividing by 0. When b == 0, the appropriate limiting z value is
		 * -Infinity, so bf(a/0) = 0, even when a = 0.
		 */
		if (b == 0)
			return 0;
		/*
		 * We check the case b == +infinity explicitly. In this case, a/b = 0,
		 * no matter what a is, f(0) is strictly positive, so b*f(0) is
		 * +infinity. This is true even if a is NaN.
		 */
		if (b == Double.POSITIVE_INFINITY)
			return Double.POSITIVE_INFINITY;
		double ans = 0, z = a / b;
		switch (fversion) {
		case 0:
			ans = a * NormalDist.cdf01(z) + b * NormalDist.density(0, 1, z);
			break;
		case 1:
			ans = a * NormalDistQuick.cdf01(z) + b
					* NormalDist.density(0, 1, z);
			break;
		}
		if (ans < 0)
			ans = 0;
		return ans;
	}

	/*
	 * Recompute the f and D values for every alternative. Assumes that b and
	 * the sigmas s2* are correct.
	 */
	public void RecomputeFDs() {
		D[b] = 0;
		for (int i = 0; i < M(); i++) {
			if (i == b)
				continue;
			fbi[i] = bf(belief.mu[b] - belief.mu[i], s2bi[i]);
			fbii[i] = bf(belief.mu[b] - belief.mu[i], s2bii[i]);
			fbib[i] = bf(belief.mu[b] - belief.mu[i], s2bib[i]);
			D[i] = fbii[i] - fbi[i];
			D[b] += fbib[i] - fbi[i];
		}
	}

	/*
	 * Remcompute the f and D values based on the knowledge that we measured
	 * alternative i, which was different from b, and it did not overtake b.
	 * Assumes that b and the s2* are correct.
	 */
	public void RecomputeFD(int i) {
		/*
		 * Remove the contribution that alternative i made to D[b] before the
		 * change.
		 */
		D[b] -= fbib[i] - fbi[i];

		fbi[i] = bf(belief.mu[b] - belief.mu[i], s2bi[i]);
		fbii[i] = bf(belief.mu[b] - belief.mu[i], s2bii[i]);
		fbib[i] = bf(belief.mu[b] - belief.mu[i], s2bib[i]);
		D[i] = fbii[i] - fbi[i];

		/*
		 * Add the new contribution that alternative i now makes to D[b] after
		 * the change.
		 */
		D[b] += fbib[i] - fbi[i];
	}

	/*
	 * Assumes that b is correct, and recomputes the sigma values s2bi, s2bii,
	 * s2bib for every i except b.
	 */
	public void RecomputeSigmas() {
		for (int i = 0; i < M(); i++) {
			if (i == b) // Don't need to compute s2*[b].
				continue;
			RecomputeSigma(i);
		}
	}

	/*
	 * Recompute sigma values s2bi, s2bii, and s2bib for a single alternative i.
	 * Assumes that b is correct.
	 */
	public void RecomputeSigma(int i) {
		s2bi[i] = Math.sqrt(1 / belief.beta[b] + 1 / belief.beta[i]);
		s2bii[i] = Math.sqrt(1 / belief.beta[b] + 1
				/ (belief.beta[i] + belief.NoiseBeta(i)));
		s2bib[i] = Math.sqrt(1 / belief.beta[i] + 1
				/ (belief.beta[b] + belief.NoiseBeta(b)));
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		D = new double[M()];
		s2bi = new double[M()];
		s2bii = new double[M()];
		s2bib = new double[M()];
		fbi = new double[M()];
		fbii = new double[M()];
		fbib = new double[M()];
		CheckNoninformative();
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		double oldmub = belief.mu[b];
		super.RecordMeasurement(x, y);

		if (noninformative) {
			/*
			 * Check to see if we are still noninformative after this last
			 * measurement.
			 */
			CheckNoninformative();
			return;
		}

		/*
		 * Now we need to update our internal calculations. There are 4 cases.
		 * The first case is when we measured b and it remained the best. The
		 * second case is when we measured b and the measurement decreased its
		 * mu, so it is no longer the best. The third case is when we measured
		 * i<>b, and didn't change the identity of b with the resulting value.
		 * The fourth case is when we measured i<>b, and did change the identity
		 * of b as a result.
		 */

		if (x == b && belief.mu[b] >= oldmub) {
			/* Case 1: we measured b, and it is still best. */
			RecomputeSigmas(); // since belief.beta[b] changed
			RecomputeFDs();
			// System.out.println("Case 1");
		} else if (x == b) {
			/* Case 2: we measured b, and it is no longer best. */
			b = MathPF.argmax(belief.mu);
			RecomputeSigmas();
			RecomputeFDs();
			// System.out.println("Case 2");
		} else if (belief.mu[x] > belief.mu[b]) {
			/*
			 * Case 3: we didn't measure b, and the one we measured is now the
			 * best.
			 */
			b = x;
			RecomputeSigmas(); // since the identity of b is different
			RecomputeFDs();
			// System.out.println("Case 3");
		} else {
			/*
			 * Case 4: we didn't measure b, and the one we measured didn't
			 * overtake b.
			 */
			RecomputeSigma(x);
			RecomputeFD(x);
			// System.out.println("Case 4");
		}
	}

	/*
	 * Checks to see if we are noninformative or not. If we are, set the flag.
	 * If we are not, fill in the b, s2*, fb*, and D arrays.
	 */
	private void CheckNoninformative() {
		if (!belief.IsIntegrable())
			noninformative = true;
		else {
			/*
			 * We have an informative posterior. Now fill in the state.
			 */
			noninformative = false;
			b = MathPF.argmax(belief.mu);
			RecomputeSigmas();
			RecomputeFDs();
		}
	}

	public int GetMeasurementDecision() {
		if (!noninformative) {
			// System.out.println(Arrays.toString(mu));
			// System.out.println(Arrays.toString(beta));
			// System.out.println(Arrays.toString(D));
			return MathPF.argmin(D);
		} else {
			/*
			 * Find the first entry with 0 precision. There should be one since
			 * the noninformative flag is set.
			 */
			int x;
			for (x = 0; x < M() && belief.beta[x] > 0; x++)
				;
			assert x < M();
			return x;
		}
	}

	@Override
	public String toString() {
		return "HeChCh07 OCBA-EOC (implementation c) m=1 tau=1 n0=0 fversion="
				+ fversion;
	}

	public static void main(String args[]) throws Exception {
		HeChCh07Fast p = new HeChCh07Fast(new Belief(10), 0);
		TestExamples.Example1(p, true);
		// TestExamples.SpeedTest(p);
	}
};
