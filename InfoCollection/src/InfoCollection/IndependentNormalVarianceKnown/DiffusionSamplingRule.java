package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;

import InfoCollection.util.MathPF;

/*
 * This measurement policy deterministically measures the alternative with the
 * largest variance.
 */
public class DiffusionSamplingRule extends BayesSamplingRule {
	int debug = 0;
	double logc;

	public DiffusionSamplingRule(Belief b, double logc) {
		super(b);
		this.logc = logc;
	}

	public int GetMeasurementDecision() {
		/*
		 * Calculate the k=1 diffusion stopping boundary for each
		 * alternative, and then choose to measure the alternative
		 * furthest from the boundary.
		 */
		int i;
		double[] distance = new double[belief.M()];
		double[] delta = Util.Delta(belief);
		for (i=0;i<belief.M();i++) {
		  double barrier = Util.ApproxStdUpperBoundary(logc,belief.beta[i],belief.noiseBeta[i]);
		  distance[i] = barrier - delta[i];
		}

		int x = MathPF.argmax(distance);
		if (debug > 0) {
			System.out.printf("DiffusionSamplingRule belief.mu=%s\n", 
			    Arrays.toString(belief.mu));
			System.out.printf("DiffusionSamplingRule distance=%s\n", 
			    Arrays.toString(distance));
			System.out.printf("DiffusionSamplingRule decision=%d\n", x);
		}
		return x;
	}

	@Override
	public String toString() {
		return "DiffusionSamplingRule logc=" + logc;
	}
}
