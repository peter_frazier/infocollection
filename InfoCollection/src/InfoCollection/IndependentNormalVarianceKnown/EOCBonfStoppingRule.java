package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.ThresholdStoppingRule;
import InfoCollection.ThresholdStoppingRuleSeq;

/*
 * Stop according to the Bonferonni approximation to the expected opportunity
 * cost.  It takes a threshold, and then when the estimated EOC drops below the
 * threshold it stops.
 */
public class EOCBonfStoppingRule extends BayesStoppingRule implements
		ThresholdStoppingRule<Double> {
	double threshold;

	public EOCBonfStoppingRule(Belief b, double threshold) {
		super(b);
		this.threshold = threshold;
	}

	public EOCBonfStoppingRule(double threshold) {
		super();
		this.threshold = threshold;
	}

	public boolean ShouldStop() {
		return (GetCurrentValue() < threshold);
	}

	public Double GetCurrentValue() {
		return belief.EOCBonf();
	}

	public Double GetThreshold() {
		return threshold;
	}

	public void SetThreshold(Double threshold) {
		this.threshold = threshold;
	}

	@Override
	public String toString() {
		return "EOCBonfStoppingRule eoc threshold=" + threshold;
	}

	public String Name() {
		return "EOCBonfStoppingRule";
	}

	/*
	 * Create custom and standard stopping rule sequences for convenience.
	 */
	public static ThresholdStoppingRuleSeq<Double> Seq(double[] threshold) {
		Double[] _threshold = new Double[threshold.length];
		for (int i = 0; i < threshold.length; i++)
			_threshold[i] = threshold[i];
		return new ThresholdStoppingRuleSeq<Double>(_threshold,
				new EOCBonfStoppingRule(threshold[0]));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq() {
		double threshold[] = { .1, .09, .08, .07, .06, .05, .04, .03, .02,
				.015, .01, .009, .008, .007, .006, .005, .004, .003, .002,
				.0015, .001 };
		return Seq(threshold);
	}
}
