package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.util.MathPF;

/*
 * Like LLseqArgmaxAlloc, except it uses the LLc1 instead of the LLck to
 * determine what the optimal budget with which to compute the LL allocation
 * is.
 */
public class LLseqc1 extends BayesSamplingRule {
	int debug = 0;
	int method;
	double c;
	LLc1 llc;

	public LLseqc1(Belief b, double c) {
		super(b);
		method = 0;
		this.c = c;
	}

	public LLseqc1(Belief b, double c, int method) {
		super(b);
		this.c = c;
		this.method = method;
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		llc = new LLc1(belief, c, method); // note that LLc1 copies belief.
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		llc.RecordMeasurement(x, y);
	}

	public int GetMeasurementDecision() {
		double[] alloc;
		alloc = llc.GoodAlloc();
		return MathPF.argmax(alloc);
	}

	public boolean ShouldStop() {
		return (llc.GoodBudget() > 0);
	}

	@Override
	public String toString() {
		return "LLseqc1 c=" + c;
	}

	public String Name() {
		return "LLseqc1";
	}
}
