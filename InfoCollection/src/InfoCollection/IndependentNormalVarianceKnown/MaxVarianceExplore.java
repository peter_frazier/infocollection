package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;

import InfoCollection.util.MathPF;

/*
 * This measurement policy deterministically measures the alternative with the
 * largest variance.
 */
public class MaxVarianceExplore extends BayesSamplingRule {
	int debug = 0;

	public MaxVarianceExplore() {
		super();
	}

	public MaxVarianceExplore(Belief b) {
		super(b);
	}

	public int GetMeasurementDecision() {
		/*
		 * The largest variance is the smallest inverse variance. Ties are
		 * broken by choosing the one with the smaller index.
		 */
		int x = MathPF.argmin(belief.beta);
		if (debug > 0) {
			System.out.printf("MaxVarianceExplore: belief.mu=%s\n", Arrays
					.toString(belief.mu));
			System.out.printf("MaxVarianceExplore: belief.beta=%s\n", Arrays
					.toString(belief.beta));
			System.out.printf("MaxVarianceExplore: decision=%d\n", x);
		}
		return x;
	}

	@Override
	public String toString() {
		return "MaxVarianceExplore";
	}
}
