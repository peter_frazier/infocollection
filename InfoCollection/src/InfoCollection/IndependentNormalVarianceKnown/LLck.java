package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;

import optimization.Fmin;
import optimization.Fmin_methods;
import InfoCollection.util.MathPF;
import InfoCollection.util.Normal;

/*
 * Maintains both an allocation and a budget that is "optimal" given the current
 * belief.  From these can be derived a stopping rule (EOCck) and a sampling
 * rule (LLseq).
 */
public class LLck {
	int numAllocsLeft;
	int maxNumAllocsLeft = 50;
	// To handle large numbers of alternatives, it may be necessary to
	// increase this value.  To compute the value of minOER for k=100
	// (which was computed using llc.ExactNetAllocValue(llc.GoodAlloc())) 
	// in the journal paper version of the 2009 WSC paper with Steve Chick,
	// I increased budgetMax to 20000.  See the note in
	// Sim200904.WSC_Chick/java/P3a.10.16.2009.txt.
	int budgetMax = 10000;
	static int debug = 0;
	int method;

	double c;
	Belief b;
	double good_budget;
	double[] good_alloc;

	// Within this class, this bit is always false.  LLc1, which inherits
	// from this class, changes this bit to true in order change
	// LogGrossAllocValue to compute the value under LLc1 instead of LLck.
	static boolean do_LLc1;

	public LLck(Belief b, double c) {
		this(b, c, 0);
	}

	/*
	 * These are the choices for method: (method 0 is the default) method 0:
	 * first decrement from the last allocation, then do Analytic Search, then
	 * do FibonacciSearch. method 1: always do FibonacciSearch method 2: always
	 * do AnalyticSearch method 3: emulate KGE method 4: always do
	 * BruteForceSearch method 5: first decrement from the last allocation, then
	 * do Analytic Search, then do BruteForceSearch method 6: first decrement
	 * from the last allocation, then do Analytic Search, then do
	 * UpperBoundSearch. method 7: Like method 6, but only allow allocating from
	 * the previous allocation a maximum of maxNumAllocsLeft times. method 8:
	 * Puts an upper bound on maxNumAllocsLeft. Use Analytic Search, then
	 * Unimodal Search. method 9: Puts an upper bound on maxNumAllocsLeft. Use
	 * Analytic Search, then IsolatingUnimodal Search.
	 * 
	 * Note that for one alternative known, one alternative unknown, (i.e. k=1),
	 * method 2 is almost the same as KGEnvelope.
	 */
	public LLck(Belief b, double c, int method) {
		do_LLc1 = false;
		this.b = new Belief(b); // copy it, to avoid its being changed by anyone
								// else.
		this.c = c;
		this.method = method;
		good_alloc = new double[b.M()];
		// if (!b.IsInformative())
		// throw new NotInformativeException();
		if (method == 1)
			FibonacciSearch();
		else if (method == 2)
			AnalyticSearch();
		else if (method == 3)
			KGEEmulator();
		else if (method == 4)
			BruteForceSearch();
		else if (method == 5) {
			if (!AnalyticSearch())
				BruteForceSearch();
		} else if (method == 6) {
			if (!AnalyticSearch())
				UpperBoundSearch();
		} else if (method == 7) {
			numAllocsLeft = maxNumAllocsLeft;
			if (!AnalyticSearch())
				UpperBoundSearch();
		} else if (method == 8) {
			numAllocsLeft = maxNumAllocsLeft;
			if (!AnalyticSearch())
				BrentsSearch();
		} else if (method == 9) {
			numAllocsLeft = maxNumAllocsLeft;
			if (!AnalyticSearch())
				IsolatingUnimodalSearch();
		} else if (method == 10) {
		  	// This method ignores numAllocsLeft and maxNumAllocsLeft.
		  	numAllocsLeft = 0;
			SteveSearch();
		} else {
			if (!AnalyticSearch())
				FibonacciSearch();
		}
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		b.Update(x, y);
		if (method == 1)
			FibonacciSearch();
		else if (method == 2)
			AnalyticSearch();
		else if (method == 3)
			KGEEmulator();
		else if (method == 4)
			BruteForceSearch();
		else if (method == 5) {
			// first try removing one from the good alloc.
			good_alloc[x]--;
			good_budget--;
			if (good_alloc[x] >= 0 && NetAllocValue(good_alloc) > 0)
				return;

			// try reducing the budget by one.
			if (good_budget > 0) {
				good_alloc = Allocation(b, (int) Math.ceil(good_budget));
				if (NetAllocValue(good_alloc) > 0)
					return;
			}

			if (!AnalyticSearch())
				BruteForceSearch();
		} else if (method == 6) {
			/*
			 * The problem with this method=6, when used for both EOCa stopping
			 * and LL sampling, is that UpperBoundSearch finds the largest
			 * budget that merits continuing. Then, if we do LLseq alloctation
			 * with argmax \tau_i as the allocation rule, we end up doing a lot
			 * of allocations without redoing the LL.
			 */
			// first try removing one from the good alloc.
			good_alloc[x]--;
			good_budget--;
			if (good_alloc[x] >= 0 && NetAllocValue(good_alloc) > 0)
				return;

			// try reducing the budget by one.
			if (good_budget > 0) {
				good_alloc = Allocation(b, (int) Math.ceil(good_budget));
				if (NetAllocValue(good_alloc) > 0)
					return;
			}

			if (!AnalyticSearch())
				UpperBoundSearch();
		} else if (method == 7) {

			if (numAllocsLeft > 0) {
				numAllocsLeft--;
				good_alloc[x]--;
				good_budget--;
				if (good_alloc[x] >= 0 && NetAllocValue(good_alloc) > 0)
					return;
			}
			numAllocsLeft = maxNumAllocsLeft;

			// try reducing the budget by one.
			if (good_budget > 0) {
				good_alloc = Allocation(b, (int) Math.ceil(good_budget));
				if (NetAllocValue(good_alloc) > 0)
					return;
			}

			if (!AnalyticSearch())
				UpperBoundSearch();
		} else if (method == 8) {

			if (numAllocsLeft > 0) {
				numAllocsLeft--;
				good_alloc[x]--;
				good_budget--;
				if (good_alloc[x] >= 0 && NetAllocValue(good_alloc) > 0)
					return;
			}
			numAllocsLeft = maxNumAllocsLeft;

			// try reducing the budget by one.
			if (good_budget > 0) {
				good_alloc = Allocation(b, (int) Math.ceil(good_budget));
				if (NetAllocValue(good_alloc) > 0)
					return;
			}

			if (!AnalyticSearch())
				BrentsSearch();
		} else if (method == 9) {

			if (numAllocsLeft > 0) {
				numAllocsLeft--;
				good_alloc[x]--;
				good_budget--;
				if (good_alloc[x] >= 0 && NetAllocValue(good_alloc) > 0)
					return;
			}
			numAllocsLeft = maxNumAllocsLeft;

			// try reducing the budget by one.
			if (good_budget > 0) {
				good_alloc = Allocation(b, (int) Math.ceil(good_budget));
				if (NetAllocValue(good_alloc) > 0)
					return;
			}

			if (!AnalyticSearch())
				IsolatingUnimodalSearch();
		} else if (method == 10) {
			SteveSearch();
		} else {
			// First see if our good allocation has positive net
			// value after removing what we measured from it.
			good_alloc[x]--;
			good_budget--;
			if (good_alloc[x] >= 0 && NetAllocValue(good_alloc) > 0) {
				return;
			}

			// If not, try to find one from scratch.
			if (!AnalyticSearch())
				FibonacciSearch();
		}
	}

	/* Call this function to mark that we did not find a good budget. */
	private void SetNoGoodBudget() {
		good_budget = 0;
		for (int i = 0; i < b.M(); good_alloc[i++] = 0)
			;
	}

	private void SetGoodBudget(double[] alloc) {
		good_budget = 0;
		for (int i = 0; i < b.M(); i++) {
			good_budget += alloc[i];
			good_alloc[i] = alloc[i];
		}
	}

	public boolean AnalyticSearchKGE() {
		// Try the various tangent points from the analytic
		// approximation.
		double[] tangents = Util.TangentPoint_Analytic(b);
		for (int i = 0; i < b.M(); i++) {
			double budget = tangents[i];
			if (budget == 0)
				continue;

			// These 3 lines replicate the allocations that KGEnvelope
			// considers.
			double[] alloc = new double[b.M()];
			for (int j = 0; j < b.M(); alloc[j++] = 0)
				;
			alloc[i] = budget;

			if (NetAllocValue(alloc) > 0) {
				SetGoodBudget(alloc);
				return true;
			}
		}
		SetNoGoodBudget();
		return false; // couldn't find one.
	}

	public boolean AnalyticSearch() {
		// Try the various tangent points from the analytic
		// approximation.
		double[] tangents = Util.TangentPoint_Analytic(b);
		for (int i = 0; i < b.M(); i++) {
			double budget = tangents[i];
			if (budget == 0)
				continue;

			double[] alloc = Allocation(b, (int) Math.ceil(budget));
			if (NetAllocValue(alloc) > 0) {
				SetGoodBudget(alloc);
				return true;
			}
		}
		SetNoGoodBudget();
		return false; // couldn't find one.
	}

	public boolean KGEEmulator() {
		// First try the various tangent points from the analytic
		// approximation.
		double[] tangents = Util.TangentPoint_Analytic(b);
		for (int i = 0; i < b.M(); i++) {
			double budget = tangents[i];
			if (budget == 0)
				continue;
			double[] alloc = Allocation(b, (int) Math.ceil(budget));
			if (NetAllocValue(good_alloc) > 0) {
				SetGoodBudget(alloc);
				return true;
			}
		}
		SetNoGoodBudget();
		return false; // couldn't find one.
	}

	public int Method() {
		return method;
	}

	public double GoodBudget() {
		return good_budget;
	}

	public double[] GoodAlloc() {
		return good_alloc;
	}

	public double GoodNetAllocValue() throws Exception {
		return NetAllocValue(good_alloc);
	}

	/*
	 * Performs a golden section search for the optimal budget. public void
	 * GoldenSectionSearch(int lb, int ub) { double lbv = GrossAllocValue(lb);
	 * double ubv = GrossAllocValue(ub);
	 * 
	 * }
	 * 
	 * /* This search tries to isolate a region in which the global maximum
	 * lies, and in which the net value is unimodal. Then it uses Brent's method
	 * to search.
	 * 
	 * To accomplish this, the search assumes that the curve relating net value
	 * (of the LL alloction) to the budget is either unimodal (increasing and
	 * then decreasing), or it dips initially and then increases.
	 * 
	 * It first isolates a region in which it is increasing on the left, and
	 * decreasing on the right.
	 * 
	 * Question: is the value of the LL allocation log-concave? Could this give
	 * us a better search procedure?
	 */
	public void IsolatingUnimodalSearch() {
		int x, prev;
		int ub, lb, budget, budget_prev;

		/*
		 * First, test pairs of points in sequence, looking for a point where
		 * the net value is increasing.
		 */
		budget_prev = 1;
		budget = 1;
		while (true) {
			if (budget > budgetMax) {
				SetNoGoodBudget();
				return;
			}

			// test a pair of points.
			double v1 = NetAllocValue(budget);
			double v2 = NetAllocValue(budget + 1);
			// we found a point where net value is increasing.
			if (v2 > v1) {
				lb = budget;
				break;
			}

			// budget = budget * 2; // or could do doubling search
			int tmp = budget_prev + budget;
			budget_prev = budget;
			budget = tmp;
		}

		/*
		 * Now test pairs of points, looking for a point where the net value is
		 * decreasing.
		 */
		prev = 1;
		x = 1;
		while (true) {
			budget = lb + x;
			if (budget > budgetMax) {
				ub = budgetMax;
				break;
			}

			// test a pair of points.
			double v1 = NetAllocValue(budget);
			double v2 = NetAllocValue(budget + 1);
			// we found a point where net value is increasing.
			if (v2 < v1) {
				ub = budget;
				break;
			}

			// budget = budget * 2; // or could do doubling search
			int tmp = prev + x;
			prev = x;
			x = tmp;
		}

		/* Find the best budget between lb and ub. */
		double tol = 1;
		FminMethod f = new FminMethod(b, c);
		good_budget = Math.floor(Fmin.fmin(lb, ub, f, tol));

		/* Get the allocation corresponding to the good budget. */
		good_alloc = Allocation(b, (int) good_budget);
		double good_val = NetAllocValue(good_alloc);
		if (good_val <= 0)
			SetNoGoodBudget();
	}

	/*
	 * Performs a search using Brent's method (implemented in optimization.Fmin)
	 * between 1 and the upper bound found by UpperBoundSearch(). Note that our
	 * function is not necessarily unimodal, so this search may find a local
	 * rather than global optimum.
	 */
	public void BrentsSearch() {
		double lb = 1; // lower bound is 1.

		/*
		 * Get an upper bound. UpperBoundSearch() puts such an upper bound in
		 * good_budget.
		 */
		UpperBoundSearch();
		if (good_budget == 0) // no good budgets
			return;
		double ub = good_budget;

		/* Search between lower and upper bound using Brent's method. */
		double tol = 1;
		FminMethod f = new FminMethod(b, c);
		good_budget = Math.floor(Fmin.fmin(lb, ub, f, tol));

		/* Get the allocation corresponding to the good budget. */
		good_alloc = Allocation(b, (int) good_budget);
		double good_val = NetAllocValue(good_alloc);
		if (good_val <= 0)
			SetNoGoodBudget();
	}

	public class FminMethod implements Fmin_methods {
		Belief b;
		double c;

		public FminMethod(Belief b, double c) {
			this.b = b;
			this.c = c;
		}

		public double f_to_minimize(double x) {
			int budget = (int) Math.floor(x);
			return -NetAllocValue(budget, b, c);
		}
	}

	/*
	 * Do a line search for a budget that has positive net value. This updates
	 * the optimal budget, the optimal gross value, the optimal net value, and
	 * the optimal allocation. Search by looking at points in a fibonacci
	 * sequence until we find one.
	 */
	public void FibonacciSearch() {
		double v = 0;
		int budget_prev = 1;
		int budget = 1;
		if (debug > 0) {
			System.out.printf("Starting FibonacciSearch, c=%f\n", c);
			System.out.println(b);
		}

		while (budget <= budgetMax) {
			double[] alloc = Allocation(b, budget);
			v = NetAllocValue(alloc);
			if (debug > 1)
				System.out.printf("FibonacciSearch: budget=%d v=%f alloc=%s\n",
						budget, v, Arrays.toString(alloc));
			if (v >= 0) {
				SetGoodBudget(alloc);
				if (debug > 0) {
					double[] nstar = Util.TangentPoint(b);
					System.out.printf(
					    "Ending FibonacciSearch, good_budget=%f, v=%f, good_alloc=%s nstar=%s",
					    good_budget, v, Arrays.toString(alloc),
					    Arrays.toString(nstar));
				}
				return;
			}
			// Fibanocci increment
			int tmp = budget_prev + budget;
			budget_prev = budget;
			budget = tmp;
		}
		SetNoGoodBudget();
		if (debug > 0) {
			double[] nstar = Util.TangentPoint(b);
			System.out.printf(
			    "Ending line search, good_budget=0 budgetMax=%d nstar=%s\n",
			    budgetMax, Arrays.toString(nstar));
			if (MathPF.max(nstar) > budgetMax)
				System.out.println(
				    "WARNING in FibonacciSearch(): budgetMax<max(nstar).  You may want to increase budgetMax.");
		}
	}

	/*
	 * Also, look at ALL of the budgets up to the maximum to find the best,
	 * rather than just stopping when we find one with net value > 0.
	 */
	public void UpperBoundSearch() {
		double grossval;
		int budget;

		/*
		 * The gross benefit of an allocation is bounded above by b.EOCBayes()
		 * (the value of perfect information). Thus, if b.EOCBayes() - c*beta <
		 * 0, the best allocation with budget beta does not merit continuing.
		 * Thus, we need not consider any budget greater than b.ExpMax() / c.
		 */
		grossval = b.EOCBayes();
		budget = (int) Math.floor(grossval / c);

		if (debug>0) {
		  System.out.printf("mu=%s\n", Arrays.toString(b.mu));
		  System.out.printf("beta=%s\n", Arrays.toString(b.beta));
		}

		while (budget > 0) {
			double[] alloc = Allocation(b, budget);
			grossval = GrossAllocValue(alloc);
			double netval = grossval - c * budget;
			if (debug > 1)
				System.out.printf(
				    "UpperBoundSearch: budget=%d grossval=%f netval=%f alloc=%s\n",
				    budget, grossval, netval, Arrays.toString(alloc));

			if (netval > 0) {
				SetGoodBudget(alloc);
				if (debug > 0)
					System.out.printf(
					    "Ending UpperBoundSearch, good_budget=%f, netval=%f, good_alloc=%s\n",
					    good_budget, netval, Arrays.toString(good_alloc));
				return;
			}

			/*
			 * By the same argument as above, we need not consider any budget
			 * greater than grossval/c.
			 */
			budget = MathPF.min(budget - 1, (int) Math.floor(grossval / c));
		}
		SetNoGoodBudget();
		if (debug > 0)
			System.out.printf("Ending UpperBoundSearch, good_budget=0\n");
	}

	/*
	 * Do a brute force search for the best budget. Also, look at ALL of the
	 * budgets up to the maximum to find the best, rather than just stopping
	 * when we find one with net value > 0.
	 */
	public void BruteForceSearch() {
		double bestv = 0;
		int budget;
		// int bestbudget = 0;
		double[] bestalloc = null;

		/*
		 * The gross benefit of an allocation is bounded above by b.EOCBayes()
		 * (the value of perfect information). Thus, if b.EOCBayes() - c*beta <
		 * 0, the best allocation with budget beta does not merit continuing.
		 * Thus, we need not consider any budget greater than b.ExpMax() / c.
		 */
		double thisBudgetMax = b.EOCBayes() / c;
		if (thisBudgetMax > budgetMax)
			thisBudgetMax = budgetMax; // truncate.

		if (debug > 0) {
			System.out.printf(
					"Starting brute force search, c=%f budgetMax=%f\n", c,
					thisBudgetMax);
			System.out.println(b);
		}

		for (budget = 1; budget < thisBudgetMax; budget++) {
			double[] alloc = Allocation(b, budget);
			double v = NetAllocValue(alloc);
			if (debug > 1)
				System.out.printf(
						"BruteForceSearch: budget=%d v=%f alloc=%s\n", budget,
						v, Arrays.toString(alloc));
			if (v >= bestv) {
				bestv = v;
				// bestbudget = budget;
				bestalloc = alloc;
				// continue searching
			}
		}
		if (bestv > 0)
			SetGoodBudget(bestalloc);
		else
			SetNoGoodBudget();
		if (debug > 0)
		  System.out.printf(
		      "Ending BruteForceSearch, good_budget=%f, v=%f, good_alloc=%s\n",
		      good_budget, bestv, Arrays.toString(good_alloc));
	}

	/* 
	 * Try a list of budgets, and take the FIRST one that merits
	 * continuing.
	 */
	public void SteveSearch() {
		/*
		 * The variable budget is the list of budgets to try.  In
		 * matlab, it is floor([3 5 10.^(1.05:.1:3.0) 1 7 9]).
		 */
		int[] budgets = {3,5,11,14,17,22,28,35,44,56,70,89,112,141,177,223,281,354,446,562,707,891,1,7,9};
		int i;
		for (i=0; i<budgets.length; i++) {
		  double[] alloc = Allocation(b, budgets[i]);
		  double v = NetAllocValue(alloc);
		  if (debug > 1)
		    System.out.printf(
			"SteveSearch: budget=%d v=%f alloc=%s\n", 
			budgets[i], v, Arrays.toString(alloc));
		  if (v>=0) {
			SetGoodBudget(alloc);
			break;
		  }
		}
		if (i==budgets.length)
		  SetNoGoodBudget();

		if (debug>0)
		  DebugSteveSearch();
	}

	public void DebugSteveSearch() {
		// This is the list of budgets from SteveSearch(), in sorted
		// order, plus a few others at the large end.
		int[] budgets = {1,3,5,7,9,10,14,17,22,28,35,44,56,70,89,112,141,177,223,281,354,446,562,707,891,1000,2000,5000};
		int i;
		System.out.printf("mu=%s\n", Arrays.toString(b.mu));
		System.out.printf("beta=%s\n", Arrays.toString(b.beta));
		for (i=0; i<budgets.length; i++) {
		  double[] alloc = Allocation(b, budgets[i]);
		  double v = NetAllocValue(alloc);
		  System.out.printf(
		      "DebugCompareAllocs: argmax(alloc)=%d budget=%d v=%f alloc=%s\n",
			MathPF.argmax(alloc), budgets[i], v, Arrays.toString(alloc));
		}
	}

	/*
	 * Routines for computing values of allocations.
	 */

	/*
	 * By putting all the calls to LL.Allocation in this wrapper routine, I can
	 * do a neat trick where I turn LLck and the classes that use it (EOCa,
	 * LLseq*) into KGEnvelope based routines. I do this by replacing the LL
	 * allocation with the best of the allocations to single alternatives. Then
	 * the gross and net value of the allocation provided by this routine will
	 * then be the maximum of the KG factors. I should then set maxNumAllocsLeft
	 * to 1.
	 */
	public static double[] Allocation(Belief b, int budget) {
		return LL.Allocation(b, budget);
		// return KGEAllocation(b,budget);
	}

	public static double[] KGEAllocation(Belief b, int budget) {
		double[] logq = Util.LogQfactor(b, budget);
		int x = MathPF.argmax(logq);
		double[] alloc = new double[b.M()];
		for (int i = 0; i < alloc.length; alloc[i++] = 0)
			;
		alloc[x] = budget;
		return alloc;
	}

	/*
	 * Calculate the logarithm of the gross (not including sampling costs) value
	 * of an allocation.
	 */
	public static double LogGrossAllocValue(double[] alloc, Belief b, double c) {
		double logval, d, s, s_best, s_i;
		double[] logvalList = new double[b.M()];
		int i, best;

		if (alloc.length != b.M()) {
			System.err.println("Allocation is the wrong length");
			return 0; // wrong allocations have no value.
		}

		best = MathPF.argmax(b.mu);
		s_best = Util.SigmaTilde(b.beta[best], alloc[best] * b.NoiseBeta(best));
		for (i = 0; i < b.M(); i++) {
			if (i == best) {
				logvalList[best] = Double.NEGATIVE_INFINITY;
				continue;
			}
			s_i = Util.SigmaTilde(b.beta[i], alloc[i] * b.NoiseBeta(i));
			s = Math.sqrt(s_best * s_best + s_i * s_i);
			// PF: 11/12/2009.  Fixed a bug with this if statement.
			// Without it, our logvalList[i] is NaN.
			if (s == 0) {
			  logvalList[i] = Double.NEGATIVE_INFINITY;
			  continue;
			}
			d = (b.mu[best] - b.mu[i]) / s;
			logvalList[i] = Math.log(s) + Normal.LogPsi(d);
		}

		if (do_LLc1)
		  logval = MathPF.max(logvalList); // This is the value used by LLc1.  
		else
		  logval = MathPF.log_sum(logvalList); // This is the value used by LLck.

		if (debug > 2)
		  System.out.printf(
		      "LogGrossAllocValue: logval=%f logvalList=%s alloc=%s\n",
		      logval, Arrays.toString(logvalList), Arrays.toString(alloc));
		return logval;
	}

	public static double GrossAllocValue(double[] alloc, Belief b, double c) {
		return Math.exp(LogGrossAllocValue(alloc, b, c));
	}

	public static double GrossAllocValue(int budget, Belief b, double c) {
		double[] alloc = Allocation(b, budget);
		return GrossAllocValue(alloc, b, c);
	}

	/*
	 * Calculate the net value of an allocation, using the Bonferonni
	 * inequality.
	 */
	public static double NetAllocValue(double[] alloc, Belief b, double c) {
		double gross = GrossAllocValue(alloc, b, c);
		double cost = 0;
		for (int i = 0; i < alloc.length; i++)
			cost += alloc[i] * c;
		return gross - cost;
	}

	public static double NetAllocValue(int budget, Belief b, double c) {
		return GrossAllocValue(budget, b, c) - budget * c;
	}

	/*
	 * Does numerical integration in order to compute the "exact" gross value of
	 * an allocation.
	 */
	public static double ExactGrossAllocValue(double[] alloc, Belief b, double c) {
		// double logval;
		double[] s = new double[b.M()];
		int i;

		if (alloc.length != b.M()) {
			System.err.println("Allocation is the wrong length");
			return 0; // the value of incorrect allocations is 0.
		}

		for (i = 0; i < b.M(); i++)
			s[i] = Util.SigmaTilde(b.beta[i], alloc[i] * b.NoiseBeta(i));

		return Normal.ExpMax(b.mu, s) - MathPF.max(b.mu);
	}

	public static double ExactNetAllocValue(double[] alloc, Belief b, double c) {
		double gross = ExactGrossAllocValue(alloc, b, c);
		double cost = 0;
		for (int i = 0; i < alloc.length; i++)
			cost += alloc[i] * c;
		return gross - cost;
	}

	/* Convenience routines. */
	public double LogGrossAllocValue(double[] alloc) {
		return LogGrossAllocValue(alloc, b, c);
	}

	public double GrossAllocValue(int budget) {
		return GrossAllocValue(budget, b, c);
	}

	public double GrossAllocValue(double[] alloc) {
		return GrossAllocValue(alloc, b, c);
	}

	public double NetAllocValue(double[] alloc) {
		return NetAllocValue(alloc, b, c);
	}

	public double NetAllocValue(int budget) {
		return NetAllocValue(budget, b, c);
	}

	public double ExactGrossAllocValue(double[] alloc) throws Exception {
		return ExactGrossAllocValue(alloc, b, c);
	}

	public double ExactNetAllocValue(double[] alloc) throws Exception {
		return ExactNetAllocValue(alloc, b, c);
	}

	public static void main(String args[]) {
		String myclass = "InfoCollection.IndependentNormalVarianceKnown.LLc";
		boolean ok = true;
		System.out.println("Testing " + myclass);
		ok = test1() && ok;
		ok = test2(false) && ok;
		ok = test3() && ok;
		System.out.printf("%s: %s\n", myclass, ok ? "OK" : "FAIL");
	}

	/*
	 * When the allocation is entirely to a single alternative, the value of the
	 * allocation should be the same as the KG factor.
	 */
	public static boolean test1() {
		boolean ok = true;
		// avoid values like beta=1 to catch missing square roots.
		Belief b = new Belief(2, 1, 0.5, 0.5);
		double[] alloc = new double[2];
		alloc[0] = 1;
		alloc[1] = 0;
		LLck llc = new LLck(b, 1);
		try {
			double gross = llc.GrossAllocValue(alloc);
			double[] logkg = Util.LogQfactor(b);
			double kgval = Math.exp(logkg[0]);
			double diff = Math.abs(gross - kgval);
			// exp(logkg[0]) should agree with gross.
			if (diff > 1e-5)
				ok = false;
		} catch (Exception e) {
			System.out.println(e);
			ok = false;
		}
		System.out.printf("test1: %s\n", ok ? "OK" : "FAIL");
		return ok;
	}

	/*
	 * This test checks that GrossAllocValue can handle extreme values of beta
	 * and noisebeta. This test used to fail until I computed GrossAllocValue in
	 * log-space.
	 */
	public static boolean test2(boolean verbose) {
		double[] mu0 = { -30, 0 };
		double[] beta0 = { 1, 1 };
		boolean ok = true;
		double lognoisebeta;
		double[] alloc = { 1, 0 };
		double c = 1;
		for (lognoisebeta = 0; lognoisebeta > -20; lognoisebeta--) {
			double noisebeta = Math.exp(lognoisebeta);
			Belief b = new Belief(mu0, beta0, noisebeta);
			LLck llc = new LLck(b, c);
			try {
				double loggross = llc.LogGrossAllocValue(alloc);
				double[] logkg = Util.LogQfactor(b);
				double diff = Math.abs(loggross - logkg[0]);
				if (diff > 1e-5)
					ok = false;
				if (verbose)
					System.out
							.printf(
									"log(noisebeta)=%f log(GrossAllocValue())=%f LogQfactor()=%f diff=%g\n",
									lognoisebeta, loggross, logkg[0], diff);
			} catch (Exception e) {
				System.out.println(e);
				ok = false;
			}
		}
		System.out.printf("test2: %s\n", ok ? "OK" : "FAIL");
		return ok;
	}

	/*
	 * This test checks a particular belief on which GrossAllocValue and
	 * LogQFactor disagree. On this belief, GrossAlloc had a good_budget of 89
	 * with a log(avg value) of 0.02, and KGEnvelope had an nstar of 76.8, with
	 * a logKGEFactor of -0.006. KGEnvelope had a logKGEFactor of -4.47 at n=89.
	 * The actual maximum is likely at n=99.
	 */
	public static boolean test3() {
		boolean verbose = true;
		boolean ok = true;
		double[] mu0 = { 0, -24871 };
		double[] beta0 = { Double.POSITIVE_INFINITY, 4.5e-9 };
		double noisebeta = 1e-10;
		double[] alloc = new double[2];
		double c = 1;
		for (int n = 1; n < 200; n++) {
			alloc[0] = 0;
			alloc[1] = n;
			Belief b = new Belief(mu0, beta0, noisebeta);
			LLck llc = new LLck(b, c);
			try {
				double loggross = llc.LogGrossAllocValue(alloc);
				double logavg = loggross - Math.log(n);
				double[] logkg = Util.LogQfactor(b, alloc);
				double diff = Math.abs(logavg - logkg[1]);
				if (diff > 1e-5)
					ok = false;
				if (verbose)
					System.out
							.printf(
									"n=%d log(GrossAllocValue/n)=%f LogQfactor()=%f diff=%g\n",
									n, logavg, logkg[1], diff);
			} catch (Exception e) {
				System.out.println(e);
				ok = false;
			}
		}
		System.out.printf("test2: %s\n", ok ? "OK" : "FAIL");
		return ok;
	}
}
