package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;

/*
 * This is a very simple policy.  It chooses that alternative with the largest
 * value of the influence.
 */
public class MaxInfluence extends BayesSamplingRule {

	/*
	 * There is a more efficient way to compute this, in which we remember which
	 * alternative is best. Also, we could cache the value of the sigmatilde
	 * (this is definitely a good idea). We could also cache the influence
	 * itself, since
	 */
	public int GetMeasurementDecision() {
		double[] influence = Util.Influence(belief);
		return (MathPF.argmax(influence));
	}

	@Override
	public String toString() {
		return "MaxInfluence";
	}

	public static void main(String args[]) throws Exception {
		TestExamples.Example1(new MaxInfluence(), true);
	}
};
