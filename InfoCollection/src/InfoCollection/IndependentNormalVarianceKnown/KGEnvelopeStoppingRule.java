package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.ThresholdStoppingRule;
import InfoCollection.ThresholdStoppingRuleSeq;
import InfoCollection.util.MathPF;

public class KGEnvelopeStoppingRule extends BayesStoppingRule implements
		ThresholdStoppingRule<Double> {
	int method;
	static int debug = 0;
	double logc;

	public KGEnvelopeStoppingRule(double logc) {
		super();
		this.logc = logc;
		method = 1;
	}

	public KGEnvelopeStoppingRule(Belief b, double logc) {
		super(b);
		this.logc = logc;
		method = 1;
	}

	public KGEnvelopeStoppingRule(double logc, int method) {
		super();
		this.logc = logc;
		this.method = method;
	}

	public KGEnvelopeStoppingRule(Belief b, double logc, int method) {
		super(b);
		this.logc = logc;
		this.method = method;
	}

	public Double GetThreshold() {
		return logc;
	}

	public void SetThreshold(Double threshold) {
		logc = threshold;
	}

	public Double GetCurrentValue() {
		if (method == 1)
			return GetCurrentValue1();
		else
			return GetCurrentValue2();
	}

	/* A simple way to get the current value. */
	public Double GetCurrentValue1() {
		/*
		 * Calculate the Q-factor for continuing, which is the maximum of the
		 * Q-factors corresponding to measuring each alternative. Actually, we
		 * calculate the logarithms because these can be small numbers.
		 */
		return MathPF.max(Util.EnvelopeLogQfactor(belief));
		// return MathPF.max(Util.LogQfactor(belief,
		// Util.TangentPoint_BruteForce(belief,200)));
		// return MathPF.max(Util.LogQfactor(belief,
		// Util.TangentPoint_Fibonacci(belief,200)));
	}

	/* A more sophisticated method for getting the current value. */
	public Double GetCurrentValue2() {
		double logq = MathPF.max(Util.EnvelopeLogQfactor(belief));
		if (logq > logc)
			return logq;

		int ub = UpperBoundSearch(belief, logc);
		if (ub == 0) // we should stop
			return MathPF.max(Util.LogQfactor(belief, 1));
		else
			// we should continue
			return MathPF.max(Util.LogQfactor(belief, ub));
	}

	public static int UpperBoundSearch(Belief b, double logc) {
		double logbenefit, logcost;
		int budget;

		/*
		 * The KG factor is bounded above by the maximum of the values of
		 * gaining perfect information about any single alternative. Call this
		 * "benefit".
		 * 
		 * Thus, if benefit - c*n < 0, the best allocation with n measurements
		 * does not merit continuing. Thus, we need not consider any n greater
		 * than benefit / c = exp(log(benefit)-logc).
		 */
		logbenefit = LogBenefit(b, Double.POSITIVE_INFINITY);
		budget = (int) Math.floor(Math.exp(logbenefit - logc));

		while (budget > 0) {
			logbenefit = LogBenefit(b, budget);
			logcost = logc + Math.log(budget);
			if (debug > 1)
				System.out
						.printf(
								"UpperBoundSearch: budget=%d logbenefit=%f logcost=%f\n",
								budget, logbenefit, logcost);

			if (logbenefit > logcost) { // we found a good budget
				if (debug > 0)
					System.out
							.printf(
									"EndingUpperBoundSearch: budget=%d logbenefit=%f logcost=%f\n",
									budget, logbenefit, logcost);
				return budget;
			}

			/*
			 * By the same argument as above, we need not consider any budget
			 * greater than benefit/c = exp(logbenefit-logc). Also, budget had
			 * net negative value, and we are only considering integers, so we
			 * need not consider any budget greater than budget-1.
			 */
			budget = MathPF.min(budget - 1, (int) Math.floor(Math
					.exp(logbenefit - logc)));
		}
		if (debug > 0)
			System.out.printf("Ending UpperBoundSearch, budget=0\n");
		return 0; // we did not find a good budget.
	}

	/*
	 * Calculate the logarithm of the benefit (not including measurement costs)
	 * of the BEST allocation of size n to a single alternative. n may be
	 * infinity.
	 */
	public static double LogBenefit(Belief b, double n) {
		return MathPF.max(Util.LogBenefit(b, n));
	}

	public boolean ShouldStop() {
		return (GetCurrentValue() < logc);
	}

	@Override
	public String toString() {
		return "Variance known KG Envelope Stopping Rule logc-threshold="
				+ logc;
	}

	public String Name() {
		return "Variance Known KG Envelope StoppingRule";
	}

	/*
	 * Create custom and standard stopping rule sequences. These are mostly for
	 * convenience.
	 */
	public static ThresholdStoppingRuleSeq<Double> Seq(Belief b, double[] logc) {
		Double[] _logc = new Double[logc.length];
		for (int i = 0; i < logc.length; i++)
			_logc[i] = logc[i];
		return new ThresholdStoppingRuleSeq<Double>(_logc,
				new KGEnvelopeStoppingRule(b, logc[0]));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq(double[] logc) {
		Double[] _logc = new Double[logc.length];
		for (int i = 0; i < logc.length; i++)
			_logc[i] = logc[i];
		return new ThresholdStoppingRuleSeq<Double>(_logc,
				new KGEnvelopeStoppingRule(logc[0]));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq() {
		double logc[] = { -1, -2, -3, -4, -5, -6, -8, -10, -12, -14, -16, -18,
				-20 };
		return Seq(logc);
	}

	public static ThresholdStoppingRuleSeq<Double> Seq(Belief b) {
		double logc[] = { -1, -2, -3, -4, -5, -6, -8, -10, -12, -14, -16, -18,
				-20 };
		return Seq(b, logc);
	}

}
