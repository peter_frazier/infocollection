package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.util.MathPF;

public abstract class BayesBothRule implements InfoCollection.BothRule {
	Belief belief0;
	Belief belief;

	public BayesBothRule() {
		belief0 = null;
		belief = null;
	}

	public BayesBothRule(Belief b) {
		belief0 = new Belief(b);
		belief = null;
	}

	public void Start(int desiredM) throws Exception {
		if (belief0 == null)
			belief = new Belief(desiredM); // non-informative.
		else if (desiredM == belief0.M())
			belief = new Belief(belief0);
		else
			throw new Exception("belief0 and desiredM don't match.");
	}

	// public abstract int GetMeasurementDecision();

	public void RecordMeasurement(int x, double y) throws Exception {
		belief.Update(x, y);
	}

	public int GetImplementationDecision() {
		return MathPF.argmax(belief.mu);
		// XXX What should I do on a tie? I should randomly choose
		// among the argmax set.
	}

	public int GetNumMeasurements(int x) {
		double num = belief.EffectiveNum(x) - belief0.EffectiveNum(x);
		int num_int = (int) num;
		assert (num == num_int);
		return num_int;
	}

	// public double mu(int x) { return belief.mu[x]; }
	// public double beta(int x) { return belief.beta[x]; }
	public int M() {
		return belief.M();
	}

	public Belief GetBelief() {
		return belief;
	}

	// Please also implement toString.
	// public abstract String toString();
}
