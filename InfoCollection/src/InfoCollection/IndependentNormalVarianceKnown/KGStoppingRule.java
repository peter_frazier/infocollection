package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.ThresholdStoppingRule;
import InfoCollection.ThresholdStoppingRuleSeq;
import InfoCollection.util.MathPF;

public class KGStoppingRule extends BayesStoppingRule implements
		ThresholdStoppingRule<Double> {
	int debug = 0;
	double logc;

	public KGStoppingRule(double logc) {
		super();
		this.logc = logc;
	}

	public KGStoppingRule(Belief b, double logc) {
		super(b);
		this.logc = logc;
	}

	public Double GetThreshold() {
		return logc;
	}

	public void SetThreshold(Double threshold) {
		logc = threshold;
	}

	public Double GetCurrentValue() {
		/*
		 * Calculate the Q-factor for continuing, which is the maximum of the
		 * Q-factors corresponding to measuring each alternative. Actually, we
		 * calculate the logarithms because these can be small numbers.
		 */
		try {
			double max_logq = MathPF.max(Util.LogQfactor(belief));
			if (debug > 0) {
				System.out.println(belief);
				System.out.printf("max(logq)=%f\n", max_logq);
			}
			return max_logq;
		} catch (Exception e) {
			System.err.println("Internal error computing LogQfactor.");
			System.err.println("KGStoppingRule is stopping.");
			e.printStackTrace();
			return Double.NEGATIVE_INFINITY;
		}
	}

	public boolean ShouldStop() {
		boolean shouldstop = (GetCurrentValue() < logc);
		if (debug > 0 && shouldstop)
			System.out.printf("Stopping, logc=%f\n", logc);
		return shouldstop;
	}

	@Override
	public String toString() {
		return "Variance known KG Stopping Rule logc-threshold=" + logc;
	}

	public String Name() {
		return "Variance Known KG StoppingRule";
	}

	/*
	 * Create custom and standard stopping rule sequences. These are mostly for
	 * convenience.
	 */
	public static ThresholdStoppingRuleSeq<Double> Seq(Belief b, double[] logc) {
		Double[] _logc = new Double[logc.length];
		for (int i = 0; i < logc.length; i++)
			_logc[i] = logc[i];
		return new ThresholdStoppingRuleSeq<Double>(_logc, new KGStoppingRule(
				b, logc[0]));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq(double[] logc) {
		Double[] _logc = new Double[logc.length];
		for (int i = 0; i < logc.length; i++)
			_logc[i] = logc[i];
		return new ThresholdStoppingRuleSeq<Double>(_logc, new KGStoppingRule(
				logc[0]));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq() {
		double logc[] = { -1, -2, -3, -4, -5, -8, -10, -15, -20, -30, -40, -50,
				-60, -70, -80, -90, -100, -150 };
		return Seq(logc);
	}

	public static ThresholdStoppingRuleSeq<Double> Seq(Belief b) {
		double logc[] = { -1, -2, -3, -4, -5, -8, -10, -15, -20, -30, -40, -50,
				-60, -70, -80, -90, -100, -150 };
		return Seq(b, logc);
	}

}
