package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;
import java.util.Comparator;

import umontreal.iro.lecuyer.probdist.NormalDist;
import InfoCollection.util.ArrayIndexComparator;
import InfoCollection.util.MathPF;

/*
 * Some static functions for computing the LL allocation.  This is used by the
 * ChIn sampling rule, as well as the EOCck stopping rule.
 */

public class LL {
	static boolean debug = false;

	/*
	 * Compute the LL allocation.
	 */
	public static double[] Allocation(Belief belief, int budget) {
		int i, x, num_fractional, nincluded;
		boolean redo, inclusionSet[] = new boolean[belief.M()];
		double r[] = new double[belief.M()];
		double n[] = new double[belief.M()];
		double term[];
		double sum; // only used for assert logic code.

		if (debug)
			System.out
					.printf("VarianceKnown.LL.Allocation budget=%d\n", budget);

		/*
		 * At first include all the alternatives. Also, precompute n for every
		 * alternative.
		 */
		nincluded = 0;
		for (x = 0; x < belief.M(); x++) {
			if (belief.beta[x] < Double.POSITIVE_INFINITY) {
				nincluded++;
				inclusionSet[x] = true;
				n[x] = belief.beta[x] / belief.NoiseBeta(x);
			} else {
				/*
				 * If the prior precision on x is infinite, the LL procedure
				 * should allocate no replications to it.
				 */
				inclusionSet[x] = false;
				n[x] = Double.POSITIVE_INFINITY; // this value is not used.
			}
		}
		if (debug)
			System.out.println("n=" + Arrays.toString(n));

		/*
		 * Get an allocation which is non-negative, but not necessarily
		 * integral.
		 */
		redo = true;
		num_fractional = 0; // to quiet the compiler
		while (redo) {
			double numerator = budget;
			double denominator = 0;

			/*
			 * If only one alternative is in the inclusion set, allocate all the
			 * measurements to it and exit the loop.
			 */
			assert nincluded >= 1;
			if (nincluded == 1) {
				// Find the one to include
				for (x = 0; !inclusionSet[x] && x < belief.M(); x++)
					;
				assert x < belief.M();
				num_fractional = 0;
				r[x] = budget;
				break; // break out of the while(redo) loop.
			}

			term = ComputeTerm(belief, inclusionSet);
			for (x = 0; x < belief.M(); x++) {
				if (!inclusionSet[x])
					continue;
				numerator += n[x];
				assert term[x] >= 0;
				denominator += term[x];
			}
			if (debug) {
				double tmp[] = new double[belief.M()];
				for (x = 0; x < belief.M(); x++) {
					if (!inclusionSet[x])
						tmp[x] = 0;
					else
						tmp[x] = term[x] * numerator / denominator;
				}
				System.out.printf("numerator=%f denominator=%f\n", numerator,
						denominator);
				System.out.printf("term[]*numerator/denominator=%s\n", Arrays
						.toString(tmp));
			}
			assert denominator > 0 : "inclusionSet="
					+ Arrays.toString(inclusionSet) + " term="
					+ Arrays.toString(term);
			redo = false;
			num_fractional = 0;
			for (x = 0; x < belief.M(); x++) {
				if (!inclusionSet[x]) {
					// setting r[x]=0 here is redundant
					r[x] = 0;
					continue;
				}
				r[x] = term[x] * numerator / denominator - n[x];
				// note that operator precedence says this is equal to
				// r[x] = (term[x]*numerator/denominator) - n[x];
				if (r[x] < 0) {
					/*
					 * Remove it from the inclusion set, set r[x] to 0, and set
					 * the redo flag so that we iterate again. Continue looping
					 * through the rest of the alternatives so that we can find
					 * other x with r[x] < 0.
					 */
					inclusionSet[x] = false;
					nincluded--;
					r[x] = 0;
					redo = true;
				} else if (r[x] - Math.floor(r[x]) > 0) {
					num_fractional++;
				}
			}
		}
		if (debug)
			System.out.println("r=" + Arrays.toString(r));
		/*
		 * At this point, all the r[x] should be nonnegative, and they should
		 * sum to the whole budget, but they don't have to be integer-valued.
		 */
		/*
		 * This is error checking code that could go inside an assert block.
		 */
		sum = 0;
		for (x = 0; x < belief.M(); x++) {
			sum += r[x];
			assert r[x] >= 0;
		}
		/*
		 * Note that when we compare two floats for equality, there is some
		 * rounding error, so we just make sure that the absolute value of the
		 * difference is less than some small value.
		 */
		assert Math.abs(sum - budget) < 1E-10 : "sum=" + sum + " budget="
				+ budget + " r=" + Arrays.toString(r);

		/*
		 * Now round the r[x]. Sort the x with non-integral entries according to
		 * their non-intgeral part. Note that when we finish the previous loop
		 * we have a count of how many r[x] entries have fractional parts and
		 * need to be rounded.
		 */
		if (num_fractional > 0) {
			Integer rlist[] = new Integer[num_fractional];
			double r_frac[] = new double[belief.M()]; // fractional part of r
			int sofar = 0;
			for (i = x = 0; x < belief.M(); x++) {
				double r_round = Math.floor(r[x]);
				r_frac[x] = r[x] - r_round;
				sofar += r_round;
				if (r_frac[x] == 0)
					continue;
				r[x] = r_round; // round down
				rlist[i++] = x; // remember the fractional ones
				assert (x >= 0);
				assert (x < belief.M());
			}
			assert i == num_fractional : "i=" + i + " num_fractional="
					+ num_fractional;
			Comparator<Integer> c = new ArrayIndexComparator(r_frac);
			Arrays.sort(rlist, c);

			/*
			 * We now have budget - sofar to give. Allocate them to the entries
			 * with the largest fractional part. The sort has put them in
			 * ascending numerical order.
			 */
			int togive = budget - sofar;
			assert togive <= num_fractional : "togive=" + togive + " sofar="
					+ sofar + " num_fractional=" + num_fractional;
			for (i = 1; i <= togive; i++) {
				x = rlist[num_fractional - i]; // since they were in ascending
												// order
				assert x >= 0;
				assert x < belief.M();
				r[x]++;
			}
		}
		/*
		 * At this point, all the r[x] should be integer, and they should sum to
		 * the whole budget.
		 */
		// assert code
		sum = 0;
		for (x = 0; x < belief.M(); x++) {
			sum += r[x];
			assert Math.floor(r[x]) == r[x] : x + " " + r[x];
			assert r[x] >= 0;
		}
		assert (sum == budget);
		if (debug)
			System.out.println("r=" + Arrays.toString(r));

		/*
		 * Finally, we have an allocation. This is r[x] measurements for each x.
		 */
		return r;
	}

	/*
	 * Computes the "Term" array, where term[j] is actually
	 * sqrt(c_j*gamma_{j,\alpha}/lambda_j) If the best alternative is the only
	 * one in the inclusionSet, then that term will be 0 and all the rest will
	 * be unused. Otherwise, the term should be > 0.
	 */
	private static double[] ComputeTerm(Belief belief, boolean inclusionSet[]) {
		int best, k;
		double s, z, lambda_1k, gamma, gamma_best;
		double debug_gamma[], debug_lambda_1k[];
		double term[] = new double[belief.M()];
		boolean includeBest;

		best = MathPF.argmax(belief.mu);
		includeBest = inclusionSet[best];

		if (debug) {
			/*
			 * If we are debugging, it is nice to remember the gammas and
			 * lambdas in an array to make it easy to print them out.
			 */
			debug_gamma = new double[belief.M()];
			debug_lambda_1k = new double[belief.M()];
		} else {
			// Make the compiler not complain.
			debug_gamma = null;
			debug_lambda_1k = null;
		}

		gamma_best = 0;
		for (k = 0; k < belief.M(); k++) {
			/*
			 * The gamma and term of the best alternative is the sum of the
			 * others, so we don't need to do anything here if k == best.
			 */
			if (k == best) {
				term[k] = -1;
				if (debug)
					debug_lambda_1k[k] = -1; // unused
				continue;
			}
			/*
			 * If this alternative isn't being included, don't add its gamma to
			 * the gamma of the best alternative.
			 */
			if (!inclusionSet[k]) {
				term[k] = -1;
				if (debug)
					debug_lambda_1k[k] = -1; // unused
				continue;
			}

			/*
			 * The computation of lambda_1k depends on what is in the inclusion
			 * set.
			 */
			if (includeBest)
				lambda_1k = 1 / (1 / belief.beta[best] + 1 / belief.beta[k]);
			else
				lambda_1k = belief.beta[k];

			/* Now actually compute gamma and the term. */
			s = Math.sqrt(lambda_1k); // sqrt of precision
			z = s * (belief.mu[best] - belief.mu[k]);
			gamma = s * NormalDist.density(0, 1, z);
			gamma_best += gamma;
			term[k] = Math.sqrt(gamma / belief.NoiseBeta(k));
			if (debug) {
				debug_lambda_1k[k] = lambda_1k;
				debug_gamma[k] = gamma;
			}
		}
		if (debug)
			debug_gamma[best] = gamma_best;
		term[best] = Math.sqrt(gamma_best / belief.NoiseBeta(best));

		if (debug) {
			System.out.println("best=" + best);
			System.out.println(belief);
			System.out.println("lambda_1k=" + Arrays.toString(debug_lambda_1k));
			System.out.println("gamma=" + Arrays.toString(debug_gamma));
			System.out.println("term=" + Arrays.toString(term));
		}

		return term;
	}

	/*
	 * public static void main(String args[]) { String myclass =
	 * "InfoCollection.IndependentNormalVarianceKnown.LL"; boolean ok = true;
	 * System.out.println("Testing " + myclass); ok = test1() && ok;
	 * System.out.printf("%s: %s\n", myclass, ok ? "OK" : "FAIL"); }
	 */

	/*
	 * Compare the value of the LL allocation of the allocation should be the
	 * same as the KG factor.
	 */
	/*
	 * public static boolean test1() { boolean ok = true; // avoid values like
	 * beta=1 to catch missing square roots. Belief b = new Belief(2,1,0.5,0.5);
	 * double[] alloc = new double[2]; alloc[0] = 1;
	 */

};
