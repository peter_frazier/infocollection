package InfoCollection.IndependentNormalVarianceKnown;
import InfoCollection.StoppingRuleSeq;

import java.util.Arrays;

/*
 * Implements StoppingRuleSeq for the OptimalStoppingRule.
 * OptimalStoppingRule is *almost* a ThresholdStoppingRule, in the sense that
 * one can just have Next() set a new lower logc value.  It fails to be a
 * ThresholdStoppingRule becasue logc is not a threshold that we test against.
 * Instead, we use logc and the posterior variance to calculate an upper
 * stopping barrier against which we test the posterior mean.
 */
public class OptimalStoppingRuleSeq implements StoppingRuleSeq {
	int i; // current parameter being used.
	double logc[];
	OptimalStoppingRule rule;

	public OptimalStoppingRuleSeq(Belief b, double[] logc) throws Exception {
		this.logc = logc.clone();
		// I should also check that the logcs are in the right order. 
		rule = new OptimalStoppingRule(b,logc[0]);
		i = -1;
	}

	public void Start(int M) throws Exception {
		i = 0;
		rule.SetLogC(logc[i]);
		rule.Start(M);
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		rule.RecordMeasurement(x, y);
	}

	public boolean ShouldStop() {
		return rule.ShouldStop();
	}

	public boolean Next() {
		if (i == Length() - 1)
			return false;
		// else
		rule.SetLogC(logc[++i]);
		// System.out.println("Moved to logc " + i);
		return true;
	}

	public int Length() {
		return logc.length;
	}

	@Override
	public String toString() {
		return rule.Name() + Arrays.toString(logc);
	}
}
