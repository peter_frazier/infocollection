package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;

import umontreal.iro.lecuyer.probdist.NormalDist;
import InfoCollection.TestExamples;
import InfoCollection.util.ArrayIndexComparator;
import InfoCollection.util.MathPF;

/*
 * Variance known version of Chick & Inoue, where I recompute lambda_1k and
 * gamma arrays based on what is in the inclusion set.
 */

public class ChIn extends BayesSamplingRule {
	int n0; // number of measurements per alternative in the first stage
	int tau; // number of measurements to allocate per stage
	LinkedList<Integer> measurementsToGive;
	boolean debug = false;

	public ChIn() {
		this(1, 1);
	}

	public ChIn(int n0, int tau) {
		belief0 = null; // noninformative, flexible M.
		this.n0 = n0;
		this.tau = tau;
		measurementsToGive = new LinkedList<Integer>();
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		int i, x;
		measurementsToGive.clear();
		/*
		 * Allows doing a first stage in which n0 measurements are allocated to
		 * each alternative.
		 */
		for (i = 0; i < n0; i++)
			for (x = 0; x < M(); x++)
				measurementsToGive.add(x);
		/*
		 * We may have errors later if we begin with a noninformative prior on
		 * an alternative, and n0 = 0. Check for this and complain if we see it.
		 * An alternative way to handle it would be to pick a value of n0, like
		 * n0=6, but instead of just allocating 6 measurements round robin to
		 * each alternative as we do above, just allocate measurements round
		 * robin to those alternatives with beta/noisebeta < 6.
		 */
		if (n0 == 0) {
			for (x = 0; x < M() && belief.beta[x] > 0; x++)
				;
			if (x < M()) // we found a belief.beta[x] == 0
				throw new Exception(
						"ChIn cannot handle beta[x] == 0 and n0 == 0");
		}
	}

	/*
	 * Computes the "Term" array, where term[j] is actually
	 * sqrt(c_j*gamma_{j,\alpha}/lambda_j) If the best alternative is the only
	 * one in the inclusionSet, then that term will be 0 and all the rest will
	 * be unused. Otherwise, the term should be > 0.
	 */
	public double[] ComputeTerm(boolean inclusionSet[]) {
		int best, k;
		double s, z, lambda_1k, gamma, gamma_best;
		double debug_gamma[], debug_lambda_1k[];
		double term[] = new double[M()];
		boolean includeBest;

		best = MathPF.argmax(belief.mu);
		includeBest = inclusionSet[best];

		if (debug) {
			/*
			 * If we are debugging, it is nice to remember the gammas and
			 * lambdas in an array to make it easy to print them out.
			 */
			debug_gamma = new double[M()];
			debug_lambda_1k = new double[M()];
		} else {
			// Make the compiler not complain.
			debug_gamma = null;
			debug_lambda_1k = null;
		}

		gamma_best = 0;
		for (k = 0; k < M(); k++) {
			/*
			 * The gamma and term of the best alternative is the sum of the
			 * others, so we don't need to do anything here if k == best.
			 */
			if (k == best) {
				term[k] = -1;
				if (debug)
					debug_lambda_1k[k] = -1; // unused
				continue;
			}
			/*
			 * If this alternative isn't being included, don't add its gamma to
			 * the gamma of the best alternative.
			 */
			// if (!inclusionSet[best]) { // PF: old version, with bug!
			if (!inclusionSet[k]) { // PF: new version, fixed on 12/23/07
				term[k] = -1;
				if (debug)
					debug_lambda_1k[k] = -1; // unused
				continue;
			}

			/*
			 * The computation of lambda_1k depends on what is in the inclusion
			 * set.
			 */
			if (includeBest)
				lambda_1k = 1 / (1 / belief.beta[best] + 1 / belief.beta[k]);
			else
				lambda_1k = belief.beta[k];

			/* Now actually compute gamma and the term. */
			s = Math.sqrt(lambda_1k); // sqrt of precision
			z = s * (belief.mu[best] - belief.mu[k]);
			gamma = s * NormalDist.density(0, 1, z);
			gamma_best += gamma;
			term[k] = Math.sqrt(gamma / belief.NoiseBeta(k));
			if (debug) {
				debug_lambda_1k[k] = lambda_1k;
				debug_gamma[k] = gamma;
			}
		}
		if (debug)
			debug_gamma[best] = gamma_best;
		term[best] = Math.sqrt(gamma_best / belief.NoiseBeta(best));

		if (debug) {
			System.out.println("best=" + best);
			System.out.println(belief);
			System.out.println("lambda_1k=" + Arrays.toString(debug_lambda_1k));
			System.out.println("gamma=" + Arrays.toString(debug_gamma));
			System.out.println("term=" + Arrays.toString(term));
		}

		return term;
	}

	/*
	 * Allocate more measurements according to the algorithm, and put them into
	 * the measurementsToGive list in round-robin fashion.
	 */
	public void AllocateMore() {
		int i, x, budget, num_fractional, nincluded;
		boolean redo, inclusionSet[] = new boolean[M()];
		double r[] = new double[M()];
		double n[] = new double[M()];
		double term[];
		double sum; // only used for assert logic code.

		if (debug)
			System.out.println("AllocateMore (VarianceKnown.ChIn)");

		/*
		 * In the traditional version of the algorithm, we check how many
		 * measurements are left and if this is less than tau, then we only
		 * allocate up to that. In this version, we ignore this, allocating tau
		 * each time. This is ok if tau divides the total budget, as happens
		 * when tau=1, or when N is infinite.
		 */
		budget = tau;

		/*
		 * At first include all the alternatives. Also, precompute n for every
		 * alternative.
		 */
		nincluded = M();
		for (x = 0; x < M(); x++) {
			inclusionSet[x] = true;
			n[x] = belief.beta[x] / belief.NoiseBeta(x);
		}
		if (debug)
			System.out.println("n=" + Arrays.toString(n));

		/*
		 * Get an allocation which is non-negative, but not necessarily
		 * integral.
		 */
		redo = true;
		num_fractional = 0; // to quiet the compiler
		while (redo) {
			double numerator = budget;
			double denominator = 0;

			/*
			 * If only one alternative is in the inclusion set, allocate all the
			 * measurements to it and exit the loop.
			 */
			assert nincluded >= 1;
			if (nincluded == 1) {
				// Find the one to include
				for (x = 0; !inclusionSet[x] && x < M(); x++)
					;
				assert x < M();
				num_fractional = 0;
				r[x] = budget;
				break; // break out of the while(redo) loop.
			}

			term = ComputeTerm(inclusionSet);
			for (x = 0; x < M(); x++) {
				if (!inclusionSet[x])
					continue;
				numerator += n[x];
				assert term[x] >= 0;
				denominator += term[x];
			}
			if (debug) {
				double tmp[] = new double[M()];
				for (x = 0; x < M(); x++) {
					if (!inclusionSet[x])
						tmp[x] = 0;
					else
						tmp[x] = term[x] * numerator / denominator;
				}
				System.out.printf("numerator=%f denominator=%f\n", numerator,
						denominator);
				System.out.printf("term[]*numerator/denominator=%s\n", Arrays
						.toString(tmp));
			}
			assert denominator > 0 : "inclusionSet="
					+ Arrays.toString(inclusionSet) + " term="
					+ Arrays.toString(term);
			redo = false;
			num_fractional = 0;
			for (x = 0; x < M(); x++) {
				if (!inclusionSet[x]) {
					// setting r[x]=0 here is redundant
					r[x] = 0;
					continue;
				}
				r[x] = term[x] * numerator / denominator - n[x];
				// note that operator precedence says this is equal to
				// r[x] = (term[x]*numerator/denominator) - n[x];
				if (r[x] < 0) {
					/*
					 * Remove it from the inclusion set, set r[x] to 0, and set
					 * the redo flag so that we iterate again. Continue looping
					 * through the rest of the alternatives so that we can find
					 * other x with r[x] < 0.
					 */
					inclusionSet[x] = false;
					nincluded--;
					r[x] = 0;
					redo = true;
				} else if (r[x] - Math.floor(r[x]) > 0) {
					num_fractional++;
				}
			}
		}
		if (debug)
			System.out.println("r=" + Arrays.toString(r));
		/*
		 * At this point, all the r[x] should be nonnegative, and they should
		 * sum to the whole budget, but they don't have to be integer-valued.
		 */
		/*
		 * This is error checking code that could go inside an assert block.
		 */
		sum = 0;
		for (x = 0; x < M(); x++) {
			sum += r[x];
			assert r[x] >= 0;
		}
		/*
		 * Note that when we compare two floats for equality, there is some
		 * rounding error, so we just make sure that the absolute value of the
		 * difference is less than some small value.
		 */
		assert Math.abs(sum - budget) < 1E-10 : "sum=" + sum + " budget="
				+ budget + " r=" + Arrays.toString(r);

		/*
		 * Now round the r[x]. Sort the x with non-integral entries according to
		 * their non-intgeral part. Note that when we finish the previous loop
		 * we have a count of how many r[x] entries have fractional parts and
		 * need to be rounded.
		 */
		if (num_fractional > 0) {
			Integer rlist[] = new Integer[num_fractional];
			double r_frac[] = new double[M()]; // fractional part of r
			int sofar = 0;
			for (i = x = 0; x < M(); x++) {
				double r_round = Math.floor(r[x]);
				r_frac[x] = r[x] - r_round;
				sofar += r_round;
				if (r_frac[x] == 0)
					continue;
				r[x] = r_round; // round down
				rlist[i++] = x; // remember the fractional ones
				assert (x >= 0);
				assert (x < M());
			}
			assert i == num_fractional : "i=" + i + " num_fractional="
					+ num_fractional;
			Comparator<Integer> c = new ArrayIndexComparator(r_frac);
			Arrays.sort(rlist, c);

			/*
			 * We now have budget - sofar to give. Allocate them to the entries
			 * with the largest fractional part. The sort has put them in
			 * ascending numerical order.
			 */
			int togive = budget - sofar;
			assert togive <= num_fractional : "togive=" + togive + " sofar="
					+ sofar + " num_fractional=" + num_fractional;
			for (i = 1; i <= togive; i++) {
				x = rlist[num_fractional - i]; // since they were in ascending
												// order
				assert x >= 0;
				assert x < M();
				r[x]++;
			}
		}
		/*
		 * At this point, all the r[x] should be integer, and they should sum to
		 * the whole budget.
		 */
		// assert code
		sum = 0;
		for (x = 0; x < M(); x++) {
			sum += r[x];
			assert Math.floor(r[x]) == r[x] : x + " " + r[x];
			assert r[x] >= 0;
		}
		assert (sum == budget);
		if (debug)
			System.out.println("r=" + Arrays.toString(r));

		/*
		 * Now put these allocations onto the measurementsToGive list. Put on
		 * r[x] measurements for each x.
		 */
		for (x = 0; x < M(); x++) {
			for (i = 0; i < r[x]; i++)
				measurementsToGive.add(x);
		}
	}

	public int GetMeasurementDecision() throws Exception {
		/*
		 * If there are more measurements in the currently allocated bucket,
		 * allocate them. Otherwise need to allocate more.
		 */
		// System.out.println("GetMeasurementDecision onqueue=" +
		// measurementsToGive.size());
		if (measurementsToGive.size() == 0)
			AllocateMore();
		if (measurementsToGive.size() == 0)
			throw new Exception("Failure to Allocate More");
		// System.out.println("After AllocateMore onqueue=" +
		// measurementsToGive.size());
		return measurementsToGive.removeFirst();
	}

	@Override
	public String toString() {
		return "ChIn: Chick & Inoue LL(S) known variance n0=" + n0 + " tau="
				+ tau;
	}

	public static void main(String args[]) throws Exception {
		// TestExamples.RandomProblem(new ChIn(1,1));
		// TestExamples.SlippageWithKGStopping(new ChIn(6,1));
		// TestExamples.SlippageWithEOCBonfStopping(new ChIn(6,1));
		// System.out.println(Simulator.SimulateOpportunityCost(new ChIn(1,10),
		// Problem.Example1(900), 5000, 0));
		TestExamples.Example1(new ChIn(10, 10), true);
		// TestExamples.Example1SpeedTest(new ChIn(1,10));
	}
};
