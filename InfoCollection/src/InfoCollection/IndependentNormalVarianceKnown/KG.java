package InfoCollection.IndependentNormalVarianceKnown;

import java.io.PrintStream;
import java.util.Arrays;

import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;
import InfoCollection.util.Normal;

/*
 * This computes the standard knowledge-gradient policy, but it computes the
 * logarithms of the KG-factors, rather than the KG-factors themselves.
 * Actually, it computes the logarithm of a term which is equal to the KG-factor
 * minus a constant.  This makes the code less sensitive to numerical errors.
 */
public class KG extends BayesSamplingRule {
	boolean error_checking_on;
	boolean debug = false;
	int first, second; // the indices of the largest and second largest values.
	double sigmatilde[];
	double logKG[];

	/*
	 * Constructors. If we pass no belief, it uses the noninformative prior. To
	 * pass whether to use error checking, we pass an int because Matlab has
	 * trouble passing booleans. We pass an int because Matlab has trouble
	 * passing booleans.
	 */
	public KG() {
		super();
		error_checking_on = false;
	}

	public KG(Belief b) {
		super(b);
		error_checking_on = false;
	}

	public KG(int check_errors) {
		super();
		error_checking_on = (check_errors != 0);
	}

	public KG(Belief b, int check_errors) {
		super(b);
		error_checking_on = (check_errors != 0);
	}

	/*
	 * Find the indices of the best (called "first") and second best mu's.
	 */
	private void RecomputeFirstSecond() {
		int ret[];
		ret = MathPF.FirstSecond(belief.mu);
		first = ret[0];
		second = ret[1];
	}

	/*
	 * Recompute the KGfactor for only one alternative. Assumes that
	 * sigmatilde[x] is correct. Also, assumes that first and second are
	 * correct, although there is one exception: if first is right but second is
	 * wrong and you know that x is not second best, this routine will work. In
	 * some cases we could get rid of the if statement because we know whether
	 * or not x == first, but this optimization would add code and wouldn't
	 * improve speed much.
	 */
	private void RecomputeKGfactor(int x) {
		double distance, influence;
		// System.out.println("RecomputeKGfactor x=" + x);
		if (sigmatilde[x] == 0) {
			/*
			 * We do this so that we don't have to worry about dividing by 0 to
			 * compute influence.
			 */
			logKG[x] = Double.NEGATIVE_INFINITY;
			return;
		}
		// We make the distance positive so that we don't need to
		// calculate an absolute value.
		if (x == first)
			distance = belief.mu[x] - belief.mu[second];
		else
			distance = belief.mu[first] - belief.mu[x];
		assert distance >= 0;
		influence = -distance / sigmatilde[x];
		// compute log of KG-factor instead
		try {
			logKG[x] = Math.log(sigmatilde[x]) + Normal.LogPsi(-influence);
		} catch (Exception e) {
			System.err.println("Internal error: LogPsi failed");
			System.err.println("KG factors may be out of order.");
			e.printStackTrace();
		}
	}

	/*
	 * Recompute the KGfactors for all of the alternatives. Assumes that the
	 * first and the second indices are correct; also assumes that the
	 * sigmatildes are correct.
	 */
	private void RecomputeKGfactors() {
		for (int i = 0; i < M(); i++)
			RecomputeKGfactor(i);
	}

	/*
	 * RecomputeSigmatildes() recomputes all the sigmatilde values from scratch.
	 * RecomputeSigmatilde(int x) recompute the sigmatilde for alternative x.
	 */
	private void RecomputeSigmatilde() {
		for (int x = 0; x < M(); x++)
			RecomputeSigmatilde(x);
	}

	private void RecomputeSigmatilde(int x) {
		sigmatilde[x] = Util.SigmaTilde(belief.beta[x], belief.NoiseBeta(x));
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		logKG = new double[M()];
		sigmatilde = new double[M()];
		RecomputeSigmatilde();
		RecomputeFirstSecond();
		RecomputeKGfactors();
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		RecomputeSigmatilde(x);
		// System.out.println(Arrays.toString(mu));
		// System.out.println(Arrays.toString(beta));
		// System.out.println(Arrays.toString(logKG));
		// System.out.println(Arrays.toString(sigmatilde));
		// System.out.println("first="+first + " second=" + second);
		// System.out.println("RecordMeasurement(x=" + x + "y=" + y);
		if (belief.mu[x] > belief.mu[first]) {
			/*
			 * The measured alternative replaces the largest. The old largest is
			 * now second largest. All the KG-factors change.
			 */
			second = first;
			first = x;
			RecomputeKGfactors();
		} else if (x == first && belief.mu[x] < belief.mu[second]) {
			/*
			 * We measured the largest and it is now lower than what used to be
			 * the second best. The old second best becomes best. We don't know
			 * which alternative is now second best. We loop through the
			 * alternatives once, computing all KG-factors except for the best
			 * alternative, and finding which alternative is second best. Then,
			 * we compute the KG-factor for the best alternative.
			 */
			int oldfirst = first;
			first = second;
			// we just need to initialize second to something different from
			// first
			// we also could have done second = first == 0 ? 1 : 0;
			second = oldfirst;
			for (int i = 0; i < M(); i++) {
				if (i == first)
					continue;
				else if (belief.mu[i] > belief.mu[second])
					second = i;
				RecomputeKGfactor(i);
			}
			RecomputeKGfactor(first);
		} else if (x == first) {
			/*
			 * We measured the best alternative, and it changed, but it is still
			 * the best. Thus we don't have to change first or second, but we do
			 * have to change all the KGfactors.
			 */
			RecomputeKGfactors();
		} else if (belief.mu[x] > belief.mu[second]) {
			/*
			 * We measured an alternative that was neither best nor second best,
			 * but it became second best. We have to recompute the KGfactors for
			 * both the best and for this alternative.
			 */
			second = x;
			RecomputeKGfactor(x);
			RecomputeKGfactor(first);
		} else if (x == second) {
			/*
			 * We measured the second best alternative. We know that it didn't
			 * change enough to make it best, but it might have changed enough
			 * to no longer be the second best.
			 */
			RecomputeFirstSecond();
			RecomputeKGfactor(x);
			RecomputeKGfactor(first);
		} else {
			/*
			 * Neither the indices of, nor the values of, the best and the
			 * second best changed. Recompute the qfactor only for the
			 * alternative that we measured.
			 */
			RecomputeKGfactor(x);
		}
	}


	/*
	 * This is the version of RecordMeasurement that outputs debugging info. We
	 * implement this method rather than inheriting the default so that we can
	 * print out the extra state this policy keeps.
	 */
	public void RecordMeasurement(int x, double y, PrintStream s)
			throws Exception {
		s.format("mu=%s\n", Arrays.toString(belief.mu));
		s.format("beta=%s\n", Arrays.toString(belief.beta));
		s.format("mu[first]=%g mu[second]=%g\n", belief.mu[first],
				belief.mu[second]);
		s.format("sigmatilde=%s\n", Arrays.toString(sigmatilde));
		s.format("logKG=%s\n", Arrays.toString(logKG));
		s.format("RecordMeasurement: x=%d y=%g\n", x, y);
		RecordMeasurement(x, y);
	}

	public int GetMeasurementDecision() throws Exception {
		if (debug) {
			double[] influence = Util.Influence(belief);
			System.out.println(belief);
			System.out.println("sigmatilde=" + Arrays.toString(sigmatilde));
			System.out.println("influence=" + Arrays.toString(influence));
			System.out.printf("logKG=%s\n", Arrays.toString(logKG));
		}
		/*
		 * We use the traditional way of breaking ties, but below we have two
		 * other choices, which is to break ties according to precision, or to
		 * break ties randomly.
		 */
		int x = MathPF.argmax(logKG);
		// int x = MathPF.argmaxSpecial(logKG,belief.beta,belief.mu);
		// int x = MathPF.argmax(logKG,rnd);

		if (!error_checking_on)
			return x;
		/* Below here is all error checking. */

		/*
		 * The following is error checking. We know via theory that the
		 * knowledge-gradient policy, when computed correctly, will never
		 * measure an alternative which is dominated in both influence and
		 * precision. We check that this doesn't happen, and if it does, then we
		 * report an error.
		 */
		double[] influence = Util.Influence(belief);
		for (int i = 0; i < M(); i++) {
			if (belief.beta[x] < belief.beta[i])
				continue; // x beats i on variance
			if (influence[x] > influence[i])
				continue; // x beats i on mean
			if (influence[x] == influence[i]
					&& belief.beta[x] == belief.beta[i])
				continue; // it's a tie
			// If we get here, then we know there is a problem.
			// Dump state and throw an exception.
			String err = String.format(
					"mu=%s\nbeta=%s\nlogKG=%s\nx=%d i=%d first=%d second=%d",
					Arrays.toString(belief.mu), Arrays.toString(belief.beta),
					Arrays.toString(logKG), x, i, first, second);
			throw new Exception(
					"KG calculation error: measuring a dominated alternative\n"
							+ err);
		}

		// PF: this needs to be altered because the KGfactor computation
		// is not in log-space, and so is less accurate than what we
		// compute here in log space.
		/*
		 * double KGfactorCheck[] = Util.KGfactor(belief); if (x !=
		 * MathPF.argmaxSpecial(KGfactorCheck,belief.beta,belief.mu)) { String
		 * err = String.format("logKG=%s\nKGfactorCheck=%s\n",
		 * Arrays.toString(logKG), Arrays.toString(KGfactorCheck)); throw new
		 * Exception("KG calculation error: disagreement with KGfactor\n" +
		 * err); }
		 */

		return (x);
	}

	/* Allows getting various state for debugging. */
	public double[] GetKG() {
		double[] KG = new double[M()];
		for (int i = 0; i < M(); i++)
			KG[i] = java.lang.Math.exp(logKG[i]);
		return KG;
	}

	/* Allows getting various state for debugging. */
	public double[] GetLogKG() {
		return logKG;
	}

	public double[] GetSigmatilde() {
		return sigmatilde;
	}

	public int GetFirst() {
		return first;
	}

	public int GetSecond() {
		return second;
	}

	@Override
	public String toString() {
		return "KG";
	}

	public static void main(String args[]) throws Exception {
		test5();
		// RegressionTest();
	}

	public static void RegressionTest() throws Exception {
		// these first tests succeed as long as they don't crash.
		TestExamples.Example1(new KG(1), false);
		TestExamples.RandomProblem(new Belief(10, 0, 1, 1), new KG(1));
		TestExamples.TestStopping(new KG(1), KGStoppingRule.Seq());

		// the second test should be equivalent.
		if (TestExamples.CheckEquivalence(new KG(1), new KGSimple()) > 0)
			throw new Exception("not equivalent");
	}

	/*
	 * Run it on example 1 with a KG stopping rule to see if there are any
	 * errors. It also reports the results for graphing. This test can take a
	 * long time and isn't good for regression.
	 */
	public static boolean test5() {
		try {
			TestExamples.TestStopping(new KG(), KGStoppingRule.Seq());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test5: FAILED");
			return false;
		}
		System.out.println("test5: OK");
		return true;
	}

};
