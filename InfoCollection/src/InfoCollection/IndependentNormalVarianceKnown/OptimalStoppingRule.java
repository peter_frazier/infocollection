package InfoCollection.IndependentNormalVarianceKnown;
import InfoCollection.ThresholdStoppingRule;
import InfoCollection.ThresholdStoppingRuleSeq;

public class OptimalStoppingRule extends BayesStoppingRule {
	double logc;
	double gamma;
	double WSC_beta;

	// Index of the alternative we don't know perfectly. Either 0 or 1.
	int unk;

	// Value of the alternative that is known perfectly. In the paper,
	// this is called m.
	double standard;

	public OptimalStoppingRule(Belief b, double logc) throws Exception {
		super(b);

		if (b.M() != 2)
			throw new Exception(
					"We do not know optimal policy for the inputted"
							+ "belief.  We require exactly 2 alternatives.");
		if (b.beta[0] == Double.POSITIVE_INFINITY) {
			unk = 1;
			standard = b.mu[0];
		} else if (b.beta[1] == Double.POSITIVE_INFINITY) {
			unk = 0;
			standard = b.mu[1];
		} else
			throw new Exception(
					"We do not know optimal policy for the inputted"
							+ "belief.  We require one of the alternatives to be known "
							+ "perfectly.");
		SetLogC(b.NoiseVar(unk), logc);
	}

	private void SetLogC(double noisevar, double logc) {
		this.logc = logc;
		double c = Math.exp(logc);
		WSC_beta = Math.pow(c * noisevar, -1.0 / 3.0);
		// WSC_beta is the beta from the WSC paper, not to be confused
		// with the precision which is often labeled beta.
		gamma = Math.pow((c * c) / noisevar, 1.0 / 3.0);
	}

	public void SetLogC(double logc) {
		SetLogC(belief0.NoiseVar(unk), logc);
	}




	public boolean ShouldStop() {
		/*
		 * Compares two things: 1. the difference in value between the posterior
		 * mean and the standard. 2. the transformed continuation region value.
		 */

		double delta = Math.abs(belief.mu[unk] - standard);
		// effective # times sampled
		double t = belief.beta[unk] / belief.noiseBeta[unk];
		double barrier = Util.ApproxStdUpperBoundary(1 / (gamma * t)) / WSC_beta;

		return (delta > barrier);
	}

	@Override
	public String toString() {
		return "Variance known Optimal Stopping Rule logc=" + logc;
	}

	public String Name() {
		return "Variance Known Optimal Stopping Rule";
	}

	/*
	 * Create custom and standard stopping rule sequences. These are mostly for
	 * convenience.
	 */
	 /*
	 public static ThresholdStoppingRuleSeq Seq(Belief b, double[] logc) {
	   Double[] _logc = new Double[logc.length];
	   for (int i=0; i<logc.length; i++)
	     _logc[i] = logc[i];
	   return new ThresholdStoppingRuleSeq<Double>(_logc, new OptimalStoppingRule(b,logc[0]));
	 }

	 public static ThresholdStoppingRuleSeq Seq(double[] logc) {
	   Double[] _logc = new Double[logc.length];
	   for (int i=0; i<logc.length; i++)
	     _logc[i] = logc[i];
	   return new StoppingRuleSeq<Double>(_logc, new OptimalStoppingRule(logc[0]));
	 }
	 
	 public static ThresholdStoppingRuleSeq Seq() {
	   double logc[] =
	   {-1,-2,-3,-4,-5,-8,-10,-15,-20,-30,-40,-50,-60,-70,-80,-90,-100,-150};
	   return Seq((double[])logc);
	 }

	 public static ThresholdStoppingRuleSeq Seq(Belief b)
	 {
	   double logc[] = {-1,-2,-3,-4,-5};
	   return Seq(b,(double[])logc);
	 }
	 */
}
