package InfoCollection.IndependentNormalVarianceKnown;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import InfoCollection.util.MathPF;
import InfoCollection.util.Normal;

/*
 * This class contains some generically useful utilities.
 */
public class Util {
	public static int debug = 0;

	/*
	 * Computes the standard deviation in \mu^{n+1}_x that we would see with
	 * respect to \Fn if we measured alternative x as a function of \beta^n_x.
	 * 
	 * PF: maybe it would be better to pass the variance of the noise instead of
	 * the noiseBeta (inverse variance of the noise), or to pass no variance and
	 * just assume it is one.
	 */
	public static double SigmaTilde(double beta, double noiseBeta) {
		/*
		 * We will compute the variance before taking the square root to get the
		 * standard deviation.
		 */
		double var;

		if (noiseBeta == 0 || beta == Double.POSITIVE_INFINITY)
			// no information in measurement
			var = 0;
		else if (beta == 0)
			// noninformative prior and measurement precision > 0.
			var = Double.POSITIVE_INFINITY;
		else if (noiseBeta == Double.POSITIVE_INFINITY)
			// perfect measurement
			var = 1.0 / beta;
		else
			var = 1.0 / (beta * (1.0 + beta / noiseBeta));

		return Math.sqrt(var);
	}

	/*
	 * Calculates |\mu_x - \max_{x'\ne x} \mu_{x'}|.
	 */
	public static double[] Delta(Belief b) {
		assert (b.M() > 1);
		/* Find the best (called "first") and second best entries */
		int first, second;
		if (b.mu[0] >= b.mu[1]) {
			first = 0;
			second = 1;
		} else {
			first = 1;
			second = 0;
		}
		for (int i = 2; i < b.M(); i++) {
			if (b.mu[i] > b.mu[first]) {
				second = first;
				first = i;
			} else if (b.mu[i] > b.mu[second])
				second = i;
		}

		double delta[] = new double[b.M()];
		for (int i = 0; i < b.M(); i++) {
			if (i == first)
				delta[i] = b.mu[i] - b.mu[second];
			else
				delta[i] = b.mu[first] - b.mu[i];
		}
		if (debug > 2) {
			System.out.println("Delta(): delta=" + Arrays.toString(delta));
			System.out.println("Delta(): belief=" + b);
		}
		return delta;
	}

	/*
	 * Calculates the (normalized) influence of mu and beta. The normalized
	 * influence is the unnormalized influence divided by SigmaTilde(beta). The
	 * unnormalized influence is -|\mu_x - \max_{x'\ne x} \mu_{x'}|. If
	 * Sigmatilde(beta) is 0, the influence is -infinity, even if the
	 * unnormalized influence is 0.
	 */
	public static double[] Influence(Belief b) {
		assert (b.M() > 1);
		/* Find the best (called "first") and second best entries */
		int first, second;
		if (b.mu[0] >= b.mu[1]) {
			first = 0;
			second = 1;
		} else {
			first = 1;
			second = 0;
		}
		for (int i = 2; i < b.M(); i++) {
			if (b.mu[i] > b.mu[first]) {
				second = first;
				first = i;
			} else if (b.mu[i] > b.mu[second])
				second = i;
		}

		if (debug > 2)
			System.out.println("first=" + first + " second=" + second);

		/* Now calculate the influence */
		double influence[] = new double[b.M()];
		for (int i = 0; i < b.M(); i++) {
			double distance;
			if (i == first)
				distance = b.mu[second] - b.mu[i];
			else
				distance = b.mu[first] - b.mu[i];
			double s = SigmaTilde(b.beta[i], b.noiseBeta[i]);
			if (s == 0)
				influence[i] = Double.NEGATIVE_INFINITY;
			else
				influence[i] = -Math.abs(distance) / s;
		}
		// System.out.println("influence = " + Arrays.toString(influence));
		return influence;
	}

	/*
	 * Calculates the normalized influence of mu and beta, like
	 * Influence(mu,beta,noiseBeta), but this function does it with a
	 * precomputed sigmatilde.
	 */
	public static double[] Influence(double mu[], double sigmatilde[]) {
		/* Find the best (called "first") and second best entries */
		int first, second;
		if (mu[0] >= mu[1]) {
			first = 0;
			second = 1;
		} else {
			first = 1;
			second = 0;
		}
		for (int i = 2; i < mu.length; i++) {
			if (mu[i] > mu[first]) {
				second = first;
				first = i;
			} else if (mu[i] > mu[second])
				second = i;
		}

		/* Now calculate the influence */
		double influence[] = new double[mu.length];
		for (int i = 0; i < mu.length; i++) {
			double distance;
			if (i == first)
				distance = mu[second] - mu[i];
			else
				distance = mu[first] - mu[i];
			if (sigmatilde[i] == 0)
				influence[i] = Double.NEGATIVE_INFINITY;
			else
				influence[i] = -Math.abs(distance) / sigmatilde[i];
		}
		return influence;
	}

	/*
	 * Compute the Qfactors, but with a precomputed sigmatilde. Note that this
	 * is actually the part of the Qfactor without the leading \max_{x'}
	 * \mu_{x'} (see the independent KG paper, theorem 3.1, equation 18 on page
	 * 13. That is, this function computes only sigmatilde(beta)*Psi(-z), where
	 * z is the normalized influence.
	 * 
	 * These functions are now obsolete, because they don't compute it in
	 * log-space, and so are prone to numerical error.
	 */
	private static double[] Qfactor(Belief b) {
		double influence[] = Influence(b);
		double qfactor[] = new double[b.M()];
		for (int i = 0; i < b.M(); i++) {
			double s = SigmaTilde(b.beta[i], b.noiseBeta[i]);
			if (s == 0)
				qfactor[i] = 0;
			else
				qfactor[i] = s * Normal.Psi(-influence[i]);

			// Debugging
			// System.out.print("i=" + i + " s=" + s + " influence=" +
			// influence[i] + "\n");
		}
		return qfactor;
	}

	@SuppressWarnings("unused")
	private static double[] Qfactor(double mu[], double sigmatilde[]) {
		double influence[] = Influence(mu, sigmatilde);
		double qfactor[] = new double[mu.length];
		for (int i = 0; i < mu.length; i++) {
			/* note that sigmatilde multiplies the whole thing */
			if (sigmatilde[i] == 0)
				qfactor[i] = 0;
			else
				qfactor[i] = sigmatilde[i] * Normal.Psi(-influence[i]);
		}
		return qfactor;
	}

	/*
	 * Compute the log (base e) of the Q-factors, rather than the Q-factors
	 * themselves. This can be more accurate, since the Q-factors are often
	 * small.
	 * 
	 * LogQfactor(Belief b, double[] n) -- considers batches of n[i]
	 * measurements. LogQfactor(Belief b) -- same as LogQfactor(Belief b, double
	 * n) with a vector of 1s passed as the second argument.
	 */
	public static double LogQfactor(double delta, double beta, double noiseBeta) {
		double sigmatilde = SigmaTilde(beta, noiseBeta);
		if (sigmatilde == 0)
			/*
			 * if the alternative is known perfectly, the q-factor is 0, and the
			 * log(q-factor) is -infinity.
			 */
			return Double.NEGATIVE_INFINITY;
		else {
			double influence = -Math.abs(delta) / sigmatilde;
			if (debug > 2) {
				System.out.println("delta=" + delta + " beta=" + beta
						+ " noiseBeta=" + noiseBeta + " sigmatilde="
						+ sigmatilde);
				System.out.println("influence=" + influence
						+ " logPsi(-influence)=" + Normal.LogPsi(-influence));
			}
			return Math.log(sigmatilde) + Normal.LogPsi(-influence);
		}
	}

	public static double[] LogQfactor(Belief b) {
		return LogQfactor(b, 1);
	}

	public static double[] LogQfactor(Belief b, double[] n) {
		double logqfactor[] = LogBenefit(b, n);
		for (int i = 0; i < logqfactor.length; i++)
			logqfactor[i] -= Math.log(n[i]);
		return logqfactor;
	}

	public static double[] LogQfactor(Belief b, double n) {
		double[] nvec = new double[b.M()];
		for (int i = 0; i < b.M(); nvec[i++] = n)
			;
		return LogQfactor(b, nvec);
	}

	/*
	 * Calculate the logarithm of the benefit of the measurement. This is the
	 * Q-factor, but not dividing by the number of measurements. n is allowed to
	 * be infinity.
	 */
	public static double[] LogBenefit(Belief b, double[] n) {
		// compute sigmatilde, modified to include the number of measurements n.
		double sigmatilde[] = new double[b.M()];
		for (int i = 0; i < b.M(); i++)
			sigmatilde[i] = SigmaTilde(b.beta[i], b.noiseBeta[i] * n[i]);
		double influence[] = Influence(b.mu, sigmatilde); // compute with the
															// n-modified
															// sigmatilde.
		double logbenefit[] = new double[b.M()];
		for (int i = 0; i < b.M(); i++) {
			if (sigmatilde[i] == 0)
				logbenefit[i] = Double.NEGATIVE_INFINITY;
			else {
				logbenefit[i] = Math.log(sigmatilde[i])
						+ Normal.LogPsi(-influence[i]);
				if (debug > 0) {
					System.out
							.printf(
									"sigmatilde=%f influence=%f LogPsi(-influence)=%f logbenefit=%f\n",
									sigmatilde[i], influence[i], Normal
											.LogPsi(-influence[i]),
									logbenefit[i]);
					System.out.println("noiseBeta=" + b.noiseBeta + " beta="
							+ b.beta[i]);
				}
			}
		}
		return logbenefit;
	}

	public static double[] LogBenefit(Belief b, double n) {
		double[] nvec = new double[b.M()];
		for (int i = 0; i < b.M(); nvec[i++] = n)
			;
		return LogBenefit(b, nvec);
	}

	/*
	 * Takes the concave envelope over the value of information curve, and
	 * returns the concavified log q-factor. The first version of this function
	 * assumes no horizon, and the second one accepts a finite horizon as input.
	 */
	public static double EnvelopeLogQfactor(double delta, double beta,
			double noiseBeta) {
		double nstar = TangentPoint(delta, beta, noiseBeta);
		return LogQfactor(delta, beta, nstar * noiseBeta) - Math.log(nstar);
	}

	public static double[] EnvelopeLogQfactor(Belief b) {
		return LogQfactor(b, TangentPoint(b));
	}

	public static double[] EnvelopeLogQfactor(Belief b, double nleft) {
		return LogQfactor(b, TangentPoint(b, nleft));
	}

	/*
	 * This collection of TangentPoint routines all compute argmax_{n\ge1}
	 * LogQfactor(Belief, n) using various approximations. If the alternative is
	 * known perfectly, then the value of measurement curve is flat at 0, and
	 * argmax is achieved everywhere. We return 1 in this case.
	 */

	/*
	 * Assumes infinite horizon and takes raw inputs.
	 */
	public static double TangentPoint(double delta, double beta,
			double noiseBeta) {
		// The analytic approximation is currently the one of choice.
		// Adding local search makes it better, but slower.
		return TangentPoint_Analytic(delta, beta, noiseBeta);
	}

	/*
	 * Assumes infinite horizon, and takes a belief as input. Returns the
	 * TangentPoint values for each alternative.
	 */
	public static double[] TangentPoint(Belief b) {
		double[] nstar = new double[b.M()];
		double[] delta = Delta(b);
		for (int i = 0; i < b.M(); i++)
			nstar[i] = TangentPoint(delta[i], b.beta[i], b.noiseBeta[i]);
		return nstar;
	}

	/*
	 * Takes a finite horizon, specified through the argument nleft. The nleft
	 * argument is the maximum number of measurements we will be allowed to
	 * take, and this nleft value can truncate TangentPoint's output. Takes a
	 * belief as input. Returns the TangentPoint values for each alternative.
	 */
	public static double[] TangentPoint(Belief b, double nleft) {
		double[] nstar = TangentPoint(b);
		for (int i = 0; i < b.M(); i++) {
			if (nstar[i] > nleft)
				nstar[i] = nleft;
		}
		return nstar;
	}

	/*
	 * Uses an analytic approximation to compute the tangent point.
	 */
	public static double TangentPoint_Analytic(double delta, double beta,
			double noiseBeta) {
		if (beta == Double.POSITIVE_INFINITY)
			return 1;
		double t = beta * delta * delta;
		double n0 = beta / noiseBeta;
		double nstar = (-1 + t + Math.sqrt(1 + 6 * t + t * t)) * n0 / 4;
		if (nstar < 1)
			nstar = 1;
		return nstar;
	}

	public static double[] TangentPoint_Analytic(Belief b) {
		double[] nstar = new double[b.M()];
		double[] delta = Delta(b);
		for (int i = 0; i < b.M(); i++)
			nstar[i] = TangentPoint_Analytic(delta[i], b.beta[i],
					b.noiseBeta[i]);
		return nstar;
	}

	/*
	 * Uses an analytic approximation to compute the tangent point, and then
	 * does some local search around this point.
	 */
	public static double TangentPoint_AnalyticLocal(double delta, double beta,
			double noiseBeta) {
		int how_far = 50;
		if (beta == Double.POSITIVE_INFINITY)
			return 1;
		double nstar = TangentPoint_Analytic(delta, beta, noiseBeta);

		if (debug > 0)
			System.out
					.printf(
							"TangentPoint_AnalyticLocal(): delta=%f beta=%f noiseBeta=%f\n",
							delta, beta, noiseBeta);
		try {
			double bestn = Math.ceil(nstar);
			double bestlogq = LogQfactor(delta, beta, bestn * noiseBeta)
					- Math.log(bestn);
			if (debug > 0)
				System.out
						.printf(
								"TangentPoint_AnalyticLocal(): nstar=%f ceil(nstar)=%f logq=%f\n",
								nstar, bestn, bestlogq);

			for (int sgn = -1; sgn <= 1; sgn += 2) {
				for (int j = 1; j <= how_far; j++) {
					double n = Math.ceil(nstar) + sgn * j;
					if (n < 1)
						continue;
					double logq = LogQfactor(delta, beta, n * noiseBeta)
							- Math.log(n);
					if (debug > 2)
						System.out.printf(
								"TangentPoint_AnalyticLocal(): n=%f logq=%f\n",
								n, logq);
					if (logq > bestlogq) {
						bestlogq = logq;
						bestn = n;
					}
				}
			}
			if (debug > 0)
				System.out.printf("TangentPoint_AnalyticLocal(): bestn=%f\n",
						bestn);
			return bestn;
		} catch (Exception e) {
			System.err.println(e);
			System.err
					.println("TangentPoint_Analytic(): Local search failed. Returning TangentPoint_Analytic()");
			return nstar;
		}
	}

	/*
	 * Takes raw inputs, and computes a brute force approximation by examining
	 * logq values up to maxN. Can be used for an infinite horizon, in which
	 * case maxN should just be "large enough", or a finite horizon in which
	 * case maxN can be the horizon.
	 */
	public static double TangentPoint_BruteForce(double delta, double beta,
			double noiseBeta, int maxN) throws Exception {
		if (beta == Double.POSITIVE_INFINITY)
			return 1;
		double[] logq = new double[maxN];
		for (int n = 1; n <= maxN; n++) {
			logq[n - 1] = LogQfactor(delta, beta, n * noiseBeta) - Math.log(n);
		}
		if (debug > 1)
			System.out.println(Arrays.toString(logq));
		return MathPF.argmax(logq) + 1; // +1 since index 0 is n=1.
	}

	public static double[] TangentPoint_BruteForce(Belief b, int maxN)
			throws Exception {
		double[] nstar = new double[b.M()];
		double[] delta = Delta(b);
		for (int i = 0; i < b.M(); i++)
			nstar[i] = TangentPoint_BruteForce(delta[i], b.beta[i],
					b.noiseBeta[i], maxN);
		return nstar;
	}

	/*
	 * Takes raw inputs, and computes a Fibonacci search up to maxN. Can be used
	 * for an infinite horizon, in which case maxN should just be
	 * "large enough", or a finite horizon in which case maxN can be the
	 * horizon.
	 */
	public static double TangentPoint_Fibonacci(double delta, double beta,
			double noiseBeta, int maxN) throws Exception {
		if (beta == Double.POSITIVE_INFINITY)
			return 1;
		int bestn = 0;
		double bestlogq = Double.NEGATIVE_INFINITY;
		int lastn = 1;
		int n = 1;
		while (n <= maxN) {
			double logq = LogQfactor(delta, beta, n * noiseBeta) - Math.log(n);
			if (logq >= bestlogq) {
				bestlogq = logq;
				bestn = n;
			}

			// Fibonacci increment
			int tmp = n;
			n = n + lastn;
			lastn = tmp;
		}
		return bestn;
	}

	public static double[] TangentPoint_Fibonacci(Belief b, int maxN)
			throws Exception {
		double[] nstar = new double[b.M()];
		double[] delta = Delta(b);
		for (int i = 0; i < b.M(); i++)
			nstar[i] = TangentPoint_Fibonacci(delta[i], b.beta[i],
					b.noiseBeta[i], maxN);
		return nstar;
	}

	/*
	 * These functions allow computation of the standard upper stopping
	 * boundary approximation comes from the WSC 2009 paper. They use the
	 * btilde function from equation (13), in the draft on 4/26/2009.
	 * The version with a single argument uses the standardized input, and
	 * the version with several inputs standardizes the inputs and then
	 * calls the version with the single input.
	 */
	public static double ApproxStdUpperBoundary(double logc, double beta, double noisebeta) {
	  	double c = Math.exp(logc);
		double noisevar = 1 / noisebeta;

		// WSC_beta is the beta from the WSC paper, not to be confused
		// with the precision which is often labeled beta.
		double WSC_beta = Math.pow(c * noisevar, -1.0 / 3.0);
		double gamma = Math.pow((c * c) / noisevar, 1.0 / 3.0);

		// t is the effective # times sampled
		// s is the standardized input to the btilde function.
		double t = beta / noisebeta;
		double s = 1 / (gamma * t);

		double btilde = ApproxStdUpperBoundary(s);
		double barrier = btilde / WSC_beta;
		return barrier;
	}

	/* This is the standardized upper boundary, or btilde function. */
	public static double ApproxStdUpperBoundary(double s) {
		assert (s >= 0);
		if (s <= 1)
			return 0.233 * Math.pow(s, 2);
		else if (s <= 3) {
			double s4 = Math.pow(s, 4);
			double s3 = Math.pow(s, 3);
			double s2 = Math.pow(s, 2);
			return 0.00537 * s4 - 0.06906 * s3 + .3167 * s2 - .02326 * s;
		} else if (s <= 40) {
			return .705 * Math.sqrt(s) * Math.log(s);
		} else {
			double term = s * Math.pow(2 * Math.log(s), 1.4);
			term = term - Math.log(32 * Math.PI);
			return .642 * Math.sqrt(term);
		}
	}


	/*
	 * 
	 * UNIT TESTS
	 */

	/*
	 * Call the test-suite. It takes an optional argument, which is the test
	 * number to run. If no argument is passed it runs all the tests.
	 */
	public static void main(String args[]) throws Exception {
		if (args.length == 0) {
			TestAll();
		} else if (args.length == 1) {
			Integer n = new Integer(args[0]);
			if (runtest(n) == -1)
				System.err.println("test not found");
		} else {
			System.err.println("usage: Util [test_num]");
		}
	}

	/*
	 * Run all the tests. Returns true if all the tests succeeded, and false if
	 * at least one failed.
	 */
	public static void TestAll() throws Exception {
		/*
		 * Run all the tests. Do this by trying the tests 1,2,... until a test
		 * is not found.
		 */
		boolean ok = true;
		for (int n = 1;; n++) {
			int status = runtest(n);
			if (status == -1)
				break; // stop when test n not found.
			else if (status == -2)
				ok = false;
			// otherwise status == 0.
		}
		if (!ok)
			throw new Exception("Test failed");
	}

	/*
	 * Run test number n. This is a function named "testn", where n is filled in
	 * with the test number. Returns 0 if the test succeeded, -1 if the test was
	 * not found, and -2 if the test was found but failed. Also prints an error
	 * message if it failed. It may also fail for some unexpected reason, in
	 * which case it throws that exception.
	 */
	public static int runtest(int n) throws Exception {
		Method method;
		try {
			method = Util.class.getDeclaredMethod("test" + n, new Class[] {});
		} catch (NoSuchMethodException e) {
			return -1; // test not found.
		}
		try {
			method.invoke(null);
		} catch (InvocationTargetException target_e) {
			target_e.getCause().printStackTrace();
			System.err.println("test " + n + " failed");
			return -2;
		}
		return 0;
	}

	public static void test1() {
		/*
		 * This test case was written while debugging a larger problem. The
		 * largest Q-factor should be index 2 because it has largest mu and beta
		 * smaller or equal to all others.
		 */
		double mu[] = new double[5];
		double beta[] = new double[5];
		mu[0] = -0.16633767489282722;
		mu[1] = -0.2155338556059685;
		mu[2] = 0.4578922457532005;
		mu[3] = -0.028860139275183452;
		mu[4] = 0.1853445116297124;
		beta[0] = 3;
		beta[1] = 3;
		beta[2] = 2;
		beta[3] = 2;
		beta[4] = 3;
		double result[] = Qfactor(new Belief(mu, beta));
		System.out.println("test1");
		if (MathPF.argmax(result) != 2) {
			System.err.print("Error in test 1: result=" + result[0]);
			for (int i = 0; i < result.length; i++)
				System.err.print("," + result[i]);
			System.err.print("\n");
		}
	}

	public static void test2() {
		double mu[] = new double[3];
		double beta[] = new double[3];
		mu[0] = 0;
		mu[1] = 2;
		mu[2] = 1;
		beta[0] = 1;
		beta[1] = 1;
		beta[2] = 1;
		double result[] = Influence(new Belief(mu, beta));
		System.out.println("test2");
		if (MathPF.argmax(result) != 1)
			System.err.println("Error in test2: result="
					+ Arrays.toString(result));
	}

	/* Compute the Q-factors with a small noise beta. */
	public static void test3() {
		double[] mu = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		double[] beta = { 17, .7, .8, .4, 1.2, 1.3, 2, 2.3, .82, .8 };
		double noiseBeta = .0278;
		double[] q = Qfactor(new Belief(mu, beta, noiseBeta));
		int best = MathPF.argmax(q);
		System.out.println("test3");
		if (best != 9)
			System.err.println("Error in test3: best=" + best + " q="
					+ Arrays.toString(q));
	}

	/* More computing the Q-factors with a small noise beta. */
	public static void test4() {
		double[] mu = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11.9 };
		double[] beta = { 17, .7, .8, .8, 1.2, 1.3, 2, 2.3, 1.75, 1.72 };
		double noiseBeta = .0278;
		double[] q = Qfactor(new Belief(mu, beta, noiseBeta));
		int best = MathPF.argmax(q);
		System.out.println("test4: q=" + Arrays.toString(q));
		if (best != 9)
			System.err.println("Error in test4: best=" + best + " q="
					+ Arrays.toString(q));
	}

	/* Want to check that the Q-factors are always computed as positive. */
	public static void test5() {
		double[] mu = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11 };
		double[] beta = { 17, 18, 10, 12, 12, 13, 20, 23, 17, 17 };
		double noiseBeta = .0278;
		double[] q = Qfactor(new Belief(mu, beta, noiseBeta));
		// int best = MathPF.argmax(q);
		System.out.println("test5: q=" + Arrays.toString(q));
	}

	/* Test printing out very small doubles. */
	public static void test6() {
		double x = Double.MIN_VALUE;
		double[] a = { Double.MIN_VALUE, Double.MIN_VALUE };
		System.out.println("test6: x=" + x + " a=" + Arrays.toString(a));
	}

	/*
	 * This test checks TangentPoint, comparing it to a brute force computation.
	 * It outputs nstar computed both ways, analytically and by brute force. It
	 * also outputs the KG factor evaluated at both points. One can then plot
	 * the two nstars vs. delta to check that they agree closely, as well as the
	 * two log-q factors to check that they agree.
	 */
	public static void test7() throws Exception {
		double beta = 1.5;
		double noiseBeta = 6;
		System.out.println("# test7: " + "nstar(analytic)\t"
				+ " nstar(brute force)\t" + " logq(analytic)\t"
				+ " logq(brute force)\t" + " delta\t" + " beta\t"
				+ " noiseBeta");
		for (int i = 0; i < 100; i++) {
			double delta = .1 * i;
			double nstar_analytic = TangentPoint(delta, beta, noiseBeta);
			double logq_analytic = LogQfactor(delta, beta, noiseBeta
					* nstar_analytic)
					- Math.log(nstar_analytic);
			double nstar_brute = TangentPoint_BruteForce(delta, beta,
					noiseBeta, 5);
			double logq_brute = LogQfactor(delta, beta, noiseBeta * nstar_brute)
					- Math.log(nstar_brute);
			System.out.println(nstar_analytic + "\t" + nstar_brute + "\t"
					+ logq_analytic + "\t" + logq_brute + "\t" + delta + "\t"
					+ beta + "\t" + noiseBeta);
		}
	}

	/* Brute force computation of argmax_{n\ge1} LogQfactor(b,n) */
	public static double test78_helper(Belief b) throws Exception {
		int upto = 1500;
		double[] logq_list = new double[upto];
		double[] n_list = new double[2];
		for (int n = 1; n <= upto; n++) {
			n_list[0] = n_list[1] = n;
			double[] logq = LogQfactor(b, n_list);
			logq_list[n - 1] = logq[0];
		}
		// System.out.println(Arrays.toString(logq_list));
		return MathPF.argmax(logq_list) + 1; // +1 since index 0 is n=1.
	}

	/*
	 * Like test7, in that it tests the accuracy of TangentPoint, except that it
	 * varies beta instead of delta.
	 */
	public static void test8() throws Exception {
		double delta = 0.5;
		double noiseBeta = 1;
		System.out.println("# test8: " + "nstar(analytic)\t"
				+ " nstar(brute force)\t" + " logq(analytic)\t"
				+ " logq(brute force)\t" + " delta\t" + " beta\t"
				+ " noiseBeta");
		for (int i = 1; i <= 100; i++) {
			double beta = i;
			double nstar_analytic = TangentPoint(delta, beta, noiseBeta);
			double logq_analytic = LogQfactor(delta, beta, noiseBeta
					* nstar_analytic)
					- Math.log(nstar_analytic);
			double nstar_brute = TangentPoint_BruteForce(delta, beta,
					noiseBeta, 100);
			double logq_brute = LogQfactor(delta, beta, noiseBeta * nstar_brute)
					- Math.log(nstar_brute);
			System.out.println(nstar_analytic + "\t" + nstar_brute + "\t"
					+ logq_analytic + "\t" + logq_brute + "\t" + delta + "\t"
					+ beta + "\t" + noiseBeta);
		}
	}

	/*
	 * For an alternative with a fixed value of delta, beta, and betanoise, it
	 * considers another alternative with the same betanoise. For a range of
	 * this alternative's delta, it varies this alternative's beta until it
	 * finds the first beta that causes envelope KG to prefer to measure it.
	 */
	public static void test9() throws Exception {
		double delta0 = 0.5;
		double beta0 = 1;
		double noiseBeta = 1;
		double logq0 = EnvelopeLogQfactor(delta0, beta0, noiseBeta);
		System.out.println("# test9: delta0=" + delta0 + " beta0 =" + beta0
				+ " noiseBeta=" + noiseBeta);
		System.out.println("# test9: delta\tcritical beta");
		for (double delta = 0; delta <= 10; delta += .1) {
			double beta = test9_helper(delta, logq0, noiseBeta);
			System.out.println(delta + "\t" + beta);
		}
	}

	// Let beta^* = inf[beta>0] logq(delta,beta) < logq0.
	// This function approximates beta^*. Call its return value beta_ub.
	// The function guarantees that beta_ub >= beta^*, and that beta_ub -
	// beta < tol, where tol = .1;
	public static double test9_helper(double delta, double logq0,
			double noiseBeta) throws Exception {
		double logq;
		double beta_lb = 0;
		double beta_ub = 100;
		double tol = .1;

		// Check that our upper bound on beta really is such. We know
		// that the lower bound is ok because the LogQ of beta=0 is
		// infinity. When beta is big, we expect logq to be small, so
		// we check that logq is not bigger than logq0.
		if (EnvelopeLogQfactor(delta, beta_ub, noiseBeta) > logq0)
			throw new Exception("logq(delta=" + delta + ",beta_ub=" + beta_ub
					+ ") < logq0");

		// bisection search
		while (beta_ub - beta_lb > tol) {
			double beta = (beta_ub + beta_lb) / 2;
			logq = EnvelopeLogQfactor(delta, beta, noiseBeta);
			if (logq > logq0)
				beta_lb = beta;
			else if (logq < logq0)
				beta_ub = beta;
			else
				return beta;
		}
		return beta_ub;
	}

	// Tests that various functions work with a prior that has an
	// alternative known perfectly, i.e., that has infinite precision.
	public static void test10() throws Exception {
		// This creates a belief where one alternative is known
		// perfectly, and another has a standard normal prior.
		Belief b = Belief.OneKnownAlternative(1);

		double[] influence = Influence(b);
		// The first influence should be -infinity, and the second should be
		// strictly greater than 0.
		if (Double.isNaN(influence[0]) || Double.isNaN(influence[1]))
			System.err.println("Error in test10: influence[] contains NaN");
		else {
			if (influence[0] != Double.NEGATIVE_INFINITY)
				System.err
						.println("Error in test10: influence[0] should be -Infinity");
			if (influence[1] == Double.NEGATIVE_INFINITY)
				System.err
						.println("Error in test10: influence[1] should not be -Infinity");
		}

		double[] logq = EnvelopeLogQfactor(b);
		// The first Q-factor should be 0, and the second should be
		// strictly greater than 0.
		if (Double.isNaN(logq[0]) || Double.isNaN(logq[1]))
			System.err.println("Error in test10: logq[] contains NaN");
		else {
			if (logq[0] != Double.NEGATIVE_INFINITY)
				System.err
						.println("Error in test10: logq[0] should be -Infinity");
			if (logq[1] == Double.NEGATIVE_INFINITY)
				System.err
						.println("Error in test10: logq[1] should not be -Infinity");
		}
	}

	public static void test11() throws Exception {
		// double noiseBeta = 1;
		double sigma = 1e5;
		double t0 = 1;

		double logq[] = new double[500];
		// double delta = 0;
		double noiseBeta = 1 / (sigma * sigma);
		double beta = noiseBeta * t0;

		System.out.println("# n\tlogq[n]");
		logq[0] = Double.NEGATIVE_INFINITY; // value of 0 measurements
		for (int n = 1; n < logq.length; n++) {
			logq[n] = LogQfactor(0, beta, n * noiseBeta);
			System.out.printf("%d\t%f\n", n, logq[n]);
		}

		// The limit for n large should be about log(sigma/sqrt(2*pi)).
	}
}
