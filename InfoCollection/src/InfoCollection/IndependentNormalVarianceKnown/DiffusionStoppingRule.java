package InfoCollection.IndependentNormalVarianceKnown;
import InfoCollection.ThresholdStoppingRule;
import InfoCollection.ThresholdStoppingRuleSeq;

/*
 * This stopping rule computes the stopping decision that would be optimal if,
 * after sampling the next alternative, we were not allowed to sample any other
 * alternatives in the future.  In this case, the optimal stopping rule is
 * given by checking the stopping rule that is optimal for one alternative on
 * each of the different alternatives.
 */

public class DiffusionStoppingRule extends BayesStoppingRule {
	double logc;

	public DiffusionStoppingRule(Belief b, double logc) {
		super(b);
		this.logc = logc;
	}

	public boolean ShouldStop() {
		int x;
		double[] delta = Util.Delta(belief);
		for (x=0;x<belief.M();x++) {
		  double barrier = Util.ApproxStdUpperBoundary(logc,belief.beta[x],belief.noiseBeta[x]);
		  // If delta is above the barrier, then there is not enough
		  // value to merit measuring this alternative, but there might
		  // be merit in measuring some other alternative.  If delta is
		  // below the barrier, then it is definitely worth continuing.
		  if (delta[x] < barrier)
		    return false;
		}
		return true; // No alternatives merited continuing
	}

	@Override
	public String toString() {
		return "DiffusionStoppingRule logc=" + logc;
	}

	public String Name() {
		return "DiffusionStoppingRule";
	}

	/*
	 * Create custom and standard stopping rule sequences. These are mostly for
	 * convenience.
	 */
	 /*
	 public static ThresholdStoppingRuleSeq Seq(Belief b, double[] logc) {
	   Double[] _logc = new Double[logc.length];
	   for (int i=0; i<logc.length; i++)
	     _logc[i] = logc[i];
	   return new ThresholdStoppingRuleSeq<Double>(_logc, new DiffusionStoppingRule(b,logc[0]));
	 }

	 public static ThresholdStoppingRuleSeq Seq(double[] logc) {
	   Double[] _logc = new Double[logc.length];
	   for (int i=0; i<logc.length; i++)
	     _logc[i] = logc[i];
	   return new StoppingRuleSeq<Double>(_logc, new DiffusionStoppingRule(logc[0]));
	 }
	 
	 public static ThresholdStoppingRuleSeq Seq() {
	   double logc[] =
	   {-1,-2,-3,-4,-5,-8,-10,-15,-20,-30,-40,-50,-60,-70,-80,-90,-100,-150};
	   return Seq((double[])logc);
	 }

	 public static ThresholdStoppingRuleSeq Seq(Belief b)
	 {
	   double logc[] = {-1,-2,-3,-4,-5};
	   return Seq(b,(double[])logc);
	 }
	 */
}
