package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;

import InfoCollection.util.MathPF;
import InfoCollection.util.Normal;

/*
 * Maintains both an allocation and a budget that is "optimal" given the current
 * belief.  Uses the Bonferonni inequality, but replaces the sum used by LLck
 * with a max to calculate the value of an allocation according to which the
 * allocation is optimal.  From these can be derived a stopping rule (EOCc1)
 * and a sampling rule (LLseqc1).
 */
public class LLc1 extends LLck {

	public LLc1(Belief b, double c) {
		super(b, c);
		do_LLc1 = true;
	}

	public LLc1(Belief b, double c, int method) {
		super(b, c, method);
		do_LLc1 = true;
	}

	/*
	 * Simply by changing the do_LLc1 bit to true, LogGrossAllocValue in
	 * LLck uses the max of the logvalues instead of their sum, which is
	 * the correct thing to do for LLc1.
	 */

	/*
	 * Calculate the logarithm of the gross (not including sampling costs) value
	 * of an allocation, using the Bonferonni inequality, and a max instead of a
	 * sum.
	 */
	/*
	public static double LogGrossAllocValue(double[] alloc, Belief b, double c) {
		double logval, d, s, s_best, s_i;
		double[] logvalList = new double[b.M()];
		int i, best;

		if (alloc.length != b.M()) {
			System.err.println("Allocation is the wrong length");
			return 0; // wrong allocations have no value.
		}

		best = MathPF.argmax(b.mu);
		s_best = Util.SigmaTilde(b.beta[best], alloc[best] * b.NoiseBeta(best));
		for (i = 0; i < b.M(); i++) {
			if (i == best) {
				logvalList[best] = Double.NEGATIVE_INFINITY;
				continue;
			}
			s_i = Util.SigmaTilde(b.beta[i], alloc[i] * b.NoiseBeta(i));
			s = Math.sqrt(s_best * s_best + s_i * s_i);
			// PF: 11/13/2009.  Fixed a bug with this if statement.
			// Without it, our logvalList[i] is NaN.
			if (s == 0) {
			  logvalList[i] = Double.NEGATIVE_INFINITY;
			  continue;
			}
			d = (b.mu[best] - b.mu[i]) / s;
			logvalList[i] = Math.log(s) + Normal.LogPsi(d);
		}
		logval = MathPF.max(logvalList);
		if (debug > 2)
			System.out.printf(
			    "LogGrossAllocValue: logval=%f logvalList=%s alloc=%s\n",
			    logval, Arrays.toString(logvalList), Arrays.toString(alloc));
		return logval;
	}
	*/

}
