package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.util.MathPF;

public class LLseqBestNet extends BayesSamplingRule {
	int debug = 0;
	double c;
	LLck llc;

	public LLseqBestNet(Belief b, double c) {
		super(b);
		this.c = c;
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		llc = new LLck(belief, c); // note that LLck copies belief.
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		llc.RecordMeasurement(x, y);
	}

	public int GetMeasurementDecision() {
		double[] alloc;
		alloc = llc.GoodAlloc();
		double[] net = new double[alloc.length];

		// Now see which alternative has the allocation with the
		// largest net value when considered on its own.
		try {
			double[] logq = Util.LogQfactor(belief, alloc);
			for (int i = 0; i < logq.length; i++) {
				// logq is the log of the average gross benefit per
				// measurement. exp(logq)-c is the average net benefit
				// per measurement. Then multiply by the number of
				// measurements (alloc[i]) to get the total net
				// benefit.
				net[i] = alloc[i] * (Math.exp(logq[i]) - c);
			}
		} catch (Exception e) {
			System.err.println(e);
			System.err
					.println("Continuing with a random measurement decision.");
			for (int i = 0; i < net.length; net[i++] = 0)
				;
		}

		int decision = MathPF.argmax(net);

		if (debug > 0) {
			int tau_decision = MathPF.argmax(alloc);
			if (decision != tau_decision)
				System.out.printf("decision=%d tau_decision=%d\n", decision,
						tau_decision);
		}
		return decision;
	}

	public boolean ShouldStop() {
		return (llc.GoodBudget() > 0);
	}

	@Override
	public String toString() {
		return "LLseqBestNet c=" + c;
	}

	public String Name() {
		return "LLseqBestNet";
	}
}
