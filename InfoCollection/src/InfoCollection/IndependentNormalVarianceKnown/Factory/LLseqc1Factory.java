package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class LLseqc1Factory implements BayesSamplingRuleFactory {
	double c;
	int method;
	public LLseqc1Factory(double c, int method) { this.c = c; this.method=method; }
	public LLseqc1Factory(double c) { this.c = c; method=0; }
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new LLseqc1(b,c,method);
	}
}
