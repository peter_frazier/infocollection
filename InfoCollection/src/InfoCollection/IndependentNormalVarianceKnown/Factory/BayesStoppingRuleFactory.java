package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

/*
 * This interface is implemented by classes like KGStoppingRuleFactory.
 */
public interface BayesStoppingRuleFactory {
	public BayesStoppingRule CreateStoppingRule(Belief b);
}
