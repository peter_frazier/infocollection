package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;
import InfoCollection.NotInformativeException;

public class EOCc1Factory implements BayesStoppingRuleFactory {
	double c;
	int method;
	public EOCc1Factory(double c, int method) { this.c = c; this.method = method; }
	public EOCc1Factory(double c) { this.c = c; method = 0; }
	public BayesStoppingRule CreateStoppingRule(Belief b) {
		return new EOCc1(b, c, method);
	}
}
