package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class DiffusionStoppingRuleFactory implements BayesStoppingRuleFactory {
	double logc;
	public DiffusionStoppingRuleFactory(double logc) { this.logc = logc; }
	public BayesStoppingRule CreateStoppingRule(Belief b) {
	  return new DiffusionStoppingRule(b, logc);
	}
}
