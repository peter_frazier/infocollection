package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class DiffusionSamplingRuleFactory implements BayesSamplingRuleFactory {
  	double logc;
	public DiffusionSamplingRuleFactory(double logc) { this.logc = logc; }
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new DiffusionSamplingRule(b,logc);
	}
}
