package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class DiffusionStoppingRule2Factory implements BayesStoppingRuleFactory {
	double logc;
	public DiffusionStoppingRule2Factory(double logc) { this.logc = logc; }
	public BayesStoppingRule CreateStoppingRule(Belief b) {
	  return new DiffusionStoppingRule2(b, logc);
	}
}
