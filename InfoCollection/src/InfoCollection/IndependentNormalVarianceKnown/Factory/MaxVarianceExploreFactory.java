package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class MaxVarianceExploreFactory implements BayesSamplingRuleFactory {
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new MaxVarianceExplore(b);
	}
}
