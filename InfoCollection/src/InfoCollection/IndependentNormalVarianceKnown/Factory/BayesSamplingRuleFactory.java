package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

/*
 * This interface is implemented by classes like KGSamplingRuleFactory.
 */
public interface BayesSamplingRuleFactory {
	public BayesSamplingRule CreateSamplingRule(Belief b);
}
