package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class KGESamplingRuleFactory implements BayesSamplingRuleFactory {
	public KGESamplingRuleFactory() { }
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new KGEnvelope(b);
	}
}
