package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class KGStoppingRuleFactory implements BayesStoppingRuleFactory {
	double logc;
	public KGStoppingRuleFactory(double logc) { this.logc = logc; }
	public BayesStoppingRule CreateStoppingRule(Belief b) {
		return new KGStoppingRule(b, logc);
	}
}
