package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;
import InfoCollection.NotInformativeException;

public class EOCckFactory implements BayesStoppingRuleFactory {
	double c;
	int method;
	public EOCckFactory(double c, int method) { this.c = c; this.method = method; }
	public EOCckFactory(double c) { this.c = c; method = 0; }
	public BayesStoppingRule CreateStoppingRule(Belief b) {
		return new EOCck(b, c, method);
	}
}
