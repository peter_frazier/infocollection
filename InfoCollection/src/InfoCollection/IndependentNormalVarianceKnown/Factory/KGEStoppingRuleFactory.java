package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class KGEStoppingRuleFactory implements BayesStoppingRuleFactory {
	double logc;
	int method;
	public KGEStoppingRuleFactory(double logc, int method) { this.logc = logc; this.method = method; }
	public KGEStoppingRuleFactory(double logc) { this.logc = logc; method = -1; }
	public BayesStoppingRule CreateStoppingRule(Belief b) {
		if (method == -1) // use the default value
			return new KGEnvelopeStoppingRule(b, logc);
		else
			return new KGEnvelopeStoppingRule(b, logc, method);
	}
}
