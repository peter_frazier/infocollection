package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class LLseqBestNetFactory implements BayesSamplingRuleFactory {
	double c;
	public LLseqBestNetFactory(double c) { this.c = c; }
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new LLseqBestNet(b,c);
	}
}
