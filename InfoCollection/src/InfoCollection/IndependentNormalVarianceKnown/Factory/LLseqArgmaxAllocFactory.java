package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class LLseqArgmaxAllocFactory implements BayesSamplingRuleFactory {
	double c;
	int method;
	public LLseqArgmaxAllocFactory(double c, int method) { this.c = c; this.method=method; }
	public LLseqArgmaxAllocFactory(double c) { this.c = c; method=0; }
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new LLseqArgmaxAlloc(b,c,method);
	}
}
