package InfoCollection.IndependentNormalVarianceKnown.Factory;
import InfoCollection.IndependentNormalVarianceKnown.*;

public class KGSamplingRuleFactory implements BayesSamplingRuleFactory {
	//public KGSamplingRuleFactory() { }
	public BayesSamplingRule CreateSamplingRule(Belief b) {
		return new KG(b);
	}
}
