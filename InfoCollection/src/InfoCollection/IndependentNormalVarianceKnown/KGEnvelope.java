package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;

import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;

public class KGEnvelope extends BayesSamplingRule {
	double horizon, nleft;
	Boolean debug = false;

	public KGEnvelope(Belief b) {
		super(b);
		horizon = Double.POSITIVE_INFINITY;
	} // infinite horizon

	public KGEnvelope(Belief b, double horizon) {
		super(b);
		this.horizon = horizon;
	}

	public int GetMeasurementDecision() throws Exception {
		double[] logq = GetLogQ();
		int x = MathPF.argmax(logq);
		if (debug) {
			double[] logkg = Util.LogQfactor(belief);
			int xkg = MathPF.argmax(logkg);
			if (xkg != x) {
				System.out.println(belief); // Debug
				System.out.println("logq=" + Arrays.toString(logq)); // Debug
				System.out.printf(
						"KGEnvelope decision is %d, KG decision is %d\n", x,
						xkg);
			}
		}
		return x;
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		nleft = horizon;
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		nleft--;
	}

	@Override
	public String toString() {
		return "KGEnvelope (horizon=" + horizon + ")";
	}

	public double[] GetLogQ() throws Exception {
		double[] logq;
		if (horizon == Double.POSITIVE_INFINITY)
			logq = Util.EnvelopeLogQfactor(belief);
		else
			logq = Util.EnvelopeLogQfactor(belief, nleft);
		return logq;
	}

	public static void main(String args[]) throws Exception {
		TestExamples.Example1(new KGSimple(), true);
	}
};
