package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;
import java.util.LinkedList;

import umontreal.iro.lecuyer.probdist.NormalDist;
import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;

import com.adventuresinsoftware.tuple.Tuple;

/*
 * This is the general-purpose implementation of theOCBA-EOC procedure from He,
 * Chick & Chen 2007.  It handles any choice of initial belief, m, tau, n0.
 *
 * Historical note: this was first called HeChCh07a.
 */

public class HeChCh07 extends BayesSamplingRule {
	LinkedList<Integer> measurementsToGive;
	/*
	 * mHeChCh is the number of alternatives to replicate in each allocation. We
	 * give it this funny variable name because HeChCh07 simply call it "m", but
	 * we have already used "M" to indicate the number of alternatives, so I
	 * didn't want to confuse things any more than they already are.
	 */
	int mHeChCh;

	/*
	 * tau is the number of replications to give to each replicated alternative.
	 */
	int tau;

	/*
	 * Number of measurements to allocate to each alternative in the first
	 * stage.
	 */
	int n0;

	public HeChCh07(Belief b0, int m, int tau, int n0) {
		super(b0);
		measurementsToGive = new LinkedList<Integer>();
		this.mHeChCh = m;
		this.tau = tau;
		this.n0 = n0;
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		measurementsToGive.clear();
		/*
		 * OCBA-EOC does a first stage where n0 measurements are allocated to
		 * each alternative.
		 */
		for (int i = 0; i < n0; i++)
			for (int x = 0; x < M(); x++)
				measurementsToGive.add(x);
	}

	/*
	 * Evaluates the function (a,b) -> Exp[(a+bZ)^+], where Z is a standard
	 * normal random variable, b is strictly positive, and a is any real number.
	 * Do not confuse this with Psi(). I think it is b*Psi(-a/b).
	 */
	public double bf(double a, double b) {
		double z = a / b;
		// System.out.println("bf(" + a + "," + b + ")");
		// return b * Normal.Psi(z)
		return a * NormalDist.cdf01(z) + b * NormalDist.density(0, 1, z);
	}

	/*
	 * Allocate more measurements according to the algorithm, and put them into
	 * the measurementsToGive list in round-robin fashion.
	 */
	public void AllocateMore() {
		Tuple D[] = new Tuple[M()];
		int i, b = MathPF.argmax(belief.mu);

		// System.out.println("AllocateMore");

		/*
		 * Compute D[i] for each alternative according to the procedure in the
		 * left hand column on page 958 of the paper. We compute D[i] when i<>b
		 * in a straightforward fashion in the loop. D[b] is more complicated,
		 * being computed as the sum of terms over i<>b. Also see notes on
		 * 10/9/2007, p3.
		 */
		double Db = 0;
		for (i = 0; i < M(); i++) {
			double s2bi, s2bii, s2bib; // standard deviations.
			double fbi, fbii, fbib; // values for bf.
			if (i == b)
				continue;
			s2bi = Math.sqrt(1 / belief.beta[b] + 1 / belief.beta[i]);
			s2bii = Math.sqrt(1 / belief.beta[b] + 1
					/ (belief.beta[i] + belief.noiseBeta[i]));
			s2bib = Math.sqrt(1 / belief.beta[i] + 1
					/ (belief.beta[b] + belief.noiseBeta[b]));
			fbi = bf(belief.mu[b] - belief.mu[i], s2bi);
			fbii = bf(belief.mu[b] - belief.mu[i], s2bii);
			fbib = bf(belief.mu[b] - belief.mu[i], s2bib);
			D[i] = new Tuple().add(fbii - fbi).add(i);
			Db += fbib - fbi;
		}
		D[b] = new Tuple().add(Db).add(b);

		// System.out.println(Arrays.toString(D));

		/*
		 * Now find the mHeChCh best (minimum) values of D and allocate tau each
		 * measurements to these alternatives. The sort puts them in ascending
		 * order, so best is first.
		 */
		Arrays.sort(D); // in ascending order, so best is last.
		for (int t = 0; t < tau; t++)
			for (int j = 0; j < mHeChCh && j < M(); j++) {
				// The index of the alternative is at index 1,
				// which is the second element of the tuple.
				measurementsToGive.add((Integer) D[j].get(1));
			}
	}

	public int GetMeasurementDecision() {
		/*
		 * If there are more measurements in the currently allocated bucket,
		 * allocate them. Otherwise need to allocate more.
		 */
		if (measurementsToGive.size() == 0)
			AllocateMore();
		return measurementsToGive.removeFirst();
	}

	@Override
	public String toString() {
		return "HeChCh07 OCBA-EOC (implementation a) m=" + mHeChCh + " tau="
				+ tau + " n0=" + n0 + " belief0=" + belief0;
	}

	public static void main(String args[]) throws Exception {
		Belief b0 = new Belief(10);
		// TestExamples.Example1(new HeChCh07(b0,1,10,10), true);
		TestExamples.Example1(new HeChCh07(b0, 1, 1, 0), true);
		// TestExamples.Example1SpeedTest(new HeChCh07(b0,1,1,0));
	}
};
