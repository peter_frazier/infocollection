package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;
import java.util.Random;

import InfoCollection.NormalTruth;
import InfoCollection.NotInformativeException;
import InfoCollection.Truth;
import InfoCollection.util.MathPF;
import InfoCollection.util.MeanVariance;
import InfoCollection.util.Normal;

/*
 * This class describes a Bayesian prior/posterior on a collection of
 * alternatives variables with common known sampling variance.  The
 * prior/posterior belief on the true means of the samples are independent
 * normal.
 * PF: It wouldn't be that hard to extend this to noise variances that are
 * known but not necessarily equal.
 */
public class Belief implements InfoCollection.Belief {
	public double[] mu; // mean vector
	public double[] beta; // precision vector
	public double[] noiseBeta; // noiseBeta is the precision of the

	// noise, also called \sigma^{-2}.

	/*
	 * Creates a non-informative belief with the given number of alternatives.
	 */
	public Belief(int M) {
		this.mu = new double[M];
		this.beta = new double[M];
		this.noiseBeta = new double[M];
		for (int i = 0; i < M; i++) {
			mu[i] = 0;
			beta[i] = 0;
			noiseBeta[i] = 1;
		}
	}

	/*
	 * Creates a belief with noiseBeta fixed to 1, and the passed mu and beta
	 * arrays.
	 */
	public Belief(double mu[], double beta[]) {
		assert mu.length == beta.length;
		this.mu = mu.clone();
		this.beta = beta.clone();
		this.noiseBeta = new double[M()];
		for (int i = 0; i < M(); i++) {
			this.noiseBeta[i] = 1;
		}
	}

	/* Copy constructor. */
	public Belief(Belief b) {
		mu = b.mu.clone();
		beta = b.beta.clone();
		noiseBeta = b.noiseBeta.clone();
	}

	/*
	 * Creates a homogenous belief with all mu[x] the same and all beta[x] the
	 * same.
	 */
	public Belief(int M, double mu, double beta, double noiseBeta) {
		this.mu = new double[M];
		this.beta = new double[M];
		this.noiseBeta = new double[M];
		for (int x = 0; x < M; x++) {
			this.mu[x] = mu;
			this.beta[x] = beta;
			this.noiseBeta[x] = noiseBeta;
		}
	}

	/*
	 * Creates a belief with noiseBeta fixed as a passed scalar, and the passed
	 * mu and beta arrays.
	 */
	public Belief(double mu[], double beta[], double noiseBeta) {
		assert mu.length == beta.length;
		this.mu = mu.clone();
		this.beta = beta.clone();
		this.noiseBeta = new double[M()];
		for (int i = 0; i < M(); i++) {
			this.noiseBeta[i] = noiseBeta;
		}
	}

	/*
	 * Creates a belief in full generality.
	 */
	public Belief(double mu[], double beta[], double[] noiseBeta) {
		assert mu.length == beta.length;
		this.mu = mu.clone();
		this.beta = beta.clone();
		this.noiseBeta = noiseBeta.clone();
	}

	/*
	 * Creates a belief in which there is one alternative perfectly known with
	 * value 0, and k other alternatives with a standard normal prior. This
	 * prior is used in the WSC paper from 2009 with Steve Chick, usually with
	 * beta0 = (1/10^5)^2 = 1E-10.
	 */
	public static Belief OneKnownAlternative(int k) {
		return OneKnownAlternative(k, 1, 0.1);
	}

	public static Belief OneKnownAlternative(int k, double beta0,
			double noisebeta) {
		return OneKnownAlternative(k, 0, beta0, noisebeta);
	}

	public static Belief OneKnownAlternative(int k, double mu0, double beta0,
			double noisebeta) {
		double mu[] = new double[k + 1];
		double beta[] = new double[k + 1];
		for (int i = 1; i < k + 1; i++) {
			mu[i] = mu0;
			beta[i] = beta0;
		}
		mu[0] = 0;
		beta[0] = Double.POSITIVE_INFINITY; // perfectly known
		return new Belief(mu, beta, noisebeta);
	}

	/*
	 * This function receives the value of an observation (y), and which
	 * alternative the observation measured (x), and uses Bayesian updating to
	 * update the belief about that alternative, i.e. it updates mu[x] and
	 * beta[x].
	 */
	public void Update(int x, double y) throws Exception {
		if (beta[x] == Double.POSITIVE_INFINITY) {
			/*
			 * When beta[x] is infinite the prior is perfect. In this case, as
			 * long as the measurement has finite beta, we just ignore it. If
			 * the measurement also has infinite beta, we compare the
			 * measurement and prior to make sure they agree.
			 */
			if (noiseBeta[x] == Double.POSITIVE_INFINITY && mu[x] != y)
				// Both the prior and the measurement had
				// infinite beta, but they disagreed.
				throw (new Exception());
		} else if (noiseBeta[x] == Double.POSITIVE_INFINITY) {
			/*
			 * Infinite inverse variance means zero variance, which means the
			 * measurement is perfect.
			 */
			beta[x] = Double.POSITIVE_INFINITY;
			mu[x] = y;
		} else if (noiseBeta[x] != 0 || beta[x] != 0) {
			double newbeta = beta[x] + noiseBeta[x];
			mu[x] = (beta[x] * mu[x] + noiseBeta[x] * y) / newbeta;
			beta[x] = newbeta;
		}
		/*
		 * 
		 * The remaining case is when both noiseBeta and beta[x] are zero. Zero
		 * inverse variance means infinite variance, or no information. If both
		 * the prior and the measurement contain no information, we do nothing.
		 */
	}

	/*
	 * Generates the true values from the random number generator according to
	 * the prior.
	 */
	public Truth GenerateTruth(Random rnd) throws NotInformativeException {
		if (!IsInformative())
			throw (new NotInformativeException());
		double[] value = new double[M()];
		double[] sigma = Sigma();
		for (int i = 0; i < M(); i++) {
			value[i] = mu[i] + sigma[i] * rnd.nextGaussian();
		}
		return new NormalTruth(value, noiseBeta);
	}

	/*
	 * Returns whether or not the value of every alternative is integrable under
	 * the belief. This will be the case if the belief is informative.
	 */
	public Boolean IsIntegrable() {
		int x;
		for (x = 0; x < beta.length && beta[x] > 0; x++)
			;
		return (x == beta.length); // true if all have beta[x] > 0
	}

	public Boolean IsInformative() {
		return IsIntegrable();
	}

	/*
	 * Computes the standard deviations of our belief about the true means.
	 */
	public double[] Sigma() {
		double[] sigma = new double[beta.length];
		for (int x = 0; x < beta.length; x++) {
			if (beta[x] == Double.POSITIVE_INFINITY)
				sigma[x] = 0;
			else if (beta[x] == 0)
				sigma[x] = Double.POSITIVE_INFINITY;
			else
				sigma[x] = StrictMath.sqrt(1 / beta[x]);
		}
		return sigma;
	}

	// Standard deviation of the noise. This is known perfectly.
	public double NoiseStdDev(int x) {
		return Math.sqrt(1 / noiseBeta[x]);
	}

	// Variance of the noise
	public double NoiseVar(int x) {
		return 1 / noiseBeta[x];
	}

	// Precision of the noise
	public double NoiseBeta(int x) {
		return noiseBeta[x];
	}

	// Effective number of measurements of alternative i
	public double EffectiveNum(int x) {
		return beta[x] / noiseBeta[x];
	}

	// Number of alternatives
	public int M() {
		return mu.length;
	}

	/*
	 * Expected value of the best alternative. Requires numerical integration.
	 */
	public double ExpMax() {
		return Normal.ExpMax(mu, Sigma());
	}

	/*
	 * Expected opportunity cost of stopping now given the belief state. This
	 * requires numerical integration.
	 */
	public double EOCBayes() {
		return ExpMax() - MathPF.max(mu);
	}

	/*
	 * Estimates the expected bayesian opportunity cost of stopping now given
	 * the mu and beta belief state using Monte Carlo simulation.
	 */
	// PF: this could go into the util.Normal class. It would also require
	// putting a random number generator for normal random variables in
	// that class.
	public MeanVariance EOCBayesMC(int nsamples, Random rnd)
			throws NotInformativeException {
		// int M = mu.length;
		// double[] values = new double[M];
		MeanVariance ans = new MeanVariance();
		double maxmu = MathPF.max(mu);
		for (int s = 0; s < nsamples; s++) {
			Truth truth = GenerateTruth(rnd);
			ans.AddSample(truth.BestValue() - maxmu);
		}
		return ans;
	}

	public MeanVariance EOCBayesMC(int nsamples) throws NotInformativeException {
		return EOCBayesMC(nsamples, new Random());
	}

	/*
	 * Calculates the Bonferonni approximation to expected opportunity cost.
	 * This is defined on pages 4-6 of Chick,Branke,Schmidt 2007 in Informs
	 * Journal on Computing. Note that EOCBonf does not depend on the
	 * measurement precision b.noiseBeta.
	 */
	// PF: an ExpMaxBonf() could go into the util.Normal class, that would
	// contain the heart of this function.
	public double EOCBonf() {
		double EOC, d, s;
		int i, best;
		best = MathPF.argmax(mu);
		EOC = 0;
		for (i = 0; i < M(); i++) {
			if (i == best)
				continue;
			s = Math.sqrt(1 / beta[i] + 1 / beta[best]);
			d = (mu[best] - mu[i]) / s;
			EOC += s * Normal.Psi(d);
		}

		return EOC;
	}

	@Override
	public String toString() {
		return String.format("mu=%s\nbeta=%s\nnoiseBeta=%s",
		    Arrays.toString(mu), 
		    Arrays.toString(beta),
		    Arrays.toString(noiseBeta));
	}

	/*
	 * UNIT TESTS
	 */

	public static void main(String args[]) throws Exception {
		String myclass = "InfoCollection.IndependentNormalVarianceKnown.Belief";
		boolean ok = true;
		System.out.println("Testing " + myclass);
		ok = test1() && ok;
		ok = test2() && ok;
		ok = test3() && ok;
		ok = test4() && ok;

		if (!ok)
			System.out.println(myclass + ": FAIL");
		else
			System.out.println(myclass + ": OK");
	}

	public static boolean test1() {
		boolean ok = true;
		double[] mu = new double[3];
		double[] beta = { 1, 1, 1 };
		double a, eoc0 = 0;
		int i;

		for (a = 0; a < 10; a++) {
			for (i = 0; i < mu.length; i++)
				mu[i] = a;
			Belief b = new Belief(mu, beta);
			double eoc = b.EOCBayes();
			if (a == 0)
				eoc0 = eoc;
			/*
			 * The eoc values for different values of a should all be identical,
			 * since translating every alternative doesn't change the
			 * opportunity cost.
			 */
			double diff = Math.abs(eoc0 - eoc);
			if (diff > 1e-15) {
				System.out.println("test1: ERROR a=" + a + " difference="
						+ diff);
				ok = false;
			}
		}
		if (!ok)
			System.out.println("test1: FAIL");
		else
			System.out.println("test1: OK");
		return ok;
	}

	/* Tests EOCBayes vs EOCBayesMC on randomly generated problems. */
	public static boolean test2() throws Exception {
		boolean ok;
		double[] mu;
		double[] beta;
		double t, abs_t, max_abs_t, sigma;
		int i, M, ntimes;

		ntimes = 10;
		max_abs_t = 0;
		for (i = 0; i < ntimes; i++) {
			/* Generate a random problem */
			Random rnd = new Random();
			M = 1 + rnd.nextInt(9);
			// M = 2;
			mu = new double[M];
			beta = new double[M];
			for (i = 0; i < M; i++) {
				mu[i] = 10 * rnd.nextGaussian();
				sigma = 1 + rnd.nextInt(10);
				beta[i] = 1.0 / (sigma * sigma);
			}
			Belief b = new Belief(mu, beta);
			t = b.test_CmpEOCBayes(false);
			abs_t = Math.abs(t);
			if (abs_t > max_abs_t)
				max_abs_t = abs_t;
		}
		ok = (max_abs_t <= 3);
		System.out.printf("test2: %s max(abs(t-statistic))=%f\n", ok ? "OK"
				: "FAIL", max_abs_t);
		return ok;
	}

	/*
	 * Tests EOCBayes vs EOCBayesMC on a slippage configuration with a
	 * decreasing sequence of sigmas. This is intended to simulate something
	 * similar to the parameters we might see when doing sequential measurement.
	 */
	public static boolean test3() {
		boolean ok;
		int n, i, M = 5, ntimes = 10;
		double s, sigma, max_abs_t;
		double[] mu = new double[M];
		double[] beta = new double[M];

		for (i = 0; i < M; i++)
			mu[i] = i;
		max_abs_t = 0;
		for (n = 0; n < ntimes; n++) {
			s = Math.sqrt(1.0 / (n + 1));
			for (i = 0; i < M; i++) {
				sigma = s;
				beta[i] = 1.0 / (sigma * sigma);
			}
			Belief b = new Belief(mu, beta);
			double t = b.test_CmpEOCBayes(false);
			double abs_t = Math.abs(t);
			if (abs_t > max_abs_t)
				max_abs_t = abs_t;
		}
		ok = (max_abs_t <= 3);
		System.out.printf("test3: %s max(abs(t-statistic))=%f\n", ok ? "OK"
				: "FAIL", max_abs_t);
		return ok;
	}

	/*
	 * Run both EOCBayes() and EOCBayesMC(). Check their answers against each
	 * other and return the t-statistic of the comparison.
	 */
	public double test_CmpEOCBayes(boolean verbose) {
		if (verbose)
			System.out.printf("mu=%s beta=%s\n", Arrays.toString(mu), Arrays
					.toString(beta));

		/* Evaluate using numerical integration and monte carlo. */
		double EOCBayesAns = EOCBayes();
		int mcsamples = 1000000;
		try {
			MeanVariance MCAns = EOCBayesMC(mcsamples);

			/* Get the t-statistic and report the result. */
			double t = (EOCBayesAns - MCAns.SampleMean())
					/ MCAns.SampleMeanDeviation();
			if (verbose)
				System.out.printf(
						"EOCBayes Answer=%f, MonteCarlo Answer=%f, t=%f\n",
						EOCBayesAns, MCAns, t);
			return t;
		} catch (NotInformativeException e) {
			System.out.println(e);
			System.out.printf("FAIL. NotInformativeException, system error?");
			return Double.POSITIVE_INFINITY;
		}
	}

	/*
	 * Compare EOCBonf and EOCBayes on beliefs with only one alternative
	 * unknown. They should be identical, to numerical imprecision.
	 */
	public static boolean test4() throws Exception {
		boolean ok = true;
		int k = 1;
		double noisebeta = 1; // doesn't matter for this test.
		for (double mu0 = -2; mu0 <= 2; mu0++) {
			for (double logbeta0 = -10; logbeta0 <= 10; logbeta0 += 2) {
				double beta0 = Math.exp(logbeta0);
				Belief b = OneKnownAlternative(k, mu0, beta0, noisebeta);
				double eocbayes = b.EOCBayes();
				double eocbonf = b.EOCBonf();
				double diff = Math.abs(eocbayes - eocbonf);
				if (diff > 1e-5) {
					ok = false;
					System.out
							.printf(
									"test4() ERROR: mu0=%f beta0=%f EOCBayes()=%f EOCBonf=%f diff=%g\n",
									mu0, beta0, eocbayes, eocbonf, diff);
				}
			}
		}
		System.out.printf("test4: %s\n", ok ? "OK" : "FAIL");
		return ok;
	}

}
