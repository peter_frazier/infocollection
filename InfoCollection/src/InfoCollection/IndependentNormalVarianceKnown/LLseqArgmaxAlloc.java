package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.util.MathPF;

public class LLseqArgmaxAlloc extends BayesSamplingRule {
	int debug = 0;
	int method;
	double c;
	LLck llc;

	public LLseqArgmaxAlloc(Belief b, double c) {
		super(b);
		method = 0;
		this.c = c;
	}

	public LLseqArgmaxAlloc(Belief b, double c, int method) {
		super(b);
		this.c = c;
		this.method = method;
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		llc = new LLck(belief, c, method); // note that LLck copies belief.
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		llc.RecordMeasurement(x, y);
	}

	public int GetMeasurementDecision() {
		double[] alloc;
		alloc = llc.GoodAlloc();
		if (MathPF.max(alloc) == 0) {
		  // There was no good alloc.  Do pure exploration.
		  return MathPF.argmin(belief.beta);
		}
		return MathPF.argmax(alloc);
	}

	public boolean ShouldStop() {
		return (llc.GoodBudget() > 0);
	}

	@Override
	public String toString() {
		return "LLseqArgmaxAlloc c=" + c;
	}

	public String Name() {
		return "LLseqArgmaxAlloc";
	}
}
