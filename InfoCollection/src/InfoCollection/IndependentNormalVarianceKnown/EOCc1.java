package InfoCollection.IndependentNormalVarianceKnown;

public class EOCc1 extends BayesStoppingRule {
	int debug = 0;
	int method;
	double c;
	LLc1 llc;

	public EOCc1(Belief b, double c) {
		super(b);
		this.c = c;
		method = -1; // default
	}

	public EOCc1(Belief b, double c, int method) {
		super(b);
		this.c = c;
		this.method = method;
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		// note that LLc copies our belief, so that we don't update the
		// same belief.
		if (method == -1)
			llc = new LLc1(belief, c);
		else
			llc = new LLc1(belief, c, method);
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		llc.RecordMeasurement(x, y);
	}

	public boolean ShouldStop() {
		double good_budget = llc.GoodBudget();
		return (good_budget == 0);
	}

	@Override
	public String toString() {
		return "EOC_{c,1} c=" + c;
	}

	public String Name() {
		return "EOC_{c,1}";
	}
}
