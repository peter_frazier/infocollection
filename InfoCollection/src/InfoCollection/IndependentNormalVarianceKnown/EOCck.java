package InfoCollection.IndependentNormalVarianceKnown;

import java.util.Arrays;

import InfoCollection.util.MathPF;

public class EOCck extends BayesStoppingRule {
	int debug = 0;
	int method;
	double c;
	LLck llc;
	KGEnvelopeStoppingRule emulator;

	public EOCck(Belief b, double c) {
		super(b);
		this.c = c;
		method = 0;
		if (debug > 2)
			emulator = new KGEnvelopeStoppingRule(b, Math.log(c));
	}

	public EOCck(Belief b, double c, int method) {
		super(b);
		this.c = c;
		this.method = method;
		if (debug > 2)
			emulator = new KGEnvelopeStoppingRule(b, Math.log(c));
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		// note that LLck copies our belief, so that we don't update the
		// same belief.
		llc = new LLck(belief, c, method);
		if (debug > 2)
			emulator.Start(desiredM);
	}

	@Override
	public void RecordMeasurement(int x, double y) throws Exception {
		super.RecordMeasurement(x, y);
		llc.RecordMeasurement(x, y);
		if (debug > 2)
			emulator.RecordMeasurement(x, y);
	}

	public boolean ShouldStop() {
		double good_budget = llc.GoodBudget();
		boolean shouldstop = (good_budget == 0);
		if (debug > 0) {
			try {
				double logKGEFactor1 = MathPF.max(Util
						.EnvelopeLogQfactor(belief));
				boolean kgestop1 = (logKGEFactor1 < Math.log(c));

				if (debug > 2) {
					// These values should agree with logKGEFactor1
					// and kgestop1.
					double logKGEFactor2 = emulator.GetCurrentValue();
					boolean kgestop2 = emulator.ShouldStop();

					if (kgestop2 != kgestop1) {
						System.out.println("EOCck.ShouldStop(): " + "kgestop1="
								+ kgestop1 + " kgestop2=" + kgestop2
								+ " logKGEFactor1=" + logKGEFactor1
								+ " logKGEFactor2=" + logKGEFactor2);
						System.out.println("Internal belief:");
						System.out.println(belief);
						System.out.println("Emulator belief:");
						System.out.println(emulator.belief);
						System.out.println("LLck belief:");
						System.out.println(llc.b);
					}
				}

				/*
				 * It is interesting to see where KGEnvelopeStopping and EOCck
				 * disagree, particularly for a 1 known alternative belief with
				 * 1 other alternative, since in this case the theoretical
				 * policies they are approximating agree. Thus, any differences
				 * are due either to the quality of their approximations, or
				 * bugs in the code. Both are interesting and important to
				 * observe.
				 */
				if (shouldstop != kgestop1)
					System.out
							.println("EOCck.ShouldStop()="
									+ shouldstop
									+ " KGEnvelope.ShouldStop()="
									+ kgestop1
									+ " nstar="
									+ Arrays
											.toString(Util.TangentPoint(belief))
									+ " good_alloc="
									+ Arrays.toString(llc.GoodAlloc()));

				if (shouldstop != kgestop1 && debug > 1) {
					System.out.println(belief);
					double[] good_alloc = llc.GoodAlloc();
					System.out.printf("good_alloc=%s\n", Arrays
							.toString(good_alloc));
					double loggross = llc.LogGrossAllocValue(good_alloc);
					double logavg = loggross - Math.log(good_budget);
					// double net = llc.NetAllocValue(good_alloc);
					System.out.printf("log(avg value of good alloc)=%f "
							+ "max(LogQfactor(good_alloc))=%f "
							+ "KGEFactor=max(LogQfactor(nstar))=%f\n", logavg,
							MathPF.max(Util.LogQfactor(belief, good_alloc)),
							logKGEFactor1);
				}
			} catch (Exception e) {
				System.out.println("EnvelopeLogQfactor(belief) failed");
				System.out.println(e);
				System.out.println("Continuing");
			}
		}
		return (good_budget == 0);
	}

	@Override
	public String toString() {
		return "EOC_{c,k} c=" + c;
	}

	public String Name() {
		return "EOC_{c,k}";
	}
}
