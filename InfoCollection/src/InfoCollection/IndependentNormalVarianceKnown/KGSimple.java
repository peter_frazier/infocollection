package InfoCollection.IndependentNormalVarianceKnown;

import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;

public class KGSimple extends BayesSamplingRule {

	public int GetMeasurementDecision() throws Exception {
		double logq[] = Util.LogQfactor(belief);
		// System.out.println(belief); //Debug
		// System.out.println("logq=" + Arrays.toString(logq)); //Debug
		return (MathPF.argmax(logq));
	}

	@Override
	public String toString() {
		return "KGSimple";
	}

	public double[] GetLogQ() throws Exception {
		return Util.LogQfactor(belief);
	}

	public static void main(String args[]) throws Exception {
		TestExamples.Example1(new KGSimple(), true);
	}
}
