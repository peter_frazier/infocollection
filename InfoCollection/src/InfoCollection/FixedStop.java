package InfoCollection;

/*
 * Stop after a fixed number of samples.
 */
public class FixedStop implements StoppingRule, ThresholdStoppingRule<Integer> {
	int n;
	int N;

	public FixedStop(int N) {
		n = 0;
		this.N = N;
	}

	public void Start(int M) {
		n = 0;
	}

	public void RecordMeasurement(int x, double y) {
		n++;
	}

	public Integer GetCurrentValue() {
		return n;
	}

	public boolean ShouldStop() {
		return (n >= N);
	}

	public Integer GetThreshold() {
		return N;
	}

	public void SetThreshold(Integer N) {
		this.N = N;
	}

	@Override
	public String toString() {
		return "FixedStop N=" + N;
	}

	public String Name() {
		return "FixedStop";
	}

	/*
	 * Create custom and standard stopping rule sequences for convenience.
	 */
	public static ThresholdStoppingRuleSeq<Integer> Seq(int[] N) {
		Integer[] _N = new Integer[N.length];
		for (int i = 0; i < N.length; i++)
			_N[i] = N[i];
		return new ThresholdStoppingRuleSeq<Integer>(_N, new FixedStop(N[0]));
	}

	public static ThresholdStoppingRuleSeq<Integer> Seq() {
		int N[] = { 1, 10, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275,
				300, 325, 350 };
		return Seq(N);
	}

}
