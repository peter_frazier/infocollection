package InfoCollection;

import java.util.Random;

/*
 * This class contains the true sampling distribution of a collection of
 * alternatives.
 */
public abstract class Truth {

	/* Generate the truth sampling distribution randomly from a belief. */
	// public Truth(Belief b);
	/*
	 * Take a random sample from alternative x using the passed random number
	 * generator.
	 */
	public abstract double Sample(int x, Random r);

	/* Return the value of the best of the alternatives. */
	public abstract double BestValue();

	/* Returns the loss associated with choosing x. */
	public abstract double Loss(int x);

	/*
	 * Return the value of the chosen alternative. This is the antithesis of
	 * Loss.
	 */
	public abstract double Value(int x);

	/* Returns the number of alternatives. */
	public abstract int M();

	/* Have this print out all the values. */
	@Override
	public abstract String toString();
}
