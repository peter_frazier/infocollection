package InfoCollection;

/*
 * Instead of implementing separate a SamplingRule and StoppingRule, it is
 * sometimes much more efficient numerically to implement both together in the
 * same class.  For example, if using the KG sampling rule with the KG stopping
 * rule, one needs to maintain a list of KG factors.  If the stopping and
 * sampling rules are implemented separately, one has to keep two different
 * lists incurring twice the runtime.  Instead, one can keep them together with
 * this class.
 */
public interface BothRule {
	/*
	 * Start() tells the policy to get ready to begin measuring. If it had
	 * measured previously, it gets rid of everything it learned from previous
	 * calls to RecordMeasurement(), and resets it to the state that it had
	 * before the first call to RecordMeasurement(). We pass in the number of
	 * alternatives that will be run. If the policy is not set up to run this
	 * number of alternatives then an exception is thrown. This could happen if
	 * the policy is a Bayesian one, and has a certain belief about the
	 * alternative's distributions that assumes a certain number of
	 * alternatives.
	 */
	public void Start(int desiredM) throws Exception;

	/*
	 * Determines x^n, i.e. the measurement decision at time n.
	 */
	public int GetMeasurementDecision() throws Exception;

	/*
	 * Return true if we should stop sampling.
	 */
	public boolean ShouldStop();

	/*
	 * This function receives the value of an observation (y), and which
	 * alternative the observation measured (x), and uses Bayesian updating to
	 * update the policy's knowledge of that alternative, i.e. it updates mu[x]
	 * and beta[x].
	 */
	public void RecordMeasurement(int x, double y) throws Exception;

	/*
	 * Returns the optimal implementation decision, which is an integer between
	 * 1 and M. The optimal implementation decision is the same across all
	 * measurement policies. This function should only be called at the terminal
	 * time N. If you pass a PrintStream argument in addition to the usual
	 * arguments, it will print debugging output. The default versions of the
	 * debugging output may be overridden.
	 */
	public int GetImplementationDecision();

	/*
	 * Returns a string describing the policy.
	 */
	public String toString();
}
