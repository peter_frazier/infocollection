package InfoCollection;

/*
 * This is an extra type of stopping rule that keeps a belief, and has a
 * functional that it can apply to that belief.  As soon as the functional
 * applied to the current belief crosses a threshold, it stops.
 */
public interface ThresholdStoppingRule<T> extends StoppingRule {
	public void SetThreshold(T threshold);

	public T GetThreshold();

	/*
	 * Get the value of the functional applied to the threshold.
	 */
	public T GetCurrentValue();

	/*
	 * Name should return just a string describing the type, without the
	 * parameter.
	 */
	public String Name();
	// public static int compare(T threshold1, T threshold2);
}
