package InfoCollection.util;

import java.util.Comparator;

public class ArrayIndexComparator implements Comparator<Integer> {
	double values[];

	public ArrayIndexComparator(double values[]) {
		this.values = values;
	}

	/*
	 * Compares its two arguments for order. Returns a negative integer, zero,
	 * or positive integer as the first argument is less than, equal to, or
	 * greater than the second.
	 */
	public int compare(Integer i1, Integer i2) {
		double cmp = values[i1] - values[i2];
		if (cmp > 0)
			return 1;
		else if (cmp < 0)
			return -1;
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (!o.getClass().equals(ArrayIndexComparator.class))
			return false;
		ArrayIndexComparator a = (ArrayIndexComparator) o;
		return a.values == values;
	}
}
