package InfoCollection.util;

import java.util.Arrays;

import umontreal.iro.lecuyer.probdist.StudentDist;
import umontreal.iro.lecuyer.util.Num;

/*
 * A collection of static functions for calculating various quantities related
 * to the Student-t distribution.  They are all for the standard Student-t
 * distribution, which is parameterized by the degrees of freedom, which we
 * call nu or "dof".  The standard Student-t has mean 0 and variance nu/(nu-2).
 * The (non-standardized) Student-t has 2 more parameters, which are usually
 * given as the mean, and a scale parameter sometimes corresponding to a sample
 * variance.
 */
public class Student {

	/*
	 * Logarithm of the pdf of the standard Student-t at s with degrees of
	 * freedom dof. We have an analytic expression for the density of which we
	 * take the logarithm analytically, so we compute the logpdf completely in
	 * log-space to make it more accurate.
	 */
	public static double LogPdf(double dof, double s) {
		double ans = Num.lnGamma((dof + 1) / 2) - Num.lnGamma(dof / 2);
		ans -= 0.5 * StrictMath.log(dof * StrictMath.PI);
		ans -= ((dof + 1) / 2) * StrictMath.log(1 + s * s / dof);
		return ans;
	}

	public static double Pdf(double dof, double s) {
		// This could be implemented more efficiently.
		return StrictMath.exp(LogPdf(dof, s));
	}

	/*
	 * Logarithm of the cdf of the standard Student-t at s with degrees of
	 * freedom dof. Throws an exception if it is called in a regime where it is
	 * numerically unstable. Be careful with this function -- it isn't always
	 * clear when it is numerically unstable, and sometimes it doesn't throw an
	 * exception even though it should.
	 */
	public static double LogCdf(double dof, double s) throws Exception {
		/*
		 * We case based on s being positive or negative. When s is large
		 * negative, the cdf is close to 0, so it is fine to compute the cdf
		 * directly, and then take the log. But when s is large positive, the
		 * cdf is close to 1, and likely gets rounded to 1 very quickly. It is
		 * better to compute 1-cdf, which is close to 0 and doesn't get rounded
		 * as easily, and then compute log(1-(1-cdf)) using log1p.
		 * 
		 * We have several options for computing the cdf. I tried the one from
		 * the umontreal.lecuyer library, but it only handles integral degrees
		 * of freedom. Colt has a version, but you have to instantiate it with a
		 * random number generator, which is annoying. We have another option,
		 * which is to use the bounds in Chick,Branke,Schmidt together with
		 * interval arithmetic.
		 */
		double z = -StrictMath.abs(s);
		StudentDist dist = new StudentDist((int) dof);
		double u = dist.cdf(z);
		if (s <= 0) // s=z
			return StrictMath.log(u);
		else
			// s=-z
			return StrictMath.log1p(-u);
	}

	/*
	 * Chick,Branke,Schmidt 2007 provide upper and lower bounds on the cdf of
	 * the student-t distribution. We provide functions for computing these
	 * bounds directly, and the also imply upper and lower bounds on the Mills
	 * ratio, as well as an upper bound on Psi. The lower bound they provide on
	 * Psi is 0, so we don't provide an explicit function for computing this
	 * lower bound 0.
	 */
	// public static double LogCdfUpperBoundChBrSc07(double dof, double z) { }
	// public static double LogCdfLowerBoundChBrSc07(double dof, double z) { }

	/*
	 * The Mills ratio for the student-t, which, for z>0, is
	 * (1-cdf(dof,t))/pdf(dof,t).
	 */

	/*
	 * First implementation of the logarithm of the Mill's ratio. It is simple,
	 * but it requires subtracting two numbers of approximately equal magnitude,
	 * so it may be numerically unstable. Test it with test1() below.
	 */
	public static double LogMillsRatio1(double dof, double s) throws Exception {
		assert s >= 0;
		return LogCdf(dof, -s) - LogPdf(dof, s);
	}

	/*
	 * Second implementation of the log of the Mill's ratio R_\nu. It relies on
	 * R_\nu(s) = sqrt(nu)/2 * z^-(a+b) * B(z;a,b), where z=nu/(nu+s^2), a=nu/2,
	 * b=1/2, and B(z;a,b) is the incomplete beta function, \int_0^z u^a (1-u)^b
	 * du. This can be computed from the expansion for the incomplete beta
	 * function, and if z is close to 0 then the expansion converges quickly. If
	 * z is close to 1, it doesn't converge very well.
	 */
	public static double LogMillsRatio2(double dof, double s) throws Exception {
		return LogMillsRatio2(dof, s, 10); // 10 terms by default.
	}

	public static double LogMillsRatio2(double dof, double s, int nterms)
			throws Exception {
		assert s >= 0;
		assert dof >= 1;
		double z = dof / (dof + (s * s));
		double term, sumterms;

		/* Sum up the expansion. */
		sumterms = 0;
		for (int n = 0; n < nterms; n++) {
			double prod = 1;
			for (int i = 1; i < i; i++)
				prod *= 1 - 1 / (2 * i);
			term = StrictMath.pow(z, n) * prod / (dof + 2 * n);
			sumterms += term;
		}
		return StrictMath.log(sumterms * StrictMath.sqrt(dof / z));
	}

	/*
	 * Note that the upper bound is infinite for dof=1. The upper bound is
	 * derived from the lower bound Psi>=0.
	 */
	public static double LogMillsUpperBoundChBrSc07(double dof, double z) {
		assert dof >= 1;
		assert z >= 0;
		double bound = (z + (dof / z)) / (dof - 1);
		return StrictMath.log(bound);
	}

	public static double LogMillsLowerBoundChBrSc07(double dof, double z) {
		assert dof >= 1;
		assert z >= 0;
		return Double.NEGATIVE_INFINITY; // PF: write this function.
	}

	/*
	 * The function Psi is defined in equation 1 of Chick,Branke,Schmidt 2007.
	 * It is the integral \int_{u=s}^\infty (u-s) \pdf(\dof,u) du, which is also
	 * equal to \pdf(\dof,s)*(\dof + s^2)/(\dof-1) - s*\cdf(\dof,-s). It should
	 * be computed with s positive.
	 * 
	 * LogPsi1() computes it directly from the cdf and pdf. This seems to work
	 * well for a large range of values of s, but badly when the # of degrees of
	 * freedom is large.
	 * 
	 * For this reason, LogPsi, which is the public interface for computing the
	 * logarithm of Psi, uses LogPsi1 only for small values of dof, and
	 * otherwise approximates it with the Psi for the standard normal
	 * distribution.
	 */
	public static double Psi(double dof, double s) throws Exception {
		return StrictMath.exp(LogPsi(dof, s));
	}

	public static double LogPsi(double dof, double s) throws Exception {
		/*
		 * Originally I had this code use the normal approximation if the number
		 * of degrees of freedom was larger than 20. But after fixing a bug
		 * elsewhere, I found that LogPsi1 worked fine for dof up to 100, and z
		 * up to 500, so I am just using it. PF: update on 12/04/2008. I was
		 * working on plots for my talk for winter simulation conference, and
		 * had the next 3 lines commented out, and saw errors when doing
		 * independent normal variance unknown ranking and selection using the
		 * KG stopping rule using the sampling rule from Chick & Inoue 2001. To
		 * be specific I was running the C4 test in tower/Sim20080214/. These
		 * errors were an internal error in LogPsi1 when the dof was about 300.
		 * So I am using the normal approximation when the number of degrees of
		 * freedom is over 100.
		 */
		if (dof > 100)
			return Normal.LogPsi(s);
		else
			return LogPsi1(dof, s);
	}

	private static double LogPsi1(double dof, double s) throws Exception {
		if (s < 0 || dof < 1)
			throw new Exception(String.format(
					"in call to LogPsi(dof=%f s=%f), "
							+ "LogPsi requires s>=0 and dof>=1", dof, s));
		double logterm1 = LogPdf(dof, s);
		logterm1 += StrictMath.log(dof + s * s) - StrictMath.log(dof - 1);
		double logterm2 = StrictMath.log(s) + LogCdf(dof, -s);
		if (logterm1 < logterm2) {
			throw new Exception(String.format("Internal error: "
					+ "logterm1 < logterm2.  "
					+ "s=%f dof=%f logterm1=%f logterm2=%s", s, dof, logterm1,
					logterm2));
		}
		double ans = MathPF.log_minus(logterm1, logterm2);
		if (Double.isNaN(ans))
			throw new Exception(String.format(
					"Internal error: " + "answer is NaN. "
							+ "s=%f dof=%f logterm1=%f logterm2=%s", s, dof,
					logterm1, logterm2));
		if (ans > LogPsiUpperBoundChBrSc07(dof, s))
			throw new Exception("Violated ChBrSc07 upper bound");
		return ans;
	}

	public static double LogPsiUpperBoundChBrSc07(double dof, double z) {
		// PF: fill this in.
		return Double.POSITIVE_INFINITY;
	}

	// LogPsiLowerBoundChBrSc07 doesn't exist. It would just be -Infinity.

	public static void main(String args[]) {
		// test1();
		test2(true);
		// test3(true,20);
		// test4(true,1);
	}

	/*
	 * Compute the Mill's ratio as z->-infinity. This should be smooth until
	 * numerical instabilities bite us. The test just checks that it doesn't
	 * fail, and checks the return answers to make sure that they do not violate
	 * the upper bound in ChBrSc07. Another additional test, which is not
	 * automatic, is to save the output and then graph it.
	 */
	public static boolean test1() {
		return test1(false);
	}

	public static boolean test1(boolean verbose) {
		double z = 0;
		double maxz = -500;
		double dof[] = { 1, 2, 3, 5, 10, 20, 30, 40, 50, 100, 1000 };
		double logratio[] = new double[dof.length];
		System.out.printf("test1: maxz=%f degrees of freedom=%s\n", maxz,
				Arrays.toString(dof));
		try {
			for (z = 0; z > maxz; z -= .5) {
				for (int i = 0; i < dof.length; i++) {
					logratio[i] = LogMillsRatio2(dof[i], -z);
					double logbound = LogMillsUpperBoundChBrSc07(dof[i], -z);
					if (logratio[i] > logbound) {
						System.out.printf("test1: FAIL"
								+ " (violated upper bound on "
								+ "dof=%f z=%f log(bound)=%f\n", dof[i], z,
								logbound);
						return false; // abort
					}
				}
				if (verbose)
					System.out.printf("test1: z=%.1f logratios=%s\n", z, Arrays
							.toString(logratio));
			}
		} catch (Exception e) {
			System.out.println("test1: FAIL (failed on z=" + z + ")");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * Compute the log-Psi for various degrees of freedom and various values of
	 * z. Fails if LogPsi throws an exception. There is error-checking in
	 * LogPsi.
	 */
	public static boolean test2() {
		return test2(false);
	}

	public static boolean test2(boolean verbose) {
		double z = 0;
		double maxz = 1000;
		double dof[] = { 1, 2, 3, 5, 10, 100, 1000 };
		double logeoc[] = new double[dof.length];
		System.out.printf("test2: maxz=%f degrees of freedom=%s\n", maxz,
				Arrays.toString(dof));
		try {
			for (z = 0; z < maxz; z += .5) {
				for (int i = 0; i < dof.length; i++)
					logeoc[i] = LogPsi1(dof[i], z);
				if (verbose)
					System.out.printf("test2: z=%.1f logeocs=%s\n", z, Arrays
							.toString(logeoc));
			}
		} catch (Exception e) {
			System.out.println("test2: FAIL (failed on z=" + z + ")");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * Compute the log-Psi as a function of dof. Plot both the Student-t only
	 * value, and the normal approximation.
	 */
	public static boolean test3() {
		return test3(false, 0);
	}

	public static boolean test3(boolean verbose, double z) {
		double dof = 0, max_dof = 1000;
		if (verbose) {
			System.out
					.printf(
							"test3: z=%f columns are dof, student logpsi, normal logpsi\n",
							z);
		}
		try {
			for (dof = 3; dof < max_dof; dof += 1) {
				double logpsi = LogPsi1(dof, z);
				if (verbose)
					System.out.printf("test3: %.0f %f %f\n", dof, logpsi,
							Normal.LogPsi(z));
			}
		} catch (Exception e) {
			System.out.println("test3: FAIL (failed on dof=" + dof + ")");
			e.printStackTrace();
			return false;
		}
		System.out.println("test3: OK");
		return true;
	}

	/*
	 * Compute log-Psi, log-cdf, log-pdf for a fixed z as a function of dof for
	 * both normal and student-t. Of course the normal one will not change as a
	 * function of dof, and the student-t should limit at the normal one as dof
	 * goes to infinity.
	 */
	public static boolean test4() {
		return test4(false, 0);
	}

	public static boolean test4(boolean verbose, double z) {
		double dof = 0, max_dof = 100;
		try {
			for (dof = 3; dof < max_dof; dof += 1) {
				double logpsi = LogPsi1(dof, z); // do not use normal
													// approximation
				double logpdf = LogPdf(dof, z);
				double logcdf = LogCdf(dof, z);
				if (verbose)
					System.out.printf("test4: %.0f %f %f %f\n", dof, logpdf,
							logcdf, logpsi);
			}
			if (verbose) {
				System.out
						.printf(
								"test4: z=%f columns are dof, log-pdf, log-cdf, log-psi\n",
								z);
				System.out.printf("test4: normal log-pdf=%f\n", Normal
						.LogPdf(z));
				System.out.printf("test4: normal log-cdf=%f\n", Normal
						.LogCdf(z));
				System.out.printf("test4: normal log-psi=%f\n", Normal
						.LogPsi(z));
			}
		} catch (Exception e) {
			System.out.println("test4: FAIL (failed on dof=" + dof + ")");
			e.printStackTrace();
			return false;
		}
		System.out.println("test4: OK");
		return true;
	}

}
