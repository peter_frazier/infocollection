package InfoCollection.util;

import java.util.Arrays;

import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.probdist.NormalDistQuick;
import flanagan.integration.IntegralFunction;
import flanagan.integration.Integration;

/*
 * A collection of static functions for calculating some quantities related
 * to the standard normal distribution.  Although there are lots of third party
 * packages for the normal distribution, the quantities computed here are not
 * commonly included.
 */
public class Normal {
	/* This constant is the logarithm of the square root of 2*PI. */
	static double logsqrt2pi = 0.5 * StrictMath.log(2 * StrictMath.PI);

	/*
	 * Logarithm of the pdf of the standard normal.
	 */
	public static double LogPdf(double s) {
		return -logsqrt2pi - (s * s / 2);
	}

	/*
	 * Logarithm of the cdf of the standard normal. This is not a very stable
	 * implementation, and we only have it here for testing purposes -- so that
	 * we can compare our more sophisticated techniques with this easy
	 * implementation and make sure they agree in certain easy-to-compute
	 * ranges.
	 */
	public static double LogCdf(double s) throws Exception {
		double cdf = NormalDist.cdf01(s);
		return StrictMath.log(cdf);
	}

	/*
	 * These are routines for computing Mill's ratio, which is cdf(-s)/pdf(s). s
	 * should be positive.
	 * 
	 * There are two distinct routines for computing its logarithm.
	 * LogMillsRatio1() computes it in a naive way that works well only when s
	 * is small. We keep it private. LogMillsRatio2() uses an asymptotic
	 * approximation when s is big, and thus is more stable. We keep it private.
	 * LogMillsRatio just calls LogMillsRatio2(), and is the public interface
	 * for computing Mill's ratio.
	 * 
	 * We also provide upper and lower bounds. These come from R.D. Gordon's
	 * 1941 paper, "Values of Mill's ratio of area of bounding ordinate of the
	 * normal probability integral for large values of the argument." He gets
	 * upper bound 1/x and lower bound x/(x^2+1). Also, a tighter lower bound is
	 * derived in Z.W. Birnbaum's paper, "An Inequality for Mill's Ratio." We
	 * don't currently have this tighter lower bound in the code.
	 * 
	 * PF: previously we used logf = -log(z^2+2). Does this agree with the
	 * bounds we use? I believe it comes close.
	 */
	public static double LogMillsRatio(double s) throws Exception {
		return LogMillsRatio2(s);
	}

	private static double LogMillsRatio1(double s) throws Exception {
		assert s >= 0;
		return LogCdf(-s) - LogPdf(s);
	}

	private static double LogMillsRatio2(double s) throws Exception {
		assert s >= 0;
		if (s > 10)
			/*
			 * When s is large, the upper and lower bounds are essentially
			 * equal, and are more accurate than computing it from LogCdf.
			 */
			return LogMillsRatioLowerBound(s);
		else if (s >= 0)
			return LogMillsRatio1(s);
		else
			// s < 0
			throw new Exception("Mill's ratio requires s>=0");
	}

	public static double LogMillsRatioUpperBound(double s) {
		return -StrictMath.log(s);
	}

	public static double LogMillsRatioLowerBound(double s) {
		return StrictMath.log(s) - StrictMath.log(s * s + 1);
	}

	/*
	 * The function Psi is defined in equation 1 of Chick,Branke,Schmidt 2007
	 * for student-t distribution, and here we compute the analogue for the
	 * normal distribution. Psi is the integral \int_{u=s}^\infty (u-s) \pdf(u)
	 * du, which is also equal to \pdf(s) - s*\cdf(-s). and to \pdf(s) *
	 * (1-s*R(s)), where R(s)=\cdf(-s)/\pdf(s) is Mill's ratio It should be
	 * computed with s positive. This is an important function because it is
	 * related to the expectation of the maximum of normal random variables, and
	 * appears in independent and correlated knowledge gradient papers, as well
	 * as in the HeChCh07 OCBA paper. Computing the function sounds simple, but
	 * when s is very far away from 0 we have numerical troubles due to
	 * subtracting small numbers. Also, the accuracy ofthe normcdf and normpdf
	 * routines is very important.
	 * 
	 * We have several functions for computing Psi, but only one of them is
	 * public. The rest are either for testing, are experimental routines, or
	 * are legacy code.
	 * 
	 * LogPsi1() is the first implementation and uses Mill's ratio to compute
	 * it. It is more experimental.
	 * 
	 * LogPsi2() is the second implementation and computes it without using
	 * Mill's ratio, and is the one I used for the independent normal variance
	 * known KG paper.
	 * 
	 * Psi1() computes the function directly rather than in log-space. It is
	 * accurate up to s=38, which has Psi(s)=7.6E-318. Above that it gives 0.
	 * 
	 * Psi2() also computes the function directly rather than in log-space, but
	 * it uses the asymptotic approximation when s is large.
	 * 
	 * PsiQuick() is a version of Psi() that uses the fast routines for
	 * evaluating the normal distribution. It is even less accurate.
	 */
	public static double LogPsi(double s) {
		return LogPsi2(s);
	}

	@SuppressWarnings("unused")
	private static double LogPsi1(double s) throws Exception {
		double ans, term;
		if (s < 0)
			throw new Exception(String.format("in call to LogPsi(s=%f), "
					+ "LogPsi requires s>=0", s));
		term = StrictMath.log(s) + LogMillsRatio(s);
		ans = LogPdf(s) + StrictMath.log1p(-StrictMath.exp(term));
		if (Double.isNaN(ans))
			throw new Exception(String.format("Internal error: "
					+ "answer is NaN. s=%f", s));
		return ans;
	}

	private static double LogPsi2(double s) {
		/*
		 * term is log(1 - s*normcdf(-s)/normpdf(s)) = log(1-s*R(s)), where R(s)
		 * is the Mill's ratio. As s --> infinity, it is well approximated as
		 * described on notes 12/21/2007 page 4.
		 */
		double term = 0;
		if (s < 0) {
			System.err
					.printf(
							"pfrazier.util.Normal.LogPsi2: LogPsi requires s>=0 but s=%f.  Calculating LogPsi(abs(s))\n",
							s);
			s = -s;
		}
		if (s < 10) {
			// For s in this range, just use the cdf and pdf.
			double cdf = NormalDist.cdf01(-s);
			double pdf = NormalDist.density(0, 1, s);
			term = Math.log(1 - s * cdf / pdf); // why not use log1p?
			// term = Math.log1p(-s*cdf/pdf);
		} else
			// s > 10
			// term = -Math.log(s*s+2); // why is this 2 instead of 1?
			term = -Math.log(s * s + 1);
		return LogPdf(s) + term;
	}

	public static double Psi(double s) {
		return Psi2(s);
	}

	@SuppressWarnings("unused")
	private static double Psi1(double s) {
		double ans;
		ans = -s * NormalDist.cdf01(-s) + NormalDist.density(0, 1, s);
		/*
		 * If s is very large, numerical errors cause this computation to be
		 * negative. If asserts are turned on, we check for it. Otherwise, we
		 * have a hack to fix it.
		 */
		assert ans >= 0 : "s=" + s + " phi(s)=" + NormalDist.density(0, 1, -s)
				+ " Phi(-s)=" + NormalDist.cdf01(-s) + " Psi(s)=" + ans;
		if (ans < 0)
			ans = 0;
		return ans;
	}

	private static double Psi2(double s) {
		double ans;
		if (s < 10)
			ans = -s * NormalDist.cdf01(-s) + NormalDist.density(0, 1, s);
		else
			// ans = 1.0 / (s*s+2); // why is this 2 instead of 1?
			ans = 1.0 / (s * s + 1); // why is this 2 instead of 1?
		return ans;
	}

	public static double PsiQuick(double s) {
		double ans = -s * NormalDistQuick.cdf01(-s)
				+ NormalDist.density(0, 1, s);
		if (ans < 0)
			ans = 0;
		return ans;
	}

	/*
	 * Abramowitz and Stegun page 932 equation 26.2.17 has an approximation to
	 * the normal cdf at z that uses a 5th order polynomial in 1/(1+pz), where p
	 * is some constant. This function evaluates that polynomial. This function
	 * can then be used to approximate the normal cdf as cdf(z) = 1 - pdf(z) *
	 * AbStNormCdfRational5(|z|) for z>=0 cdf(z) = pdf(z) *
	 * AbStNormCdfRational5(|z|) for z<=0 This approximation to cdf(z) will be
	 * within 7.5*10^-8 of the exact cdf. If z<0 is passed to this function, it
	 * converts it to |z|.
	 */
	public static double AbStNormCdfRational5(double z) {
		double p = .2316419;
		double t = 1 / (1 + p * StrictMath.abs(z));
		double[] b = new double[] { 0, .319381530, -.356563782, 1.781477937,
				-1.821255978, 1.330274429 };
		double ans = 0;
		@SuppressWarnings("unused")
		double ti;

		ti = t; // this will be t^i.
		for (int i = 1; i <= 5; i++) {
			ans += b[i] * StrictMath.pow(t, i);
			// ans += b[i] * ti;
			// ti *= t;
		}
		return ans;
	}

	/*
	 * ExpMax computes the expectation of the maximum of a collection of
	 * independent normal random variables. To be specific, ExpMax(mu,sigma)
	 * returns E[max_x \mu_x + \sigma_x Z_x], where Z_x are independent standard
	 * normal random variables.
	 * 
	 * PF: If there are only two alternatives, then this integration should be
	 * performed analytically. PF: This doesn't handle the case when one of the
	 * sigmas is 0. PF: The integration's upper bound is just a guess. PF: How
	 * do we know that the number of points chosen is enough?
	 */
	public static double ExpMax(double[] mu, double[] sigma) {
		/*
		 * To get better numerical precision, we normalize the mu and sigma. To
		 * do this, we subtract mu^* = max_x mu_x, and divide by sigma^* = max_x
		 * sigma_x. We then use the following fact shown in my notes on
		 * 12/27/2007 p1: Exp[max_x mu_x + sigma_x Z_x] = mu^* + sigma^*
		 * Exp[max_x (mu_x - mu^*)/sigma^* + (sigma_x/sigma^*) Z_x]
		 */
		double maxmu = MathPF.max(mu);
		double maxsigma = MathPF.max(sigma);
		if (maxsigma == 0)
			return maxmu;
		if (maxsigma == Double.POSITIVE_INFINITY)
			return Double.POSITIVE_INFINITY;
		/*
		 * We copy these arrays rather than using them directly to avoid
		 * altering the caller's versions.
		 */
		double[] norm_mu = new double[mu.length];
		double[] norm_sigma = new double[sigma.length];
		for (int i = 0; i < mu.length; i++) {
			norm_mu[i] = (mu[i] - maxmu) / maxsigma;
			assert maxsigma > 0;
			norm_sigma[i] = sigma[i] / maxsigma;
		}

		ExpMaxHelper integrand = new ExpMaxHelper(norm_mu, norm_sigma);
		double lowerLimit = 0;
		double upperLimit = 20; // something big?
		int nPoints = 64;
		double expmax, ans;
		expmax = Integration.gaussQuad(integrand, lowerLimit, upperLimit,
				nPoints);
		ans = maxsigma * expmax;
		/*
		 * We have some numerical precision problems. ans should be >= 0, but
		 * with numerical error it may be negative. We check that it is larger
		 * than -1E-10.
		 */
		assert ans >= -1E-10 : "norm_mu=" + Arrays.toString(norm_mu)
				+ " norm_sigma=" + Arrays.toString(norm_sigma) + " ans=" + ans;
		return ans + maxmu;
	}

	static private class ExpMaxHelper implements IntegralFunction {
		private double[] mu;
		private double[] sigma;

		public ExpMaxHelper(double[] mu, double[] sigma) {
			this.mu = mu;
			this.sigma = sigma;
			assert mu.length == sigma.length;
		}

		/*
		 * PF: Is this implementation accurate enough? Would it be better to
		 * stay in log-space?
		 */
		public double function(double x) {
			int i;
			double product1, product2;
			product1 = 1;
			product2 = 1;
			for (i = 0; i < mu.length; i++) {
				// PF: check that this works for sigma[i] = 0.
				// product1 is the probability that a
				// Normal(mu[i],sigma^2[i]) is less than x.
				// product2 is the probability that a
				// Normal(mu[i],sigma^2[i]) is greater than x.
				product1 *= NormalDist.cdf01((x - mu[i]) / sigma[i]);
				product2 *= NormalDist.cdf01((-x - mu[i]) / sigma[i]);
			}
			double ans = 1 - product1 - product2;
			/*
			 * We have some numerical precision problems. ans should be >= 0,
			 * but with numerical error it may be negative. We check that it is
			 * larger than -1E-10.
			 */
			assert ans >= -1E-10 : "ans=" + ans + " x=" + x + " product1="
					+ product1 + " product2=" + product2 + " mu="
					+ Arrays.toString(mu) + " sigma=" + Arrays.toString(sigma);
			return ans;
		}
	}

	public static void main(String args[]) {
		// test1(true);
		// test2(true);
		test3(true);
		// test4(true);
		test5();
		test6();
	}

	/*
	 * Compute the Mill's ratio as a function of s. This should be smooth until
	 * numerical instabilities bite us. The test just checks that it doesn't
	 * fail, and checks the return answers to make sure that they do not violate
	 * the upper and lower bounds. Another additional test, which is not
	 * automatic, is to save the output and then graph it.
	 */
	public static boolean test1() {
		return test1(false);
	}

	public static boolean test1(boolean verbose) {
		double s = 0;
		double maxs = 500;
		double logratio;
		System.out.printf("test1: maxs=%f\n", maxs);
		try {
			for (s = 0; s < maxs; s += .5) {
				logratio = LogMillsRatio(s);
				double logbound = LogMillsRatioUpperBound(s);
				if (logratio > logbound) {
					System.out.printf("test1: FAIL"
							+ " (violated upper bound on "
							+ "s=%f log(bound)=%f\n", s, logbound);
					return false; // abort
				}
				if (verbose)
					System.out.printf("test1: s=%.1f logratio=%f\n", s,
							logratio);
			}
		} catch (Exception e) {
			System.out.println("test1: FAIL (failed on s=" + s + ")");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * Compute the log-Psi for various values of s.
	 */
	public static boolean test2() {
		return test2(false);
	}

	public static boolean test2(boolean verbose) {
		double s = 0;
		double maxs = 500;
		double logeoc;
		System.out.printf("test2: maxs=%f\n", maxs);
		try {
			for (s = 0; s < maxs; s += .5) {
				logeoc = LogPsi(s);
				if (verbose)
					System.out.printf("test2: s=%.1f logeoc=%f\n", s, logeoc);
			}
		} catch (Exception e) {
			System.out.println("test2: FAIL (failed on s=" + s + ")");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/*
	 * This test computes Psi and log(Psi) for values of s and prints them out
	 * (if verbose is on). It just checks to see that no exceptions are
	 * returned. Another test, which is not automatic, is to graph them and to
	 * see that 1. log(Psi(s)) agrees with LogPsi(s) for s small 2. LogPsi(s) is
	 * smooth over the whole range of s. log(Psi(s)) can stop working at some
	 * point, because we know it is not stable for large values of s.
	 */
	public static boolean test3(boolean verbose) {
		double s, psi, logpsi;
		double maxs = 100;

		for (s = 0; s < maxs; s += .5) {
			try {
				psi = Psi(s);
				logpsi = LogPsi(s);
			} catch (Exception e) {
				System.out.println("test3: FAIL (failed on s=" + s + ")");
				e.printStackTrace();
				return false;
			}
			if (verbose) {
				System.out.printf(
						"test3: s=%f Psi(s)=%f log(Psi(s))=%f LogPsi(s)=%f\n",
						s, psi, StrictMath.log(psi), logpsi);
			}
		}
		return true;
	}

	/*
	 * The AbStNormCdfRational5() implies an approximation to normcdf(z), which
	 * we test here. If verbose is on, the test function prints stuff out to
	 * look at, but if verbose is off it computes some quantities and returns an
	 * error if an exception is thrown, but otherwise doesn't check agreement.
	 */
	public static boolean test4(boolean verbose) {
		double z = 0;
		@SuppressWarnings("unused")
		double abst, approx, logapprox, ratio, logcdf;
		for (int i = 0; i < 100; i++) {
			try {
				abst = AbStNormCdfRational5(z);
				approx = NormalDist.density(0, 1, z) * abst;
				logapprox = LogPdf(z) + StrictMath.log(abst);
				logcdf = LogCdf(z);
				ratio = NormalDist.cdf01(z) / NormalDist.density(0, 1, z);
			} catch (Exception e) {
				System.out.println("test4: FAIL (failed on z=" + z + ")");
				e.printStackTrace();
				return false;
			}
			if (verbose) {
				System.out.printf("test4:z=%f cdf(z)=%f "
						+ "Abramowitz&Stegun approx=%f\n", z, NormalDist
						.cdf01(z), approx);
				System.out.printf(
						"test4:z=%f LogCdf(z)=%f log(Ab&St approx)=%f\n", z,
						logcdf, StrictMath.log(NormalDist.cdf01(z)));
				System.out.printf("test4: z=%f AbStNormCdfRational5(z)=%f "
						+ " normcdf(z)/normpdf(z)=%f\n", z, abst, ratio);
			}
			z = z - .5;
		}
		return true;
	}

	public static boolean test5() {
		double sigmatilde1 = .0488;
		double sigmatilde2 = .0465;
		double diff = -0.5;
		try {
			double lognumerator = StrictMath.log(sigmatilde2)
					+ LogPsi(-diff / sigmatilde2);
			double logdenominator = StrictMath.log(sigmatilde1)
					+ LogPsi(-diff / sigmatilde1);
			double logvalue = lognumerator - logdenominator - logsqrt2pi;
			double value = StrictMath.exp(logvalue);
			System.out.printf(
					"lognumerator=%f logdenominator=%f logvalue=%f value=%f\n",
					lognumerator, logdenominator, logvalue, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	// This test just prints out some results from ExpMaxHelper, and is
	// judged to succeed as long as it doesn't crash.
	public static boolean test6() {
		double[] mu = { 4, 4, 4 };
		double[] sigma = { 1, 1, 1 };
		double x;
		ExpMaxHelper h = new ExpMaxHelper(mu, sigma);
		for (x = 0; x < 10; x += .5) {
			System.out.println("test6: x=" + x + " helper(x)=" + h.function(x));
		}
		return true;
	}

}
