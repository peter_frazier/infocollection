package InfoCollection.util;

import java.util.Arrays;
import java.util.Random;

/*
 * This contains generic math functions that aren't in java.lang.StrictMath.  I call
 * the class pfrazier.util.MathPF rather than simply pfrazier.util.rictMath so that
 * users can still import both this class and java.lang.Math without getting
 * conflicts.  (PF are my initials).
 */
public class MathPF {
	// OBSOLETE, use InfoCollection.Normal.
	public static double log2pi = StrictMath.log(2 * StrictMath.PI);

	/*
	 * Computes log(\sum_i a[i]) when given log(a[i]) in a numerically stable
	 * way. log(\sum a[i]) = log(amax * \sum_i a[i]/amax) = log amax +
	 * log(\sum_i exp(log(a[i])-log(amax)). This allows us to stay in log space.
	 */
	public static double log_sum(double[] loga) {
		double maxloga = max(loga);
		double sum = 0;
		for (int i = 0; i < loga.length; i++) {
			if (loga[i] != Double.NEGATIVE_INFINITY)
				sum += StrictMath.exp(loga[i] - maxloga);
		}
		if (sum == 0)
			return Double.NEGATIVE_INFINITY;
		else
			return maxloga + StrictMath.log(sum);
	}

	/*
	 * Computes log(a-b) when given log(a) and log(b) in a numerically stable
	 * way. Assumes that a > b. Uses the identity log(a-b) = log(a(1-b/a)) = log
	 * a + log(1-b/a) = log a + log(1 - exp(log b - log a). This allows us to
	 * stay in log space. Requires loga > logb, otherwise NaN is returned.
	 */
	public static double log_minus(double loga, double logb) throws Exception {
		if (loga <= logb)
			throw new Exception(String.format(
					"log_minus requires loga > logb.  loga=%f logb=%f", loga,
					logb));
		double term = StrictMath.exp(logb - loga);
		return loga + StrictMath.log1p(-term);
	}

	/*
	 * OBSOLETE, use InfoCollection.Normal. Computes the logarithm of the normal
	 * pdf, which is log(sqrt(2Pi)) - z^2/2.
	 */
	public static double log_normal_pdf(double z) {
		return -(log2pi + z * z) / 2;
	}

	/*
	 * Find the indices of the largest (called "first") and second largest
	 * entries in the array. Returns these indices in an int-array, where the
	 * 0-index value is first, and the 1-index value is second. We make no
	 * guarantees on the function's tie-breaking behavior.
	 */
	public static int[] FirstSecond(double a[]) {
		int first, second;
		int ret[] = new int[2];
		if (a[0] >= a[1]) {
			first = 0;
			second = 1;
		} else {
			first = 1;
			second = 0;
		}
		for (int i = 2; i < a.length; i++) {
			if (a[i] > a[first]) {
				second = first;
				first = i;
			} else if (a[i] > a[second])
				second = i;
		}
		ret[0] = first;
		ret[1] = second;
		return ret;
	}

	/*
	 * Returns the maximum value in an unsorted array of doubles. Assumes that
	 * the array has at least one entry. None can be NaN.
	 */
	public static double max(double a[]) {
		double max = a[0];
		assert !isNaN(a) : Arrays.toString(a);
		for (int i = 1; i < a.length; i++) {
			if (a[i] > max)
				max = a[i];
		}
		return max;
	}
	public static double max(double a, double b) {
	  if (a > b)
	    return a;
	  return b;
	}

	/*
	 * Returns the index of the maximum value in an unsorted array of doubles.
	 * Assumes that the array has at least one entry. If the maximum is not
	 * unique, it returns the smallest attaining index. Does not allow NaN.
	 */
	public static int argmax(double a[]) {
		assert !isNaN(a) : Arrays.toString(a);
		int argmax = 0;
		for (int i = 1; i < a.length; i++) {
			if (a[i] > a[argmax])
				argmax = i;
		}
		return argmax;
	}

	/*
	 * Like argmax, but breaks ties by choosing uniformly among the argmax set.
	 * Allows NaN inputs. If all the inputs are NaN, one will be chosen
	 * uniformly at random. Otherwise, any NaN inputs will be skipped.
	 */
	public static int argmax(double a[], Random rnd) {
		int set[] = argmaxset(a);
		if (set.length == 1)
			return set[0];
		return ChooseFromSet(set, rnd);
	}

	/*
	 * Returns the index with the largest value in a[], like the argmax()
	 * function, but it breaks ties by looking at the b[] vector. If the argmax
	 * still isn't unique after breaking ties, it returns the smallest index.
	 */
	public static int argmax(double a[], double b[]) {
		assert !isNaN(a) : Arrays.toString(a);
		int argmax = 0;
		for (int i = 1; i < a.length; i++) {
			if (a[i] > a[argmax])
				argmax = i;
			else if (a[i] == a[argmax] && b[i] > b[argmax])
				argmax = i;
		}
		return argmax;
	}

	/*
	 * Like argmax(double[], double[]), but breaks ties across three vectors
	 * instead of two. Also, it is special, because it reverses the sign of the
	 * b[] vector.
	 */
	public static int argmaxSpecial(double a[], double b[], double c[]) {
		assert !isNaN(a) : Arrays.toString(a);
		int argmax = 0;
		for (int i = 1; i < a.length; i++) {
			if (a[i] > a[argmax])
				argmax = i;
			else if (a[i] == a[argmax] && b[i] < b[argmax])
				argmax = i;
			else if (a[i] == a[argmax] && b[i] == b[argmax] && c[i] > c[argmax])
				argmax = i;
		}
		return argmax;
	}

	/*
	 * Like argmax, but returns the set of all values achieving the maximum,
	 * rather than breaking ties by choosing the one with the smallest index.
	 * Any input that is NaN will be treated as if it were -infinity. That is,
	 * it will not be chosen unless every other value is also NaN, in which case
	 * the decision will be made uniformly among all the choices.
	 */
	public static int[] argmaxset(double a[]) {
		int i, j, num;
		int[] set;
		double max = -1; // make the compiler stop complaining
		/*
		 * Figure out what the maximum value is, and how many elements achieve
		 * that.
		 */
		num = 0;
		for (i = 0; i < a.length; i++) {
			if (Double.isNaN(a[i]))
				continue;
			if (num == 0 || a[i] > max) {
				max = a[i];
				num = 1;
			} else if (a[i] == max)
				num++;
		}

		/* In this case they were all NaN, so return them all. */
		if (num == 0) {
			set = new int[a.length];
			for (i = 0; i < a.length; i++) {
				set[i] = i;
			}
			return set;
		}

		assert num <= a.length;
		set = new int[num];
		for (i = j = 0; j < num; i++) {
			assert i < a.length : String.format(
					"a=%s max=%f, num=%d i=%d set=%s\n", Arrays.toString(a),
					max, num, i, Arrays.toString(set));
			if (a[i] == max)
				set[j++] = i;
		}
		return set;
	}

	/*
	 * Returns true if any of the elements of a[] is NaN. Returns false
	 * otherwise.
	 */
	public static boolean isNaN(double a[]) {
		for (int x = 0; x < a.length; x++)
			if (Double.isNaN(a[x]))
				return true;
		return false;
	}

	public static int min(int a, int b) {
		if (a < b)
			return a;
		else
			return b;
	}

	public static double min(double a, double b) {
		if (a < b)
			return a;
		else
			return b;
	}

	/* Analogue of max and argmax for minimums. */
	public static double min(double a[]) {
		assert !isNaN(a) : Arrays.toString(a);
		double min = a[0];
		for (int i = 1; i < a.length; i++) {
			if (a[i] < min)
				min = a[i];
		}
		return min;
	}

	public static int argmin(double a[]) {
		assert !isNaN(a) : Arrays.toString(a);
		int argmin = 0;
		for (int i = 1; i < a.length; i++) {
			if (a[i] < a[argmin])
				argmin = i;
		}
		return argmin;
	}

	/*
	 * If we have two floats, and we want to know whether they are different, we
	 * can see how by how many ulps they differ. The ulp(x) is distance between
	 * x and the next largest double that can be represented in the double
	 * format. Usually one would take the result and see whether the result is
	 * less than some threshold. This function actually returns the absolute
	 * value of the difference divided by the ulp().
	 */
	public static int FloatCmp(double x, double y) {
		double diff = StrictMath.abs(x - y);
		double ulp = StrictMath.ulp(x);
		return (int) (diff / ulp);
	}

	/*
	 * Choose an element uniformly from the set of passed objects.
	 */
	public static int ChooseFromSet(int set[], Random rnd) {
		int i = rnd.nextInt(set.length);
		return set[i];
	}

	/*
	 * Test code.
	 */

	public static void main(String args[]) throws Exception {
		String myclass = "util.MathPF";
		System.out.printf("Testing %s\n", myclass);
		boolean ok = true;
		ok = test1() && ok;
		System.out.printf("%s: %s\n", myclass, ok ? "OK" : "FAILED");
	}

	/*
	 * Checks that the min/argmin and max/argmax routines can handle positive
	 * and negative infinities. Also checks that positive and negative
	 * infinities are not considered NaN.
	 */
	public static boolean test1() {
		boolean ok = true;
		double[] a = new double[3];
		a[0] = Double.NEGATIVE_INFINITY;
		a[1] = 0;
		a[2] = Double.POSITIVE_INFINITY;

		double vmax = max(a);
		if (vmax < Double.POSITIVE_INFINITY) {
			System.out.println("test1: vmax < infinity");
			ok = false;
		}

		int xmax1 = argmax(a);
		if (xmax1 != 2) {
			System.out.printf("test1: xmax1=%d != 2\n", xmax1);
			ok = false;
		}

		int xmax2 = argmax(a, new Random());
		if (xmax2 != 2) {
			System.out.printf("test1: xmax2=%d != 2\n", xmax2);
			ok = false;
		}

		int xmax3 = argmax(a, a);
		if (xmax3 != 2) {
			System.out.printf("test1: xmax3=%d != 2\n", xmax3);
			ok = false;
		}

		int xmax4 = argmaxSpecial(a, a, a);
		if (xmax4 != 2) {
			System.out.printf("test1: xmax4=%d != 2\n", xmax4);
			ok = false;
		}

		int[] xmaxset5 = argmaxset(a);
		if (xmaxset5.length != 1 && xmaxset5[0] != 2) {
			System.out.printf("test1: xmaxset5.length=%d xmaxset5[0]=%d!= 2\n",
					xmaxset5.length, xmaxset5[0]);
			ok = false;
		}

		double vmin = min(a);
		if (vmin > Double.NEGATIVE_INFINITY) {
			System.out.println("test1: vmin > -infinity");
			ok = false;
		}

		int xmin1 = argmin(a);
		if (xmin1 != 0) {
			System.out.printf("test1: xmin1=%d != 0\n", xmin1);
			ok = false;
		}

		if (isNaN(a)) {
			System.out.printf("test1: isNaN(a) returned true");
			ok = false;
		}

		System.out.printf("test1: %s\n", ok ? "OK" : "FAILED");
		return ok;
	}
}
