package InfoCollection.util;

/*
 * This is a version of the MeanVariance class, which collects mean and
 * variance from a stream of data, but it is designed to handle non-normal
 * data.  It does this by taking the stream of data and splitting it up into
 * batches of some fixed size.  The batch size is a parameter of the class.  It
 * then takes the simple average of each batch and calculates the sample mean
 * and variance of these batch averages.  If the batch size is large enough
 * then the batch averages will be approximately normally distributed (by the
 * law of large numbers).  The performance of this class depends on your choice
 * of batch size!
 * 
 * Since we often use this in situations where there are a lot of zeros, that
 * is, for aggregating opportunity costs, it also keeps track of the number of
 * times that an aggregate chunk had all zeros.  If this happens more than a
 * few times, or even more than just once, aggregation_size should be
 * increased.
 *
 * PF: would it be worth adding some other automated check for normality?
 * PF: should this extend MeanVariance?
 */
public class NonNormalMeanVariance {
	private int batch_size;
	private int batch_i;
	private double batch_avg;
	private int n_zero_batches;
	MeanVariance overall_mv;

	public NonNormalMeanVariance(int batch_size) throws Exception {
		batch_i = 0;
		batch_avg = 0;
		n_zero_batches = 0;
		this.batch_size = batch_size;
		if (batch_size <= 0)
			throw new Exception("batch size must be > 0");
		overall_mv = new MeanVariance();
	}

	public void AddSample(double x) {
		batch_avg = (batch_avg * batch_i + x) / (batch_i + 1);
		if (++batch_i == batch_size) {
			overall_mv.AddSample(batch_avg);
			if (batch_avg == 0)
				n_zero_batches++;
			batch_i = 0;
			batch_avg = 0;
		}
	}

	/*
	 * Note a few things about the following routines. First, SampleMean(),
	 * SampleMeanVariance(), and SampleMeanDeviation() are only influenced by
	 * the samples in completed batches. If there are samples leftover then
	 * these are not included. This number of samples included in completed
	 * batches can be obtained by calling NSampledBatched(). The total number of
	 * samples, including those left over, can be obtained by calling
	 * NSampledTotal().
	 */
	public double SampleMean() {
		return overall_mv.SampleMean();
	}

	public int NSampledTotal() {
		return batch_size * overall_mv.NSampled() + batch_i;
	}

	public int NSampledBatched() {
		return batch_size * overall_mv.NSampled();
	}

	public double SampleMeanVariance() {
		return overall_mv.SampleVariance();
	}

	public double SampleMeanDeviation() {
		return Math.sqrt(SampleMeanVariance());
	}

	@Override
	public String toString() {
		return SampleMean() + " +/- " + SampleMeanDeviation()
				+ " zero-batches/all-batches=" + n_zero_batches + "/"
				+ NumBatches();
	}

	public double NumZeroBatches() {
		return n_zero_batches;
	}

	public double NumBatches() {
		return overall_mv.NSampled();
	}
}
