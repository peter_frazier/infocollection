package InfoCollection.util;

import java.io.Serializable;
import java.util.Random;

/*
 * Class for collecting sample mean and variance from a stream of data.
 */
@SuppressWarnings("serial")
public class MeanVariance implements Serializable {
	private int i = 0;
	private double avg; // average value
	private double avg2; // average squared value

	public MeanVariance() {
		i = 0;
		avg = avg2 = 0;
	}

	/*
	 * Collect a new sample and update the mean and variance.
	 * 
	 * PF: Is this exponential smoothing ok with floating point errors? This is
	 * another way to do it that I saw on the internet:
	 * http://en.wikibooks.org/Handbook_of_Descriptive_Statistics
	 * /Measures_of_Statistical_Variability/Variance but I didn't understand it.
	 * if (i > 0) var += (var * (i-1) + StrictMath.pow(contribution - avg,2))/i;
	 */
	public void AddSample(double x) {
		avg = (avg * i + x) / (i + 1);
		avg2 = (avg2 * i + x * x) / (i + 1);
		i++;
	}

	/*
	 * SampleVariance() estimates the variance of the samples x.
	 * SampleMeanVarianceUnbiased() estimates the variance of the sample mean by
	 * (1/n)*(sum of square deviations). In some sense, this estimates the error
	 * in using the sample mean in place of the true mean.
	 * SampleMeanVarianceMSE() estimates the variance of the sample mean by
	 * (1/(n-1))*(sum of square deviations). This estimator has the minimum
	 * square error. SampleMeanDeviation() estimates the standard deviation of
	 * the sample mean. Again, this estimates the error of the sample mean.
	 */
	public double SampleMean() {
		return avg;
	}

	public double SampleVarianceUnbiased() {
		return avg2 - avg * avg;
	}

	public double SampleVarianceMSE() {
		double factor = i / ((double) i - 1);
		return factor * SampleVarianceUnbiased();
	}

	public double SampleVariance() {
		return SampleVarianceUnbiased();
	} // use the unbiased.

	public double SampleDeviation() {
		return StrictMath.sqrt(SampleVariance());
	}

	public int NSampled() {
		return i;
	}

	public double SampleMeanVariance() {
		return SampleVariance() / (NSampled() - 1);
	}

	public double SampleMeanDeviation() {
		return StrictMath.sqrt(SampleMeanVariance());
	}

	// public String toString() { return String.format("%f +/- %f",
	// SampleMean(), SampleMeanDeviation()); }
	@Override
	public String toString() {
		return SampleMean() + " +/- " + SampleMeanDeviation();
	}

	/*
	 * Unit test. Collect samples from a standard normal distribution and
	 * periodically display the statistics.
	 */
	public static void main(String args[]) throws Exception {
		int i;
		MeanVariance mv = new MeanVariance();
		Random rnd = new Random();

		for (i = 1; i <= 1000000; i++) {
			mv.AddSample(rnd.nextGaussian());
			if (i % 100000 == 0) {
				System.out
						.format(
								"i=%d SampleMean=%f SampleVariance=%f SampleMeanDeviation=%f\n",
								i, mv.SampleMean(), mv.SampleVariance(), mv
										.SampleMeanDeviation());
			}
		}

	}
}
