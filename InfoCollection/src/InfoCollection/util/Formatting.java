package InfoCollection.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;

public class Formatting {

	/*
	 * This function converts an array into a String that is nicely formatted
	 * for printing. This code is from the internet,
	 * http://www.codeproject.com/useritems/ArrayToString.asp
	 */
	public static String arrayToString(Object array) {
		if (array == null) {
			return "[NULL]";
		} else {
			Object obj = null;
			if (array instanceof Hashtable) {
				array = ((Hashtable<?, ?>) array).entrySet().toArray();
			} else if (array instanceof HashSet) {
				array = ((HashSet<?>) array).toArray();
			} else if (array instanceof Collection) {
				array = ((Collection<?>) array).toArray();
			}
			int length = Array.getLength(array);
			int lastItem = length - 1;
			StringBuffer sb = new StringBuffer("[");
			for (int i = 0; i < length; i++) {
				obj = Array.get(array, i);
				if (obj != null) {
					sb.append(obj);
				} else {
					sb.append("[NULL]");
				}
				if (i < lastItem) {
					sb.append(", ");
				}
			}
			sb.append("]");
			return sb.toString();
		}
	}
}
