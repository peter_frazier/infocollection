package InfoCollection.IndependentNormalVarianceUnknown;

import java.util.Arrays;

import InfoCollection.ThresholdStoppingRule;
import InfoCollection.ThresholdStoppingRuleSeq;
import InfoCollection.util.MathPF;

public class KGStoppingRule implements ThresholdStoppingRule<Double> {
	double logc;
	KG kg;
	boolean debug = false;

	public KGStoppingRule(double logc) {
		this.logc = logc;
		kg = new KG();
	}

	public KGStoppingRule(double logc, Belief b) {
		this.logc = logc;
		kg = new KG(b);
	}

	public Double GetThreshold() {
		return logc;
	}

	public void SetThreshold(Double threshold) {
		logc = threshold;
	}

	public String Name() {
		return "Variance Unknown KG Stopping Rule";
	}

	public void Start(int M) throws Exception {
		/*
		 * If we have M=1, then the KG factor will always be 0 and the stopping
		 * rule will never stop.
		 */
		if (M < 2)
			throw new Exception("Must have M>=2");
		kg.Start(M);
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		kg.RecordMeasurement(x, y);
	}

	public Double GetCurrentValue() {
		/*
		 * This code will work even if the belief is not integrable. If there is
		 * an error, we print it out and return that the current value is
		 * -infinity, which means that we should stop right away.
		 */
		try {
			return MathPF.max(kg.LogKGfactor());
		} catch (Exception e) {
			System.out
					.println("KGStoppingRule.ShouldStop() LogKGFactor reported exception.  Stopping.");
			System.out.println(kg.GetBelief());
			e.printStackTrace();
			return Double.NEGATIVE_INFINITY;
		}
	}

	public boolean ShouldStop() {
		if (debug) {
			System.out.printf("KGStoppingRule logc=%f currentval=%f\n", logc,
					GetCurrentValue());
			try {
				System.out
						.println("logkg=" + Arrays.toString(kg.LogKGfactor()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println(kg.belief);
		}
		return (GetCurrentValue() < logc);
	}

	@Override
	public String toString() {
		return "Variance unknown KGStoppingRule logc-threshold=" + logc;
	}

	/*
	 * Create custom and standard stopping rule sequences. These are mostly for
	 * convenience.
	 */
	public static ThresholdStoppingRuleSeq<Double> Seq(double[] logc) {
		Double[] _logc = new Double[logc.length];
		for (int i = 0; i < logc.length; i++)
			_logc[i] = logc[i];
		return new ThresholdStoppingRuleSeq<Double>(_logc, new KGStoppingRule(
				logc[0]));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq() {
		/*
		 * To get even spacing on graphs, the logc values used should grow
		 * exponentially.
		 */
		double logc[] = { -1, -2, -3, -4, -5, -8, -10, -12, -15, -17, -20, -25,
				-30, -35, -40, -50, -60, -70, -80, -90, -100, -150 };
		return Seq(logc);
	}

	public static ThresholdStoppingRuleSeq<Double> Seq(double[] logc, Belief b) {
		Double[] _logc = new Double[logc.length];
		for (int i = 0; i < logc.length; i++)
			_logc[i] = logc[i];
		return new ThresholdStoppingRuleSeq<Double>(_logc, new KGStoppingRule(
				logc[0], b));
	}

	public static ThresholdStoppingRuleSeq<Double> Seq(Belief b) {
		double logc[] = { -1, -2, -3, -4, -5, -8, -10, -15, -20, -30, -40, -50,
				-60, -70, -80, -90, -100, -150 };
		return Seq(logc, b);
	}

}
