package InfoCollection.IndependentNormalVarianceUnknown;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;

import InfoCollection.NormalTruth;
import InfoCollection.TestExamples;
import InfoCollection.util.ArrayIndexComparator;
import InfoCollection.util.MathPF;
import InfoCollection.util.Student;

/*
 * Variance unknown version of LL from Chick & Inoue.  The code is very similar
 * to InfoCollection.IndependentNormalVarianceKnown.ChIn.
 */

public class ChIn extends BayesSamplingRule {
	int n0; // number of measurements per alternative in the first stage
	int tau; // number of measurements to allocate per stage
	LinkedList<Integer> measurementsToGive;
	boolean debug = false;

	public ChIn() {
		this(6, 1);
	}

	public ChIn(Belief b) {
		this(6, 1, b);
	}

	public ChIn(int n0, int tau, Belief b) {
		super(b);
		this.n0 = n0;
		this.tau = tau;
		measurementsToGive = new LinkedList<Integer>();
	}

	public ChIn(int n0, int tau) {
		belief0 = null; // noninformative, flexible M.
		this.n0 = n0;
		this.tau = tau;
		measurementsToGive = new LinkedList<Integer>();
	}

	@Override
	public void Start(int desiredM) throws Exception {
		super.Start(desiredM);
		int i, x;
		measurementsToGive.clear();
		/*
		 * Allows doing a first stage in which n0 measurements are allocated to
		 * each alternative.
		 */
		for (i = 0; i < n0; i++)
			for (x = 0; x < M(); x++)
				measurementsToGive.add(x);
		/*
		 * We may have errors later if we begin with a noninformative prior on
		 * an alternative, and n0 = 0. Check for this and complain if we see it.
		 * An alternative way to handle it would be to pick a value of n0, like
		 * n0=6, but instead of just allocating 6 measurements round robin to
		 * each alternative as we do above, just allocate measurements round
		 * robin to those alternatives with n(x) < 6.
		 */
		if (n0 == 0 && !belief.IsIntegrable()) {
			throw new Exception(
					"ChIn cannot handle non-integrable belief and n0 == 0");
		}
	}

	/*
	 * Computes the "Term" array, where term[i] is actually
	 * sqrt(\sigmahat^2_i*gamma_{i}) If the best alternative is the only one in
	 * the inclusionSet, then that term will be 0 and all the rest will be
	 * unused. Otherwise, the term should be > 0.
	 */
	public double[] ComputeTerm(boolean inclusionSet[]) {
		int best, k;
		double s, dstar, lambda_1k, dof, gamma, gamma_best;
		double debug_gamma[], debug_lambda_1k[], debug_dof[], debug_dstar[];
		double term[] = new double[M()];
		boolean includeBest;

		best = MathPF.argmax(belief.mu);
		includeBest = inclusionSet[best];

		if (debug) {
			/*
			 * If we are debugging, it is nice to remember the gammas and
			 * lambdas in an array to make it easy to print them out.
			 */
			debug_gamma = new double[M()];
			debug_lambda_1k = new double[M()];
			debug_dof = new double[M()];
			debug_dstar = new double[M()];
		} else {
			// Make the compiler not complain.
			debug_gamma = null;
			debug_lambda_1k = null;
			debug_dof = null;
			debug_dstar = null;
		}

		gamma_best = 0;
		for (k = 0; k < M(); k++) {
			/*
			 * The gamma and term of the best alternative is the sum of the
			 * others, so we don't need to do anything here if k == best.
			 */
			if (k == best) {
				term[k] = -1;
				if (debug) {
					debug_lambda_1k[k] = -1; // unused
					debug_dof[k] = -1;
					debug_dstar[k] = -1;
				}
				continue;
			}
			/*
			 * If this alternative isn't being included, don't add its gamma to
			 * the gamma of the best alternative.
			 */
			if (!inclusionSet[k]) {
				term[k] = -1;
				if (debug) {
					debug_lambda_1k[k] = -1; // unused
					debug_dof[k] = -1;
					debug_dstar[k] = -1;
				}
				continue;
			}

			/*
			 * The computation of lambda_1k depends on what is in the inclusion
			 * set.
			 */
			if (includeBest) {
				lambda_1k = 1.0 / belief.DifferenceScale(k, best);
				dof = belief.DifferenceDofWelch(k, best);
			} else {
				lambda_1k = belief.nForMean(k) / belief.sigmahat2(k);
				dof = belief.nForPrecision(k) - 1;
			}
			assert dof > 1 : dof; // otherwise gamma will be infinity.

			/*
			 * Now actually compute gamma and the term. Be careful that the
			 * computation works for large values of dof because our test
			 * comparisons of VarianceUnknown.ChIn and VarianceKnown.ChIn
			 * reproduce the variance known results with the variance unknown
			 * algorithm by taking the knowledge of the sampling precision to be
			 * very precise, pushing the degrees of freedom large. Originally
			 * gamma was computed by
			 * s*(dof+dstar*dstar)*Student.Pdf(dof,dstar)/(dof-1), but this did
			 * not work when dof was large.
			 */
			s = StrictMath.sqrt(lambda_1k); // like sqrt of precision
			dstar = s * (belief.mu[best] - belief.mu[k]);
			gamma = s * (1 + (dstar * dstar + 1) / (dof - 1))
					* Student.Pdf(dof, dstar);
			gamma_best += gamma;
			term[k] = StrictMath.sqrt(belief.sigmahat2(k) * gamma);
			if (debug) {
				debug_lambda_1k[k] = lambda_1k;
				debug_dof[k] = dof;
				debug_dstar[k] = dstar;
				debug_gamma[k] = gamma;
			}
		}
		if (debug)
			debug_gamma[best] = gamma_best;
		term[best] = StrictMath.sqrt(belief.sigmahat2(best) * gamma_best);

		if (debug) {
			System.out.println("best=" + best);
			System.out.println(belief);
			System.out.println("lambda_1k=" + Arrays.toString(debug_lambda_1k));
			System.out.println("dof=" + Arrays.toString(debug_dof));
			System.out.println("dstar=" + Arrays.toString(debug_dstar));
			System.out.println("gamma=" + Arrays.toString(debug_gamma));
			System.out.println("term=" + Arrays.toString(term));
		}

		return term;
	}

	/*
	 * Allocate more measurements according to the algorithm, and put them into
	 * the measurementsToGive list in round-robin fashion.
	 */
	public void AllocateMore() {
		int i, x, budget, num_fractional, nincluded;
		boolean redo, inclusionSet[] = new boolean[M()];
		double r[] = new double[M()];
		double term[];
		double sum; // only used for assert logic code.

		if (debug)
			System.out.println("AllocateMore (VarianceUnknown.ChIn)");

		/*
		 * If we are not integrable, we need to allocate until we are. PF: fill
		 * this in. Right now it is handled by having n0 large enough.
		 */
		// if (!belief.IsIntegrable())
		assert belief.IsIntegrable();

		/*
		 * In the traditional version of the algorithm, we check how many
		 * measurements are left and if this is less than tau, then we only
		 * allocate up to that. In this version, we ignore this, allocating tau
		 * each time. This is ok if tau divides the total budget, as happens
		 * when tau=1, or when N is infinite.
		 */
		budget = tau;

		/*
		 * At first include all the alternatives.
		 */
		nincluded = M();
		for (x = 0; x < M(); x++) {
			inclusionSet[x] = true;
		}

		/*
		 * Get an allocation which is non-negative, but not necessarily
		 * integral.
		 */
		redo = true;
		num_fractional = 0; // to quiet the compiler
		while (redo) {
			double numerator = budget;
			double denominator = 0;

			/*
			 * If only one alternative is in the inclusion set, allocate all the
			 * measurements to it and exit the loop.
			 */
			assert nincluded >= 1;
			if (nincluded == 1) {
				// Find the one to include
				for (x = 0; !inclusionSet[x] && x < M(); x++)
					;
				assert x < M();
				num_fractional = 0;
				r[x] = budget;
				break; // break out of the while(redo) loop.
			}

			term = ComputeTerm(inclusionSet);
			for (x = 0; x < M(); x++) {
				if (!inclusionSet[x])
					continue;
				numerator += belief.nForMean(x);
				assert term[x] >= 0;
				denominator += term[x];
			}
			if (debug) {
				double tmp[] = new double[M()];
				for (x = 0; x < M(); x++) {
					if (!inclusionSet[x])
						tmp[x] = 0;
					else
						tmp[x] = term[x] * numerator / denominator;
				}
				System.out.printf("numerator=%f denominator=%f\n", numerator,
						denominator);
				System.out.printf("term[]*numerator/denominator=%s\n", Arrays
						.toString(tmp));
			}
			assert denominator > 0 : "inclusionSet="
					+ Arrays.toString(inclusionSet) + " term="
					+ Arrays.toString(term);
			redo = false;
			num_fractional = 0;
			for (x = 0; x < M(); x++) {
				if (!inclusionSet[x]) {
					// setting r[x]=0 here is redundant
					r[x] = 0;
					continue;
				}
				r[x] = (term[x] * numerator / denominator) - belief.nForMean(x);
				assert !Double.isNaN(r[x]) : String.format(
						"x=%d term[x]=%f numerater=%f denominator=%f", x,
						term[x], numerator, denominator);
				if (r[x] < 0) {
					/*
					 * Remove it from the inclusion set, set r[x] to 0, and set
					 * the redo flag so that we iterate again. Continue looping
					 * through the rest of the alternatives so that we can find
					 * other x with r[x] < 0.
					 */
					inclusionSet[x] = false;
					nincluded--;
					r[x] = 0;
					redo = true;
				} else if (r[x] - StrictMath.floor(r[x]) > 0) {
					num_fractional++;
				}
			}
		}
		if (debug)
			System.out.println("r=" + Arrays.toString(r));
		/*
		 * At this point, all the r[x] should be nonnegative, and they should
		 * sum to the whole budget, but they don't have to be integer-valued.
		 */
		/*
		 * This is error checking code that could go inside an assert block.
		 */
		sum = 0;
		for (x = 0; x < M(); x++) {
			sum += r[x];
			assert r[x] >= 0 : Arrays.toString(r);
		}
		/*
		 * Note that when we compare two floats for equality, there is some
		 * rounding error, so we just make sure that the absolute value of the
		 * difference is less than some small value.
		 */
		assert StrictMath.abs(sum - budget) < 1E-10 : "sum=" + sum + " budget="
				+ budget + " r=" + Arrays.toString(r);

		/*
		 * Now round the r[x]. Sort the x with non-integral entries according to
		 * their non-intgeral part. Note that when we finish the previous loop
		 * we have a count of how many r[x] entries have fractional parts and
		 * need to be rounded.
		 */
		if (debug)
			System.out.println("r=" + Arrays.toString(r));
		if (num_fractional > 0) {
			Integer rlist[] = new Integer[num_fractional];
			double r_frac[] = new double[M()]; // fractional part of r
			int sofar = 0;
			for (i = x = 0; x < M(); x++) {
				double r_round = StrictMath.floor(r[x]);
				r_frac[x] = r[x] - r_round;
				sofar += r_round;
				if (r_frac[x] == 0)
					continue;
				r[x] = r_round; // round down
				rlist[i++] = x; // remember the fractional ones
				assert (x >= 0);
				assert (x < M());
			}
			assert i == num_fractional : "i=" + i + " num_fractional="
					+ num_fractional;
			Comparator<Integer> c = new ArrayIndexComparator(r_frac);
			Arrays.sort(rlist, c);

			/*
			 * We now have budget - sofar to give. Allocate them to the entries
			 * with the largest fractional part. The sort has put them in
			 * ascending numerical order.
			 */
			int togive = budget - sofar;
			assert togive <= num_fractional : "togive=" + togive + " sofar="
					+ sofar + " num_fractional=" + num_fractional;
			for (i = 1; i <= togive; i++) {
				x = rlist[num_fractional - i]; // since they were in ascending
												// order
				assert x >= 0;
				assert x < M();
				r[x]++;
			}
		}
		/*
		 * At this point, all the r[x] should be integer, and they should sum to
		 * the whole budget.
		 */
		// assert code
		sum = 0;
		for (x = 0; x < M(); x++) {
			sum += r[x];
			assert StrictMath.floor(r[x]) == r[x] : x + " " + r[x];
			assert r[x] >= 0 : Arrays.toString(r);
		}
		assert (sum == budget);
		// System.out.println("r=" + Arrays.toString(r));

		/*
		 * Now put these allocations onto the measurementsToGive list. Put on
		 * r[x] measurements for each x.
		 */
		for (x = 0; x < M(); x++) {
			for (i = 0; i < r[x]; i++)
				measurementsToGive.add(x);
		}
	}

	public int GetMeasurementDecision() throws Exception {
		/*
		 * If there are more measurements in the currently allocated bucket,
		 * allocate them. Otherwise need to allocate more.
		 */
		// System.out.println("GetMeasurementDecision onqueue=" +
		// measurementsToGive.size());
		if (measurementsToGive.size() == 0)
			AllocateMore();
		if (measurementsToGive.size() == 0)
			throw new Exception("Failure to Allocate More");
		// System.out.println("After AllocateMore onqueue=" +
		// measurementsToGive.size());
		return measurementsToGive.removeFirst();
	}

	@Override
	public String toString() {
		return "ChIn: Chick & Inoue LL(S) unknown variance n0=" + n0 + " tau="
				+ tau;
	}

	public static void main(String args[]) throws Exception {
		// TestExamples.RandomProblem(new ChIn(1,1));
		// TestExamples.SlippageWithKGStopping(new ChIn(6,1));
		// TestExamples.SlippageWithEOCBonfStopping(new ChIn(6,1));
		// System.out.println(Simulator.SimulateOpportunityCost(new ChIn(1,10),
		// Problem.Example1(900), 5000, 0));
		// TestExamples.Example1(new ChIn(), true);
		// TestExamples.Example1SpeedTest(new ChIn(1,10));
		Regression();
	}

	public static void Regression() {
		// test1();
		test2();
	}

	/*
	 * This may potentially be a regression test. The idea is that if we run the
	 * LL variance unknown policy with n0 reasonably high, by the time the n0 is
	 * exhausted it has a pretty good idea of what the sigmas are.
	 */
	/*
	 * public static boolean test1() { p1 = new ChIn(20,1); CheckEquivalence(
	 * //TestExamples.Example1SpeedTest(new ChIn(1,10)); }
	 */

	/*
	 * Compare this implementation of LL (with variance unknown) to the version
	 * with variance known by giving this LL a prior where the sampling
	 * precisions are essentially known under the prior.
	 * 
	 * We only get 0 disagreements sometimes, because sometimes we get a
	 * situation where there is a tie between alternatives and then the variance
	 * known LL just chooses one, while the variance unknown LL has some
	 * difference in the predictive precision because of its past observations,
	 * and even though this differences is absolutely tiny it is enough to break
	 * the tie, sometimes the opposite way of LL. This often happens once per
	 * run, and occasionally several times. I have seen up to 14. If I just
	 * report an error if it happens 6 times or more, then that keeps the false
	 * alarms down. To really check, you have to turn on debug and see if all
	 * the ones where there were disagreements are the result of ties by looking
	 * at the r values before they were rounded.
	 */
	public static boolean test2() {
		NormalTruth truth = NormalTruth.SlippageExample();
		Belief b = new Belief(truth.SamplingPrecision());
		InfoCollection.SamplingRule p1 = new ChIn(6, 1, b);
		InfoCollection.SamplingRule p2 = new InfoCollection.IndependentNormalVarianceKnown.ChIn(
				6, 1);
		int numDisagreements = 0;
		boolean ok = true;
		try {
			numDisagreements = TestExamples.CheckEquivalence(p1, p2, truth,
					false);
			ok = numDisagreements <= 5;
			if (!ok)
				System.out.println("test2: numDisagreements="
						+ numDisagreements);
		} catch (Exception e) {
			e.printStackTrace();
			ok = false;
		}
		if (ok)
			System.out.println("test2: OK");
		else
			System.out.println("test2: FAILED");
		return ok;
	}
};
