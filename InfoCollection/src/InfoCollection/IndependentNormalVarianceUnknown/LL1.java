package InfoCollection.IndependentNormalVarianceUnknown;

import java.util.Random;

import InfoCollection.TestExamples;
import InfoCollection.util.MathPF;

public class LL1 implements InfoCollection.SamplingRule {
	Random rnd; // for breaking ties.
	KG kg;

	public LL1() {
		kg = new KG();
		rnd = new Random();
	}

	public LL1(Belief b) {
		kg = new KG(b);
		rnd = new Random();
	}

	public void Start(int M) throws Exception {
		kg.Start(M);
	}

	public int GetImplementationDecision() {
		return MathPF.argmax(kg.belief.mean(), rnd);
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		kg.RecordMeasurement(x, y);
	}

	public int GetMeasurementDecision() throws Exception {
		/* Breaks ties randomly. */
		if (kg.IsIntegrable()) {
			double[] logkg = kg.LogKGfactor();
			// System.out.printf("logKG=%s\n", Arrays.toString(logkg));
			return MathPF.argmax(logkg, rnd);
		} else {
			// System.out.printf("not integrable\n");
			// System.out.println(kg.belief.toString());
			int choice = kg.belief.ChooseRandomNotIntegrable(rnd);
			assert !kg.belief.IsIntegrable(choice);
			return choice;
		}
	}

	public double[] GetLogQ() throws Exception {
		return kg.LogKGfactor();
	}

	@Override
	public String toString() {
		return "LL1";
	}

	public static void main(String args[]) throws Exception {
		Regression();
	}

	public static void Regression() {
		// test1(); // test1 and 2 are not that good for regression.
		// test2();
		test3();
	}

	/*
	 * Just run it on example 1 to see if there are any errors.
	 */
	public static boolean test1() {
		try {
			TestExamples.Example1(new LL1Simple(), false);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test1: FAILED");
			return false;
		}
		System.out.println("test1: OK");
		return true;
	}

	/*
	 * Run it on example 1 with a KG stopping rule to see if there are any
	 * errors.
	 */
	public static boolean test2() {
		try {
			TestExamples.TestStopping(new LL1Simple(), KGStoppingRule.Seq());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test2: FAILED");
			return false;
		}
		System.out.println("test2: OK");
		return true;
	}

	/*
	 * Run two different KGs, one where decisions are made statically, and the
	 * other where decisions are made using stored state. The decisions made
	 * should be identical.
	 */
	public static boolean test3() {
		int numDisagreements = 0;
		try {
			numDisagreements = TestExamples.CheckEquivalence(new LL1Simple(),
					new LL1(), true);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test3: FAILED");
			return false;
		}
		if (numDisagreements < 10) {
			System.out.printf("test3: OK (numDisagreements=%d)\n",
					numDisagreements);
			return true;
		} else {
			System.out.printf("test3: FAILED (numDisagreements=%d)\n",
					numDisagreements);
			return false;
		}
	}

	/*
	 * Compare the variance-known KG to a variance-unknown KG whose gamma prior
	 * on the decisions are actually quite certain. The decisions should be the
	 * same. PF: I shouldn't have to do this because I compare LL1Simple to
	 * variance-known KG.
	 */
	/*
	 * public static boolean test4() { NormalTruth truth =
	 * NormalTruth.Example1(); SamplingRule p1 = new
	 * InfoCollection.IndependentNormalVarianceKnown.KG(); // PF: not done yet:
	 * //Create a belief where the means are noninformative, but the
	 * //precisions are fixed to the precisions in NormalTruth. SamplingRule p2
	 * = new LL1(); int numDisagreements = TestExamples.CheckEquivalence(p1, p2,
	 * true); if (numDisagreements < 10)
	 * System.out.printf("test4: OK (numDisagreements=%d)\n", numDisagreements);
	 * else System.out.printf("test4: FAILED (numDisagreements=%d)\n",
	 * numDisagreements);
	 * 
	 * }
	 */

};
