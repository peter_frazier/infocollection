package InfoCollection.IndependentNormalVarianceUnknown;

public abstract class BayesStoppingRule implements InfoCollection.StoppingRule {
	int n;
	Belief belief0;
	Belief belief;

	public BayesStoppingRule() {
		belief0 = null;
		belief = null;
		n = 0;
	}

	public BayesStoppingRule(Belief b) {
		belief0 = b;
		belief = null;
		n = 0;
	}

	public void Start(int desiredM) throws Exception {
		n = 0;
		if (belief0 == null)
			belief = new Belief(desiredM); // non-informative.
		else if (desiredM == belief0.M())
			belief = belief0;
		else
			throw new Exception("belief0 and desiredM don't match.");
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		n++;
		belief.Update(x, y);
	}
}
