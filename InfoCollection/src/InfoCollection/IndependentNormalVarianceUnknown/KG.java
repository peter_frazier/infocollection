package InfoCollection.IndependentNormalVarianceUnknown;

import java.util.Arrays;

import InfoCollection.util.MathPF;
import InfoCollection.util.Student;

/*
 * This class contains methods for calculating the 1-step Q-factor, also called
 * the KG factor, for a collection of alternatives.  It has static routines,
 * and it also has the ability to maintain the Q-factors and associated
 * quantities for an internal belief which gets updated through a
 * RecordMeasurement() function of the same type as used in SamplingRule and
 * StoppingRule.  Maintaining them dynamically on an internal belief is faster
 * than re-calling the static routines each time the belief is updated.
 *
 * Note that there is *a lot* of overlap between this code and 
 * IndependentNormalVarianceKnown/KnowledgeGradient.java
 */
public class KG {
	Belief belief0;
	Belief belief;
	int first, second; // indices of best and second-best alternatives
	double sigmatilde[];
	double logKG[];
	static boolean debug = false;

	public KG() {
		belief0 = null;
		belief = null;
	}

	/*
	 * Remember that we need to keep a COPY of our belief in belief0, rather
	 * than the original thing.
	 */
	public KG(Belief b) {
		belief0 = new Belief(b);
		belief = null;
	}

	/*
	 * You MUST call this before calling any other routines, even if you
	 * instantiate KG with a concrete Belief.
	 */
	public void Start(int desiredM) throws Exception {
		// System.out.println("kg.Start");
		if (desiredM < 1)
			throw new Exception("desiredM must be >= 1");
		if (belief0 == null)
			belief = new Belief(desiredM); // non-informative.
		else if (desiredM == belief0.M())
			belief = new Belief(belief0);
		else
			throw new Exception("belief0 and desiredM don't match.");
		sigmatilde = new double[M()];
		logKG = new double[M()];
		/*
		 * If the belief is not integrable, we don't bother computing Sigmatilde
		 * and FirstSecond until after everything becomes integrable. They are
		 * not defined until then. RecomputeKGfactors still works if the belief
		 * is not integrable.
		 */
		if (belief.IsIntegrable()) {
			RecomputeSigmatilde();
			RecomputeFirstSecond();
		}
		RecomputeKGfactors();
	}

	/*
	 * Set the indices of the best (called "first") and second best mu's.
	 */
	private void RecomputeFirstSecond() {
		assert belief.IsIntegrable();
		int ret[];
		ret = MathPF.FirstSecond(belief.mu);
		first = ret[0];
		second = ret[1];
	}

	/*
	 * Recompute the KGfactor for only one alternative. Assumes that
	 * sigmatilde[x] is correct. Also, assumes that first and second are
	 * correct, although there is one exception: if first is right but second is
	 * wrong and you know that x is not second best, this routine will work. In
	 * some cases we could get rid of the if statement because we know whether
	 * or not x == first, but this optimization would add code and wouldn't
	 * improve speed much.
	 */
	private void RecomputeKGfactor(int x) {
		double distance, influence;
		// System.out.println("RecomputeKGfactor x=" + x);
		if (!belief.IsIntegrable(x)) {
			logKG[x] = Double.POSITIVE_INFINITY;
			return;
		}
		if (sigmatilde[x] == 0) {
			/*
			 * We do this so that we don't have to worry about dividing by 0 to
			 * compute influence.
			 */
			logKG[x] = Double.NEGATIVE_INFINITY;
			return;
		}
		// We make the distance positive so that we don't need to
		// calculate an absolute value.
		if (x == first)
			distance = belief.mu[x] - belief.mu[second];
		else
			distance = belief.mu[first] - belief.mu[x];
		assert distance >= 0;
		influence = -distance / sigmatilde[x];
		try {
			logKG[x] = StrictMath.log(sigmatilde[x])
					+ Student.LogPsi(belief.dof(x), -influence);
		} catch (Exception e) {
			System.out
					.println("Warning: Student.LogPsi failed.  KG may return incorrect decisions.");
			e.printStackTrace();
		}
	}

	/*
	 * Recompute the KGfactors for all of the alternatives. Assumes that the
	 * first and the second indices are correct; also assumes that the
	 * sigmatildes are correct.
	 */
	private void RecomputeKGfactors() {
		for (int i = 0; i < M(); i++)
			RecomputeKGfactor(i);
	}

	/*
	 * RecomputeSigmatildes() recomputes all the sigmatilde values from scratch.
	 * RecomputeSigmatilde(int x) recompute the sigmatilde for alternative x.
	 */
	private void RecomputeSigmatilde() {
		assert belief.IsIntegrable();
		for (int x = 0; x < M(); x++)
			RecomputeSigmatilde(x);
	}

	private void RecomputeSigmatilde(int x) {
		assert belief.IsIntegrable();
		sigmatilde[x] = 1.0 / StrictMath.sqrt(belief.PredictivePrecision(x));
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		boolean wasIntegrable = belief.IsIntegrable();
		belief.Update(x, y);
		if (!wasIntegrable) {
			if (belief.IsIntegrable()) {
				/*
				 * We were not integrable before, and so sigmatilde and
				 * first,second were not defined. But they are now. Recompute
				 * them, and then recompute the KG factors.
				 */
				RecomputeSigmatilde();
				RecomputeFirstSecond();
				RecomputeKGfactors();
			} else if (belief.IsIntegrable(x)) {
				/*
				 * We are still not integrable overall, but the one we just
				 * measured is integrable now. So we leave the unintegrable
				 * alternatives with +infinity kg factors, and set the now
				 * integrable alternative to have kg factor of 0. Note
				 * log(0)=-infinity.
				 */
				logKG[x] = Double.NEGATIVE_INFINITY;
			} else {
				/*
				 * Otherwise we were not integrable before, and alternative x is
				 * still not integrable, so it should remain unchanged at
				 * +infinity.
				 */
				assert logKG[x] == Double.POSITIVE_INFINITY;
			}
			return;
		}
		/*
		 * Only alternative x has changed. Change its sigmatilde, and then do
		 * different things based on whether the best or second best alternative
		 * has changed.
		 */
		RecomputeSigmatilde(x);
		if (belief.mu[x] > belief.mu[first]) {
			/*
			 * The measured alternative replaces the largest. The old largest is
			 * now second largest. All the Q-factors change.
			 */
			second = first;
			first = x;
			RecomputeKGfactors();
		} else if (x == first && belief.mu[x] < belief.mu[second]) {
			/*
			 * We measured the largest and it is now lower than what used to be
			 * the second best. The old second best becomes best. We don't know
			 * which alternative is now second best. We loop through the
			 * alternatives once, computing all Q-factors except for the best
			 * alternative, and finding which alternative is second best. Then,
			 * we compute the Q-factor for the best alternative.
			 */
			int oldfirst = first;
			first = second;
			/*
			 * We just need to initialize second to something different from
			 * first we also could have done: second = first == 0 ? 1 : 0;
			 */
			second = oldfirst;
			for (int i = 0; i < M(); i++) {
				if (i == first)
					continue;
				else if (belief.mu[i] > belief.mu[second])
					second = i;
				RecomputeKGfactor(i);
			}
			RecomputeKGfactor(first);
		} else if (x == first) {
			/*
			 * We measured the best alternative, and it changed, but it is still
			 * the best. Thus we don't have to change first or second, but we do
			 * have to change all the KGfactors.
			 */
			RecomputeKGfactors();
		} else if (belief.mu[x] > belief.mu[second]) {
			/*
			 * We measured an alternative that was neither best nor second best,
			 * but it became second best. We have to recompute the KGfactors for
			 * both the best and for this alternative.
			 */
			second = x;
			RecomputeKGfactor(x);
			RecomputeKGfactor(first);
		} else if (x == second) {
			/*
			 * We measured the second best alternative. We know that it didn't
			 * change enough to make it best, but it might have changed enough
			 * to no longer be the second best.
			 */
			RecomputeFirstSecond();
			RecomputeKGfactor(x);
			RecomputeKGfactor(first);
		} else {
			/*
			 * Neither the indices of, nor the values of, the best and the
			 * second best changed. Recompute the qfactor only for the
			 * alternative that we measured.
			 */
			RecomputeKGfactor(x);
		}

	}

	public int M() {
		return belief.M();
	}

	public Belief GetBelief() {
		return belief;
	}

	public boolean IsIntegrable() {
		return belief.IsIntegrable();
	}

	public double[] LogKGfactor() throws Exception {
		return logKG; // do it dynamically
		// if we want to do it statically, use the following code. I
		// could add a check to see if the two methads give the same
		// answer, but it would be really slow.
		// return LogKGfactor(belief);
	}

	/*
	 * Static routines for computing quantities from a passed belief.
	 */

	/*
	 * This function returns an array whose ith component is \sigmatilde_i.
	 */
	static public double[] Sigmatilde(Belief b) {
		assert b.IsIntegrable();
		double[] sigmatilde = new double[b.M()];
		for (int x = 0; x < b.M(); x++) {
			sigmatilde[x] = 1.0 / StrictMath.sqrt(b.PredictivePrecision(x));
		}
		return sigmatilde;
	}

	/*
	 * Computes the unnormalized influences of each alternative in a belief.
	 * This is called d_{(j)(k)} in Chick,Branke,Schmidt 2007. Requires that
	 * b.M>=2.
	 */
	static public double[] UnnormalizedInfluence(Belief b) throws Exception {
		int x, first, second;

		if (b.M() < 2)
			throw new Exception("Must have b.M()>=2");
		if (!b.IsIntegrable())
			throw new Exception("The Belief must be integrable.");

		/*
		 * Find the best & second best. Start by figuring it out among the first
		 * two alternatives, and then iterate through the rest to find out if
		 * there are any better.
		 */
		if (b.mean(0) >= b.mean(1)) {
			first = 0;
			second = 1;
		} else {
			first = 1;
			second = 0;
		}
		for (x = 2; x < b.M(); x++) {
			if (b.mean(x) > b.mean(first)) {
				second = first;
				first = x;
			} else if (b.mean(x) > b.mean(second)) {
				second = x;
			}
		}

		/*
		 * Now compute the difference between x and the "best of the rest". This
		 * is the unnormalized influence.
		 */
		double[] influence = new double[b.M()];
		influence[first] = b.mean(second) - b.mean(first);
		for (x = 0; x < b.M(); x++) {
			if (x == first)
				continue;
			influence[x] = b.mean(x) - b.mean(first);
		}
		return influence;
	}

	/*
	 * The normalized influence is just the unnormalized influence divided by
	 * sigmatilde. Only works if b.M() >= 2.
	 */
	static public double[] NormalizedInfluence(Belief b) throws Exception {
		assert b.IsIntegrable();
		double[] ans = UnnormalizedInfluence(b);
		double[] sigmatilde = Sigmatilde(b);
		for (int x = 0; x < b.M(); x++)
			ans[x] /= sigmatilde[x];
		return ans;
	}

	/*
	 * Computes in a static fashion the logarithm of the KG factor, where
	 * KGfactor(x) = Qfactor(x) - max \mu. This is the one-step look-ahead value
	 * of measuring x. Can throw an exception if it is outside numerical
	 * accuracy. If any alternatives are not integrable, then the KG factor is
	 * not well defined, even for those alternatives that are integrable. This
	 * is because
	 */
	static public double[] LogKGfactor(Belief b) throws Exception {
		double[] logkg = new double[b.M()];
		assert b.M() >= 1;
		if (!b.IsIntegrable()) {
			/*
			 * If b is not integrable, we define the KG factor to be infinite
			 * for all the non-integrable components and 0 for the integrable
			 * ones. This choice of 0 is a little arbitrary because the max of
			 * the current belief on the remaining is undefined.
			 */
			for (int x = 0; x < b.M(); x++) {
				if (b.IsIntegrable(x))
					logkg[x] = Double.NEGATIVE_INFINITY;
				else
					logkg[x] = Double.POSITIVE_INFINITY;
			}
		} else if (b.M() == 1) {
			/*
			 * If there is only one alternative, Qfactor(x) = mu(x), and the KG
			 * factor is 0.
			 */
			logkg[0] = 0;
		} else {
			double[] influence = null;
			try {
				/* This should succeed because b.M() >= 2. */
				influence = NormalizedInfluence(b);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}
			double[] s = Sigmatilde(b);
			// System.out.println(b);
			// System.out.println("s=" + Arrays.toString(s));
			// System.out.println("influence=" + Arrays.toString(influence));
			for (int x = 0; x < b.M(); x++) {
				logkg[x] = StrictMath.log(s[x])
						+ Student.LogPsi(b.dof(x), -influence[x]);
			}
		}
		return logkg;
	}

	public static void main(String args[]) {
		test1();
		test2();
		test3();
		test4();
	}

	/*
	 * Tests 1 & 2 test to see that this class correctly returns the KG factors
	 * for beliefs that are not integrable. They should be infinite. test1 does
	 * the static version and test2 the dynamic.
	 */
	public static boolean test1() {
		Belief b = new Belief(5);
		KG kg = new KG(b);
		double logkg[];
		try {
			kg.Start(5);
			logkg = kg.LogKGfactor();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test1: FAILED");
			return false;
		}
		for (int x = 0; x < kg.M(); x++)
			if (logkg[x] != Double.POSITIVE_INFINITY) {
				System.out.println("test1: FAILED");
				return false;
			}
		System.out.println("test1: OK");
		return true;
	}

	public static boolean test2() {
		Belief b = new Belief(5);
		double logkg[];
		try {
			logkg = KG.LogKGfactor(b);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test2: FAILED");
			return false;
		}
		for (int x = 0; x < b.M(); x++)
			if (logkg[x] != Double.POSITIVE_INFINITY) {
				System.out.println("test2: FAILED");
				return false;
			}
		System.out.println("test2: OK");
		return true;
	}

	public static boolean test3() {
		Belief b = new Belief(5);
		KG kg = new KG(b);
		double logkg[];
		try {
			kg.Start(5);
			kg.RecordMeasurement(0, 1.0); // make alternative 0 informative.
			kg.RecordMeasurement(0, 1.0);
			kg.RecordMeasurement(0, 1.0);
			logkg = kg.LogKGfactor();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test3: FAILED");
			return false;
		}
		if (logkg[0] != Double.NEGATIVE_INFINITY) {
			System.out.println("logkg = " + Arrays.toString(logkg));
			System.out.println("test3: FAILED");
			return false;
		}
		for (int x = 1; x < kg.M(); x++)
			if (logkg[x] != Double.POSITIVE_INFINITY) {
				System.out.println("logkg = " + Arrays.toString(logkg));
				System.out.println("test3: FAILED");
				return false;
			}
		System.out.println("test3: OK");
		return true;
	}

	public static boolean test4() {
		Belief b = new Belief(5);
		double logkg[];
		try {
			b.Update(0, 1.0); // make alternative 0 informative.
			b.Update(0, 1.0);
			b.Update(0, 1.0);
			logkg = KG.LogKGfactor(b);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test4: FAILED");
			return false;
		}
		if (logkg[0] != Double.NEGATIVE_INFINITY) {
			System.out.println("logkg = " + Arrays.toString(logkg));
			System.out.println("test4: FAILED");
			return false;
		}
		for (int x = 1; x < b.M(); x++)
			if (logkg[x] != Double.POSITIVE_INFINITY) {
				System.out.println("logkg = " + Arrays.toString(logkg));
				System.out.println("test4: FAILED");
				return false;
			}
		System.out.println("test4: OK");
		return true;
	}

	/*
	 * We have no further test code for this class here, since the test code in
	 * LL1 and LL1Simple do a good job of exercising this class.
	 */
}
