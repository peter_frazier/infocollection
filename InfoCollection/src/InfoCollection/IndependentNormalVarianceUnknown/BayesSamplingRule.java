package InfoCollection.IndependentNormalVarianceUnknown;

import java.util.Random;

import InfoCollection.util.MathPF;

/*
 * See description of IndependentNormalVarianceKnown.BayesSamplingRule.  This
 * class is essentially the same as that one.
 */
public abstract class BayesSamplingRule implements InfoCollection.SamplingRule {
	Belief belief0;
	Belief belief;
	Random rnd; // If you have to make any randomized decisions, use this.

	public BayesSamplingRule() {
		belief0 = null;
		belief = null;
		rnd = new Random();
	}

	public BayesSamplingRule(Belief b) {
		belief0 = new Belief(b);
		belief = null;
		rnd = new Random();
	}

	public void Start(int desiredM) throws Exception {
		if (belief0 == null)
			belief = new Belief(desiredM); // non-informative.
		else if (desiredM == belief0.M())
			belief = new Belief(belief0);
		else
			throw new Exception("belief0 and desiredM don't match.");
	}

	public void RecordMeasurement(int x, double y) throws Exception {
		belief.Update(x, y);
	}

	public int GetImplementationDecision() {
		return MathPF.argmax(belief.mean(), rnd);
	}

	public int M() {
		return belief.M();
	}

	public Belief GetBelief() {
		return belief;
	}
}
