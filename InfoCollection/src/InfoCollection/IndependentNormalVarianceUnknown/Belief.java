package InfoCollection.IndependentNormalVarianceUnknown;

import java.util.Arrays;
import java.util.Random;

import umontreal.iro.lecuyer.randvar.GammaGen;
import umontreal.iro.lecuyer.rng.MRG31k3p;
import umontreal.iro.lecuyer.rng.RandomStream;
import InfoCollection.NormalTruth;
import InfoCollection.NotInformativeException;
import InfoCollection.Truth;
import InfoCollection.util.MathPF;
import InfoCollection.util.MeanVariance;
import InfoCollection.util.Student;

/*
 * This class describes a Bayesian prior/posterior on a collection of
 * alternatives variables with unknown sampling variance.  The prior/posterior
 * belief on the true means of the samples are normal/inverse-gamma.
 * 
 * In particular, conditioned on the sampling precision R, the mean of the
 * sampling distribution Y is normal with mean \mu and precision R\tau.  Note
 * that this means that \tau is something like a number of observed
 * measurements.  The distribution of R is gamma(a,b).  This notation is almost
 * consistent with deGroot, p.169, except deGroot uses alpha,beta where we use
 * a,b.
 *
 * According to deGroot p.170, the marginal of \mu is student-t with 2a degrees
 * of freedom, location parameter mu, and precision a*tau/b.
 *
 * One way to think of a as the number of times we have observed 
 */
public class Belief implements InfoCollection.Belief {
	public double mu[];
	public double tau[];
	public double a[];
	public double b[];

	/*
	 * Creates a non-informative belief with the given number of alternatives.
	 * See deGroot p.195 for this noninformative prior.
	 */
	public Belief(int M) {
		mu = new double[M];
		tau = new double[M];
		a = new double[M];
		b = new double[M];
		for (int i = 0; i < M; i++) {
			mu[i] = tau[i] = b[i] = 0;
			a[i] = -0.5;
		}
	}

	/*
	 * Creates a belief where the mu,tau,a,b parameters are the same for each
	 * alternative. These parameters are passed in, as is the number of
	 * alternatives M.
	 */
	public Belief(int M, double mu, double tau, double a, double b) {
		this.mu = new double[M];
		this.tau = new double[M];
		this.a = new double[M];
		this.b = new double[M];
		for (int i = 0; i < M; i++) {
			this.mu[i] = mu;
			this.tau[i] = tau;
			this.a[i] = a;
			this.b[i] = b;
		}
	}

	/*
	 * Create a belief where the sampling precision is known very well, but the
	 * sampling mean is completely unknown. In particular, the sampling
	 * precision R has a gamma(a,b) prior. This has mean a/b, and in the limit
	 * as a,b->infinity, is equal to a point mass at a/b. We simply take b
	 * large, set a equal to b times the known precision, and then take tau=0 to
	 * give a noninformative prior on the sampling mean.
	 */
	public Belief(double precision[]) {
		int M = precision.length;
		assert M > 0;
		mu = new double[M];
		tau = new double[M];
		a = new double[M];
		b = new double[M];
		for (int i = 0; i < M; i++) {
			mu[i] = tau[i] = 0;
			b[i] = 10000; // make this bigger to make it more certain.
			a[i] = b[i] * precision[i];
		}

	}

	/* Copy constructor. */
	public Belief(Belief tocopy) {
		mu = tocopy.mu.clone();
		tau = tocopy.tau.clone();
		a = tocopy.a.clone();
		b = tocopy.b.clone();
	}

	public void Update(int x, double y) throws Exception {
		double newtau, newmu;
		newtau = tau[x] + 1;
		newmu = (tau[x] * mu[x] + y) / newtau;
		a[x] += 0.5;
		b[x] += StrictMath.pow(y - mu[x], 2) * tau[x] / (2.0 * newtau);
		mu[x] = newmu;
		tau[x] = newtau;
	}

	/*
	 * Parameters of the marginal distribution of Y.
	 * 
	 * According to deGroot p.170, the marginal of the true mean Y is student-t
	 * with 2a degrees of freedom, location parameter mu, and precision a*tau/b.
	 * We also have shorthand names for these functions, because they are called
	 * frequently. The marginal precision can also be written tau / sigmahat^2.
	 */
	public double MarginalDegreesFreedom(int x) {
		return 2 * a[x];
	}

	public double[] MarginalMean() {
		double[] ret = new double[M()];
		for (int x = 0; x < M(); x++)
			ret[x] = MarginalMean(x);
		return ret;
	}

	public double MarginalMean(int x) {
		if (IsIntegrable(x))
			return mu[x];
		else
			return Double.NaN;
	}

	public double MarginalPrecision(int x) {
		return a[x] * tau[x] / b[x];
	}

	public double dof(int x) {
		return MarginalDegreesFreedom(x);
	}

	public double mean(int x) {
		return MarginalMean(x);
	}

	public double[] mean() {
		return MarginalMean();
	}

	public double prec(int x) {
		return MarginalPrecision(x);
	}

	/*
	 * \sigmahat^2 is a sufficient statistic often used by frequentists to
	 * estimate the sampling variance R. It is equal to (sum of square
	 * residuals)/(num samples - 1).
	 * 
	 * If we take the noninformative prior, or if we think of the prior as
	 * giving the results of some first stage, then we can think of the
	 * following routine as returning sigmahat.
	 */
	public double sigmahat2(int x) {
		/*
		 * deGroot p.195 notes that under the noninformative prior, b[x] = 0.5 *
		 * (sum of square residuals). This formula comes from that. This result
		 * is also equal to 2*b[x]/(nForPrecision(x)-1), which makes some sense
		 * if we compare it to the frequentist estimator (sum of squares)/(n-1),
		 * and think of 2*b[x] as the sum of squares.
		 */
		return b[x] / a[x];
	}

	/*
	 * These functions compute the precision parameter of the Student-t
	 * distributed predictive posterior mean of alternative i, when we sample
	 * alternative i. In the limit as the degrees of freedom goes to infinity,
	 * it is the precision of a normal distribution. When alternative j has no
	 * measurements allocated to it and alternative i has only 1, it is equal to
	 * lambda_{{ij}} in Chick,Branke,Schmidt 2007. Note that there is a
	 * difference between lambda with {} in the subscripts, and lambda without.
	 * It is also equal to 1/sigmatilde^2(x).
	 */
	public double PredictivePrecision(int x) {
		return nForMean(x) * (nForMean(x) + 1) / sigmahat2(x);
	}

	/*
	 * Consider the marginal distribution of Y_i - Y_j. This can be approximated
	 * by a student-t distribution with mean mu(i)-mu(j), some scale, and some
	 * number of degrees of freedom. The following two functions compute this
	 * scale and this number of degrees of freedom. The degrees of freedom
	 * computation is the Welch approximation. DifferenceScale(i,j) is also
	 * known as \lambda_{ij}^{-1} in ChBrSc07. It is something like the
	 * variance, but not exactly. DifferenceDofWelch(i,j) is also known as
	 * \nu_{(i)(j)}
	 */
	public double DifferenceScale(int i, int j) {
		double ri = sigmahat2(i) / nForMean(i);
		double rj = sigmahat2(j) / nForMean(j);
		return ri + rj;
	}

	public double DifferenceDofWelch(int i, int j) {
		double numerator, denominator, ri, rj;
		ri = sigmahat2(i) / nForPrecision(i);
		rj = sigmahat2(j) / nForPrecision(j);
		numerator = StrictMath.pow(ri + rj, 2);
		denominator = StrictMath.pow(ri, 2) / (nForPrecision(i) - 1);
		denominator += StrictMath.pow(rj, 2) / (nForPrecision(j) - 1);
		return numerator / denominator;
	}

	/*
	 * If we take the noninformative prior, or if we think of the prior as
	 * giving the results of some first stage, then we can think of the
	 * following routine as returning the number of samples taken.
	 * 
	 * For some priors, nForMean() and nForPrecision() are different, but for
	 * the noninformative prior they are equal.
	 */
	public double nForMean(int x) {
		return tau[x];
	}

	public double nForPrecision(int x) {
		return 2 * a[x] + 1;
	}

	/*
	 * Generates the true values from the random number generator according to
	 * the prior.
	 */
	public Truth GenerateTruth(Random rnd) throws NotInformativeException {
		if (!IsInformative())
			throw (new NotInformativeException());
		double mean[] = new double[M()];
		double precision[] = new double[M()];
		RandomStream s = new MRG31k3p();
		for (int x = 0; x < M(); x++) {
			/*
			 * First generate the precisions, and then generate the means.
			 */
			precision[x] = GammaGen.nextDouble(s, a[x], b[x]);
			mean[x] = mu[x] + StrictMath.sqrt(precision[x])
					* rnd.nextGaussian();
		}
		return new NormalTruth(mean, precision);
	}

	/*
	 * Returns whether or not the distribution on Y is both L1 and L2 integrable
	 * under the belief. This will be the case if the belief is informative. For
	 * it to be informative, we need the number of degrees of freedom to be > 1,
	 * otherwise it is Cauchy or worse. Furthermore, if our prior had good
	 * knowledge of the precision but not of the mean, we need the number of
	 * measurements of the mean to be positive because otherwise we can have
	 * infinite degrees of freedom but be noninformative on the mean.
	 */
	public boolean IsIntegrable(int x) {
		return (MarginalDegreesFreedom(x) > 1 && nForMean(x) > 0);
	}

	public boolean IsIntegrable() {
		int x;
		for (x = 0; x < M() && IsIntegrable(x); x++)
			;
		return (x == M()); // true if all are integrable
	}

	public Boolean IsInformative() {
		return IsIntegrable();
	}

	/*
	 * Return the indices of those alternatives that are not integrable. PF: Has
	 * not been tested.
	 */
	public int[] GetNotIntegrable() {
		int n, i, x, indices[];
		/* First count those alternatives that are not integrable. */
		for (n = x = 0; x < M(); x++) {
			if (!IsIntegrable(x))
				n++;
		}
		if (n == 0)
			return null;
		indices = new int[n];
		for (i = x = 0; x < M(); x++) {
			if (!IsIntegrable(x))
				indices[i++] = x;
		}
		return indices;
	}

	/*
	 * Among all the not integrable alternatives, choose one uniformly at
	 * random.
	 */
	public int ChooseRandomNotIntegrable(Random rnd) {
		int i, j, x, n;
		/* First count those alternatives that are not integrable. */
		for (n = x = 0; x < M(); x++) {
			if (!IsIntegrable(x))
				n++;
		}

		/*
		 * Then choose one of the not-integrable ones. Choose i uniformly, and
		 * then count up the non-integrable ones until you get to the ith.
		 */
		i = rnd.nextInt(n);
		for (j = x = 0;; x++) {
			if (!IsIntegrable(x)) {
				if (j++ == i)
					return x;
			}
		}
	}

	/*
	 * Calculates the Bonferonni approximation to expected opportunity cost.
	 * This is defined on pages 4-6 of Chick,Branke,Schmidt 2007 in Informs
	 * Journal on Computing.
	 */
	public double EOCBonf() throws Exception {
		double EOC, d, s, dof;
		int i, best;
		best = MathPF.argmax(mu);
		EOC = 0;
		if (!IsIntegrable())
			return Double.POSITIVE_INFINITY;
		for (i = 0; i < M(); i++) {
			if (i == best)
				continue;
			// s is lambda_{ik}^{-1/2} in the language of ChBrSc07.
			s = StrictMath.sqrt(DifferenceScale(i, best));
			dof = DifferenceDofWelch(i, best);
			d = (mu[best] - mu[i]) / s;
			EOC += s * Student.Psi(dof, d);
		}

		return EOC;
	}

	// Number of alternatives
	public int M() {
		return mu.length;
	}

	@Override
	public String toString() {
		return String.format("mu=%s\ntau=%s\na=%s\nb=%s", Arrays.toString(mu),
				Arrays.toString(tau), Arrays.toString(a), Arrays.toString(b));
	}

	/*
	 * Returns the belief used to generate random problems instances (RPI) in
	 * ChBrSc07.
	 * 
	 * They have a parameter eta set to 0.5, a parameter alpha set to beta+1,
	 * and a parameter beta which I believe is set to 100. I am not completely
	 * sure because they only say in figure 7 that "b=100", and I think by b
	 * they mean beta.
	 * 
	 * Given these parameters, they say that they draw the true sampling
	 * variance from an Inverse Gamma (alpha,beta), or equivalently the
	 * precision from a Gamma(alpha,beta). Then they draw the true sampling
	 * means from a normal with mean mu_0 and variance equal to the true
	 * sampling variance divided by eta. They don't say what mu_0 is, but it
	 * doesn't matter since it is just a baseline level. A sensible choice would
	 * be 0. They have 5 alternatives.
	 * 
	 * So for us, we take mu=0, tau=1/eta=2, a=alpha=101, b=beta=100, M=5.
	 */
	public static Belief RandomProblemBeliefChBrSc07() {
		return new Belief(5, 0, 2, 101, 100);
	}

	/*
	 * Test code.
	 */

	public static void main(String args[]) throws Exception {
		String myclass = "IndependentNormalVarianceUnknown.Belief";
		System.out.printf("Testing %s\n", myclass);
		boolean ok = true;
		ok = test1() && ok;
		ok = test2() && ok;
		ok = test3() && ok;
		ok = test4() && ok;
		System.out.printf("%s: %s\n", myclass, ok ? "OK" : "FAILED");
	}

	/*
	 * Tests that the computation of sigmahat is correct.
	 */
	public static boolean test1() {
		try {
			int N = 1000;
			MeanVariance mv = new MeanVariance();
			Random rnd = new Random();
			Belief b = new Belief(1);
			for (int n = 0; n < N; n++) {
				double data = rnd.nextGaussian();
				b.Update(0, data);
				mv.AddSample(data);
			}
			/*
			 * Now, SampleVarianceMSE(), which computes sigmahat^2, should be
			 * equal to sigmahat^2 as computed by Belief.
			 */
			double mv_val = mv.SampleVarianceMSE();
			double b_val = b.sigmahat2(0);
			int ulps = MathPF.FloatCmp(mv_val, b_val);
			boolean ok = ulps < 30;
			System.out.printf("test1: diff/ulp=%d (true)variance=1 "
					+ "MeanVariance.sigmahat=%.3f belief.sigmahat=%.3f\n",
					ulps, mv.SampleVarianceMSE(), b.sigmahat2(0));
			if (ok)
				System.out.println("test1: OK");
			else
				System.out.println("test1: FAILED");
			return ok;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/*
	 * Tests that ChooseRandomNotIntegrable() is correct.
	 */
	public static boolean test2() {
		int M = 5;
		Belief b = new Belief(M);
		try {
			b.Update(2, 0);
			b.Update(2, 1);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		/* Now alternative 2 is integrable, but nothing else. */
		if (b.IsIntegrable() || !b.IsIntegrable(2)) {
			System.out.println("test2: FAILED");
			return false;
		}
		/*
		 * The distribution of ChooseRandomNotIntegrable() should be uniform on
		 * {0,1,3,4}. Test this.
		 */
		int x;
		int nsamples = 1000;
		int hist[] = new int[M];
		Random rnd = new Random();
		for (x = 0; x < M; x++) {
			hist[x] = 0;
		}
		for (int n = 0; n < nsamples; n++) {
			hist[b.ChooseRandomNotIntegrable(rnd)]++;
		}
		/*
		 * Check that these counts are reasonable. hist[2] should be about 0,
		 * and the rest should be about equal.
		 */
		int min, max;
		max = min = hist[0];
		for (x = 1; x < M; x++) {
			if (x == 2)
				continue;
			if (hist[x] < min)
				min = hist[x];
			if (hist[x] > max)
				max = hist[x];
		}
		double diff = (double) (max - min) / nsamples;
		System.out.printf("test2: max=%d min=%d diff=%3f counts=%s\n", max,
				min, diff, Arrays.toString(hist));
		if (diff > .1 || hist[2] != 0) {
			System.out.println("test2: FAIL");
			return false;
		}

		System.out.println("test2: OK");
		return true;
	}

	/*
	 * Tests that EOCBonf can handle non-integrable states.
	 */
	public static boolean test3() {
		Belief b = new Belief(10);
		try {
			@SuppressWarnings("unused")
			double eoc = b.EOCBonf();
			System.out.println("test3: OK");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("test3: FAILED");
			return false;
		}
	}

	/*
	 * If we create a belief where the precisions are essentially known, then
	 * our estimate of the sampling variance should be essentially equal to the
	 * true sampling variance.
	 */
	public static boolean test4() {
		return test4(false);
	}

	public static boolean test4(boolean verbose) {
		boolean ok = true;
		double precision[] = { 1.0, 2.0 };
		Belief b = new Belief(precision);
		for (int i = 0; i < precision.length; i++) {
			double estimate = 1.0 / b.sigmahat2(i);
			int ulps = MathPF.FloatCmp(precision[i], estimate);
			if (verbose)
				System.out.printf("test4: diff/ulp=%d (true)precision=%.3f "
						+ "1/sigmahat2=%.3f\n", ulps, precision[i], estimate);
			if (ulps > 30)
				ok = false;
		}
		if (ok)
			System.out.println("test4: OK");
		else
			System.out.println("test4: FAILED");
		return ok;
	}
}
