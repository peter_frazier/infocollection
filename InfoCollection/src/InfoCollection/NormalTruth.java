package InfoCollection;

import java.util.Arrays;
import java.util.Random;

import InfoCollection.util.MathPF;

public class NormalTruth extends Truth {
	double mean[];
	double precision[];
	double best; // cache this for speed.

	public NormalTruth(double values[]) {
		mean = values.clone();
		best = MathPF.max(values);
		precision = new double[values.length];
		for (int x = 0; x < values.length; x++)
			precision[x] = 1;
	}

	public NormalTruth(double values[], double precision) {
		mean = values.clone();
		best = MathPF.max(values);
		this.precision = new double[values.length];
		for (int x = 0; x < values.length; x++)
			this.precision[x] = precision;
	}

	public NormalTruth(double mean[], double precision[]) {
		assert mean.length == precision.length;
		this.mean = mean.clone();
		this.precision = precision.clone();
		best = MathPF.max(mean);
	}

	@Override
	public double Sample(int x, Random r) {
		return mean[x] + NoiseStdDev(x) * r.nextGaussian();
	}

	@Override
	public double BestValue() {
		return best;
	}

	@Override
	public double Value(int x) {
		return mean[x];
	}

	/* Linear loss of deciding x. */
	@Override
	public double Loss(int x) {
		assert BestValue() >= Value(x) : String.format("best=%f values=%s\n",
				best, Arrays.toString(mean));
		return BestValue() - Value(x);
	}

	@Override
	public int M() {
		return mean.length;
	}

	@Override
	public String toString() {
		return "sampling mean=" + Arrays.toString(mean)
				+ "\nsampling precision=" + Arrays.toString(precision);
	}

	// Standard deviation of the noise
	public double NoiseStdDev(int x) {
		return Math.sqrt(1 / precision[x]);
	}

	public double[] SamplingPrecision() {
		return precision;
	}

	/*
	 * Example 1 from HeChCh07. The example uses a noninformative prior and true
	 * values which are monotonically increasing, 1,...,10. It uses a noise
	 * precision of 1/36. Generally one uses it with a noninformative prior.
	 */
	public static NormalTruth Example1() {
		double[] v = new double[10];
		for (int x = 0; x < 10; x++)
			v[x] = x + 1;
		return new NormalTruth(v, 1.0 / 36);
	}

	/*
	 * This is the slippage configuration. It is designed to reward exploration.
	 * 1 alternative is best, and M-1 alternatives are tied for second best. The
	 * measurement precision is fixed to 1, and the difference between best and
	 * second best is configurable (it is called delta). Generally one uses it
	 * with a noninformative prior.
	 */
	public static NormalTruth SlippageExample() {
		/*
		 * M=5, delta=.5, and noise beta=1 are the parameters used in Figure 6
		 * of Chick, Branke, Schmidt 2007.
		 */
		return SlippageExample(.5, 5);
	}

	public static NormalTruth SlippageExample(double delta, int M) {
		double[] v = new double[M]; // "v" is for "values"
		for (int x = 0; x < M; x++) {
			if (x == 0)
				v[x] = 0;
			else
				v[x] = -delta;
		}
		return new NormalTruth(v); // this makes noiseBeta=1.
	}

	/*
	 * This example uses monotonically increasing true values. A noninformative
	 * prior is generally used with it. Measurement precision is fixed to 1. In
	 * Chick,Branke,Schmidt 2007 (IJoC), they use M=10, delta=.5.
	 */
	public static NormalTruth MonotoneDecreasingMeansExample() {
		return MonotoneDecreasingMeansExample(.5, 10);
	}

	public static NormalTruth MonotoneDecreasingMeansExample(double delta, int M) {
		double[] v = new double[M]; // "v" is for "values"
		for (int x = 0; x < M; x++)
			v[x] = -x * delta;
		return new NormalTruth(v); // this makes noiseBeta=1.
	}

	/*
	 * Sets Y_x = b * a^x. If we choose b=100 and a=1/2 then this is the example
	 * where Y_0 = 100, Y_1 = 50, Y_2 = 25, etc. This might work well with
	 * noiseBeta=1/(10^4)=.0001. Generally one uses it with a noninformative
	 * prior.
	 */
	public static NormalTruth ExponentialDecreasingMeansExample() {
		return ExponentialDecreasingMeansExample(.5, 100, .0001, 20);
	}

	public static NormalTruth ExponentialDecreasingMeansExample(double a,
			double b, double noiseBeta, int M) {
		double[] v = new double[M];
		for (int x = 0; x < M; x++)
			v[x] = b * Math.pow(a, x);
		return new NormalTruth(v, noiseBeta);
	}

	/*
	 * Randomly generate a problem according to a gamma prior, by default
	 * according to the gamma prior used in ChBrSc07. We pass in the seed to use
	 * so that the results are reproducable, i.e. so that you can get back the
	 * same random configuration again and again if you like.
	 */
	public static Truth RandomConfiguration(int seed) {
		Random rnd = new Random(seed);
		InfoCollection.IndependentNormalVarianceUnknown.Belief belief = InfoCollection.IndependentNormalVarianceUnknown.Belief
				.RandomProblemBeliefChBrSc07();
		Truth config;
		try {
			config = belief.GenerateTruth(rnd);
		} catch (NotInformativeException e) {
			/*
			 * If this catch block triggers it's a system error because the
			 * belief was created to be informative.
			 */
			config = Example1();
			assert (false);
		}
		return config;
	}

}
