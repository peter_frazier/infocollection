// This example code uses the InfoCollection library to run the KG allocation
// rule with a fixed stopping rule on a random problem instance drawn from an
// independent normal prior distribution.

// Use independent normal sampling, with independent normal prior and known variance.
import InfoCollection.IndependentNormalVarianceKnown.*; 

// import other classes from InfoCollection 
import InfoCollection.Simulator;
import InfoCollection.SamplingRule;
import InfoCollection.StoppingRule;
import InfoCollection.FixedStop;
import InfoCollection.Truth;

import java.util.Random;

public class example {
	public static void main(String args[])
	{
		System.out.println("******************************");
		System.out.println("InfoCollection Example Program");
		System.out.println("******************************");

		// Creates a homogenous belief with 5 alternatives.  On each
		// alternative, we have an independent normal prior with mean 0
		// and precision 2 on its value.  Each alternative has a
		// sampling precision of 0.5.
		Belief prior = new Belief(5,0,2,0.5);

		// Use knowledge gradient for our sampling allocation rule.
		SamplingRule alloc = new KG(prior);

		// Use a fixed stopping rule, that stops after 20 measurements.
		// More sophisticated stopping rules may also be used.
		// A simulation sampling rule is a combination of allocation
		// and stopping rule.
		StoppingRule stop = new FixedStop(20); 

		// Initialize a random number generator
		Random rnd;
		try { rnd = new Random(); } 
		catch(Exception e) {
		  System.out.println("Initialization of random number generator failed.");
		  System.err.println(e); 
		  return;
		}

		// Generate a random truth from the prior.  It is also possible
		// to specify a fixed truth, not from the prior.
		Truth truth;
		try { truth = prior.GenerateTruth(rnd); } 
		catch(Exception e) {
		  System.out.println("Belief.GenerateTruth failed.");
		  System.out.println("This should only happen for non-informative priors.");
		  System.err.println(e); 
		  return;
		}


		// Number of times we will repeat our sampling policy in the
		// Monte Carlo experiment run below.
		int nruns = 1000;

		// Specify a maximum number of samples that we allow our
		// simulation sampling rule to take, even if the stopping rule
		// asks for more.  This is to prevent experiments from taking
		// too long.
		int maxN = 100000; 

		// Run our simulation sampling rule on the truth generated above.
		// It is also possible to run the sampling rule on a
		// different truth, drawn from the prior, each time.
		try {
		  System.out.println("Allocation rule: " + alloc);
		  System.out.println("Stopping rule: " + stop);
		  System.out.println(prior);
		  System.out.println(truth);
		  System.out.println("Running Simulation...");

		  double[] result = Simulator.LossWhenStopped(alloc, truth, stop, nruns, maxN); 

		  System.out.println("Simulation Results:");
		  System.out.println("   Expected Opportunity Cost:" + result[0] + "+/-" + result[3]);
		  System.out.println("   Expected Number of Samples:" + result[1] + "+/-" + result[4]);
		  // This reports the number of times that the sampling
		  // algorithm was terminated because it took more than maxN
		  // samples.
		  System.out.println("   Number of timeouts:" + result[2]);
		} catch(Exception e) { 
		  System.out.println("Simulation failed.");
		  System.err.println(e); 
		}
	}
}
