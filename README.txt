This is the InfoCollection library, written in java and, containing algorithms
for doing simulation optimization.  Here is a brief description of the overall
structure:

Makefile.osx
Makefile.cygwin	(coming soon)

Platform-specific makefiles for compiling the InfoCollection library and creating
lib/InfoCollection.jar.  They expect the third-party libraries to be present in
InfoCollection/lib as described below.  To run a specific makefile on OSX,
Cygwin, or UNIX, use the flag -f, as in, for example

	make -f Makefile.osx
	make -f Makefile.cygwin

InfoCollection/src:	
Contains all of the source code for the library.

InfoCollection/lib:	
Contains all third-party libraries, and InfoCollection.jar, which can be
created by the Makefiles in the top-level directory.
  Note: To compile and use the current version of this library, you will need
to copy here all of the third-party libraries that the library currently uses.
These third-party libraries are not distributed as part of the bitbucket
repository.  Our long-term goal is to get rid of our dependence on these
third-party libraries.
	 
InfoCollection/examples:
Contains example code using the InfoCollection library.

InfoCollection/doc:
Contains some basic documentation.
